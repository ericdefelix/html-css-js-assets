var RESPONSE = {

    init: function(FSRObj) {
        this.sessionID = FSRObj.sessionToken;
    },
    
    get_data: function(url) {
        $.ajax({
            type:'GET',
            url: 'http://fsrgen.gqaustralia.com.au/front/fsr?url='+url,
            dataType: 'json',
            cache: true
        })
        .success(function(response) {
            //console.log(response);
            FULL_PAGE.init(response);
            if (response.data.hasOwnProperty('bg_image')){
                $('body').css('background-image', 'url('+response.data.bg_image+')');
            }
            console.log("success");
        })
        .error(function() {
            console.log("error");
        });
    },
    rebuild: function() {
        
    },

    build: function(url) {
        
    }
}