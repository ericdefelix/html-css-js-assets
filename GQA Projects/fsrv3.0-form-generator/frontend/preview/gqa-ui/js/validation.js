/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var validateInput = {
    doc: $(document),
    bind : function(){
        this.doc.on('click','[data-target="btnevent"]',function(){
          
            validateInput.validate($(this));
        });
    },
    validate :function(element){
    
        
        var input = element.siblings('.form-group').children('input');
        var value = input.val();
        if(value.trim() == '') {
            var label = input.next('label');
            label.text("Field cannot be empty!");
            if(label.hasClass('errormsg')){
                label.removeClass('errormsg');
            } else {
            label.addClass('errormsg');
        }
            input.focus();
        }
        
    },
    build: function(){ 
        
		if (this.doc.find('[data-target="btnevent"]').length) {
                    
                    validateInput.bind();
                }
    }
}
$(document).ready(function(){
    validateInput.bind();
});
// var validateInput = function(id, type, message) {
//     var value = $("#"+id).val();
//    
//     if (value === '') {
//         alert(message);
//       //sendMessage(id, false, message) ;
//     }
//         
//     
// }
 var sendMessage = function(id, result, message){
     
 {
    
    if (result) {
         
        $("#"+id+"_label").text('OK!');
        $("#"+id).removeClass('error');
        $("#"+id+"_label1").removeClass('imgerror');
        $("#"+id).addClass('success valid');
        
        $("#"+id+"_label").removeClass('error');
        $("#"+id+"_label").addClass('success valid');
        $("#"+id+"_label").show();
        
    } else {
        if($("#"+id).hasClass('success valid')){
        $("#"+id+"_label").removeClass('success valid');
    }
        $("#"+id+"_label").text(message);
        $("#"+id).addClass('error');
        $("#"+id+"_label").addClass('error');
         $("#"+id+"_label1").addClass('imgerror');
         $("#"+id+"_label").show();
     
       // $("#"+id+"_label").hide();
    }
};
 }

