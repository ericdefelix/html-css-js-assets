// Modified Bootstrap dropdown so it doesnt close if clicked 
var DROPDOWN_MANUAL = {
    obj: [],
    doc: $(document),
    bind: function () {
        this.doc.on('click', '[data-toggle="dropdown-custom"]', function (e) {
            e.preventDefault();
            DROPDOWN_MANUAL.toggle_menu($(this));
        });

        this.doc.on('click', 'html', function (event) {
            var dd = $(event.target).closest('.dropdown');

            if (dd.length == 0) {
                $('.dropdown').removeClass('open');
                DROPDOWN_MANUAL.obj = [];
            }
        });
    },

    toggle_menu: function (elem) {
        var dd = elem.parent('.dropdown');

        dd.addClass('open');
        DROPDOWN_MANUAL.obj.push(elem);

        if (DROPDOWN_MANUAL.obj.length == 2) {
            DROPDOWN_MANUAL.obj[0].parent('.dropdown').removeClass('open');
            DROPDOWN_MANUAL.obj[0].prev().remove();
        }
        if (DROPDOWN_MANUAL.obj.length > 2) {
            DROPDOWN_MANUAL.obj.shift();
            DROPDOWN_MANUAL.obj[0].parent('.dropdown').removeClass('open');
            DROPDOWN_MANUAL.obj[0].prev().remove();
        }
    },

    close_opened_menu: function (elem) {
        DROPDOWN_MANUAL.obj = [];
        elem.parent('.dropdown').removeClass('open');
        elem.remove();
    },
    build: function () {
        this.bind();
    }
}

// Toggle elements menu
var TOGGLE_MENU = {
    hidden      : true,
    doc 	: $(document),
    sidebar     : $('#sidebarParent'),
    btn         : $('#btnSidebar'),
    scroll      : $('[data-sidebar-scroll]'),
    expand      : $('.js_expandMenu'),
    container   : $('.fsr-interface-create'),
    overlay     : '<div class="sidebar-overlay"></div>',
    sb_width    : 0,
    prev_elem  	: undefined,

    init: function() {
        this.sb_width = this.sidebar.outerWidth();

        TOGGLE_MENU.sidebar.hide();

        TOGGLE_MENU.doc.on('click', '[data-toggle="sidebar"]', function() {
            TOGGLE_MENU.hidden == false ? TOGGLE_MENU.hide($(this)) : TOGGLE_MENU.show($(this));
            $(this).addClass('js_no-spam-clicking'); // no double click if sidebar is sliding
        });

        TOGGLE_MENU.doc.on('click', '.js_expandMenu', function(){  TOGGLE_MENU.display_sublist($(this)); });

        // Apply Custom Scrollbar
        TOGGLE_MENU.doc.find('[data-sidebar-scroll]').mCustomScrollbar({
            autoHideScrollbar:  false,
            axis:               'y',
            contentTouchScroll: true,
            scrollbarPosition:  'inside',
            scrollInertia:      200,
            scrollButtons:      {enable: false},
            theme:              'dark',
            live:               true,
            advanced:           { updateOnContentResize: true }
        });

        TOGGLE_MENU.resize();
    },
    set_height: function() {
        this.scroll.css('height', TOGGLE_MENU.sidebar.height() - (38+15) );
    },
    resize: function() {
        var resizeTimer;
        window.addEventListener("resize", function(){
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(
                function(){
                    TOGGLE_MENU.doc.find('[data-sidebar-scroll]').mCustomScrollbar('update');
                    TOGGLE_MENU.set_height();
                }, 200);
        });
    },
    hide: function(btn) {
        TOGGLE_MENU.sidebar.velocity(
            { right: [-(TOGGLE_MENU.sb_width),'0'] }, 
            {
                begin: function() {
                    TOGGLE_MENU.scroll.velocity({ opacity: 0 },{ duration: 300, easing: 'linear' });
                },
                duration: 500, 
                easing: [.72,.51,.31,.84],
                complete: function(){
                    TOGGLE_MENU.sidebar.addClass('sidebar-hidden');
                    TOGGLE_MENU.hidden = true;
                    btn.removeClass('js_no-spam-clicking');
                    TOGGLE_MENU.container.addClass('js_adjust-content');
                    $(this).hide();
                }
            }
        );
        if (TOGGLE_MENU.sidebar.hasClass('has-overlay')) {
            $('.sidebar-overlay').velocity({ opacity: 0 },{ 
                duration: 500, 
                easing: [.72,.51,.31,.84], 
                display: 'none',
                complete: function() {
                    $(this).remove();
                }
            });
        }
    },

    show: function(btn) {
    	TOGGLE_MENU.scroll.velocity({ opacity: [1,0] },{ display: 'block', duration: 300, delay: 400, easing: 'linear' });
        TOGGLE_MENU.sidebar.velocity(
            { right: ['0',-TOGGLE_MENU.sb_width] },
            {
                begin: function() {
                	TOGGLE_MENU.scroll.hide();
                    TOGGLE_MENU.sidebar.removeClass('sidebar-hidden');
                    TOGGLE_MENU.hidden = false;
                },
                complete: function() { 
                    btn.removeClass('js_no-spam-clicking');
                    TOGGLE_MENU.container.removeClass('js_adjust-content')
                },
                display: 'block', 
                duration: 500, 
                easing: [.72,.51,.31,.84]
            }
        );

        if (TOGGLE_MENU.sidebar.hasClass('has-overlay')) {
            $(TOGGLE_MENU.overlay)
                .insertAfter(TOGGLE_MENU.sidebar)
                .velocity({ opacity: [1,0] },{ duration: 500, easing: [.72,.51,.31,.84] });
        }

    },

    display_sublist: function(target) {
        if (target.hasClass('open')) {
            return false;
        }
        else {
            target.addClass('js_no-spam-clicking');
            target.find('.arrowhead').velocity({rotateZ: '90deg'},{duration: 300, easing: [.4,.31,.33,1] });
            target.siblings('.js_sidebarSubList').show();
            target.siblings('.js_sidebarSubList').velocity({ 
                translateY: ['0%','-5%'], 
                opacity: [1,0]}, 
                {   
                    duration: 500, easing: [.4,.31,.33,1],
                    complete: function() {
                        TOGGLE_MENU.prev_elem = target;
                        target.addClass('open');
                        target.removeClass('js_no-spam-clicking');
                    }
                }
            );
            if (TOGGLE_MENU.prev_elem != undefined) {
                TOGGLE_MENU.prev_elem.removeClass('open');
                TOGGLE_MENU.prev_elem.siblings('.js_sidebarSubList').hide();
                TOGGLE_MENU.prev_elem.find('.arrowhead').velocity('reverse');
            }
        }
    },

    build: function() {
    	if (TOGGLE_MENU.sidebar.length) {
	    	this.set_height();
	        this.init();
    	}
    }
}

var SVG_HOVER = {
    init: function() {
        var mySVGsToInject = document.querySelectorAll('img.svg');
        SVGInjector(mySVGsToInject);
    },
    build: function() {
        this.init();
    }
}

// Call functions 
$(document).ready(function() {
    // SVG_HOVER.build();
    TOGGLE_MENU.build();
});