// Fullpage .JS
var FULL_PAGE = {
    bind: function() {
    },
    fsr : {},
    init: function(response) {
        // this.get_data();
        this.fsr = response.data;        
        this.fsr.sessionToken = response.token;
        PAGE_ELEMENTS.init(FULL_PAGE.fsr);
        
        this.is_mobile();
        $('#fsrFullPage').fullpage({
            navigation: true,
            css3: true
        });
        $.fn.fullpage.setMouseWheelScrolling(false);
        $.fn.fullpage.setAllowScrolling(false);
        $.fn.fullpage.setKeyboardScrolling(false);
    },
    
    is_mobile: function() {
        console.log(isMobile);
    },
    
    get_data: function(url) {
        
        $.ajax({
            type:'GET',
            url: 'http://fsrgen.gqaustralia.com.au/front/fsr?url='+url,
            dataType: 'json',
            cache: true
        })
        .success(function(response) {
            //console.log(response);
            FULL_PAGE.init(response);
            if (response.data.hasOwnProperty('bg_image')){
                $('body').css('background-image', 'url('+response.data.bg_image+')');
            }
            console.log("success");
        })
        .error(function() {
            console.log("error");
        });
    },
    rebuild: function() {
        
    },
    build: function(url) {
        this.get_data(url); 
    }
}

$(document).ready(function() {
   //var url = window.location.href;
    var url = 'https://www.gqaustralia.edu.au/free-skills-review-763758'; // use this once functionality is ready in function get_data
   FULL_PAGE.build(url); 
});
