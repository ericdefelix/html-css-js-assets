// elements
var PAGE_ELEMENTS  = {
    init : function (fsrObj){
        this.fsr = fsrObj;
        this.mainContainer = $('main');
        var pages = this.fsr.pages;
        this.answers = {};
        var parent = this;
        pages.forEach(function(page){
            PAGE_ELEMENTS.pageMarkup(page.id);
            page.questions.forEach(function(questionObj){
                qid = questionObj.id;
                parent.answers.qid = [];
                PAGE_ELEMENTS.questionMarkup(page.id, questionObj);
            })
        })
    },
    
    pageMarkup :function (pageId){
        $('<section/>', {
            'class':'section list-screen-item',
            'id' : 'sec-'+pageId,
        }).appendTo(PAGE_ELEMENTS.mainContainer)
    },
    
    questionMarkup : function (pageId, questionObj){
        var temphtml = '';
        var buttonhtml = '';
        
        temphtml += questionObj.hasOwnProperty('title') ? 
            '<h1>'+questionObj.title+'</h1>' : '';
        temphtml += questionObj.hasOwnProperty('sub_title') ? 
            '<h2>'+questionObj.sub_title+'</h2>' : '';
        temphtml += questionObj.hasOwnProperty('content') ? 
            '<h3>'+questionObj.content+'</h3>' : '';
        buttonhtml += questionObj.hasOwnProperty('button_text')?questionObj.button_text:'Submit';
                       
//            temphtml += questionObj.hasOwnProperty('button_text') ? 
//            '<button>'+questionObj.button_text+'</button>' : '';
        switch (questionObj.type) {
            case 'text' :
            temphtml += '<div class="form-group">'
                            +'<input type="text" class="form-control "  data-element= "textType" placeholder="'+questionObj.placeholder+'" id="'+questionObj.xml_equivalent+'">'
                            +'<label class="validation_label"></label>'   
                            +'</div>';
            var answerVal = $('#answer_'+questionObj.id).val();
//            temphtml += ' <button type="button" class="btn btn-primary btn-lg" onClick="slideNext(\''
//                    +questionObj.id+'\',\''+answerVal+'\')">'+buttonhtml+'</button>';
            temphtml += ' <button type="button" class="btn btn-primary btn-lg btnevent" data-target="btnevent">'+buttonhtml+'</button>'; 
          $('<div/>', {
            'class':'elements',
            'html': temphtml
             }).appendTo($('#sec-'+pageId));
            break;
            
            case 'select' :
                
                temphtml += '<button type="button" class="btn btn-primary btn-lg btn-chevron" data-toggle="sidebar" id="question-'+questionObj.id+'">'
                            +buttonhtml+'<i class="chevron right"></i>'
                            +'</button>';
             $('<div/>', {
            'class':'elements',
            'html': temphtml
        }).appendTo($('#sec-'+pageId));   
        if(questionObj.hasOwnProperty('answers')) {
              questionObj.answers.forEach(function(answer){
                         PAGE_ELEMENTS.answerMarkupSelect(questionObj.id,answer);
                    })
                }
             break;
            
            case 'checkbox' :
                  if(questionObj.hasOwnProperty('answers')) {
                    temphtml += '<div class="form-group">'
                            + '<div class="btn-group btn-group-stack" data-toggle="buttons" id="question-'+questionObj.id+'">'
                            +'</div>'
                            + '</div>';
      $('<div/>', {
            'class':'elements',
            'html': temphtml
        }).appendTo($('#sec-'+pageId));
                    questionObj.answers.forEach(function(answer){
                        var htmlEle =  PAGE_ELEMENTS.answerMarkupChkBox(questionObj.id,answer);
                        htmlEle.appendTo($('#question-'+questionObj.id));
                    })
                
               }
            break;
            
            case 'radio' :
                if(questionObj.hasOwnProperty('answers')) {
                    temphtml += '<div class="form-group">'
                            + '<div class="btn-group btn-group-stack" data-toggle="buttons" id="question-'+questionObj.id+'">'
                            +'</div>'
                            + '</div>';
      $('<div/>', {
            'class':'elements',
            'html': temphtml
        }).appendTo($('#sec-'+pageId));
                    questionObj.answers.forEach(function(answer){
                        var htmlEle =  PAGE_ELEMENTS.answerMarkupRadio(answer);
                        htmlEle.appendTo($('#question-'+questionObj.id));
                    })
                
               }
                
            break;
        }
     
    },
    
    answerMarkupRadio : function ( answer) {
        
        var answerhtml = '';
        answerhtml += '<label for='+answer.title+' class="btn btn-default btn-lg">';
        answerhtml += '<input type="radio" id="'+answer.title+'" name="'+answer.title+'"/>';
        answerhtml += ' <span class="option-label-fsr">'+answer.title+'</span>'
                      + '</label>';
         return ($('<div/>', {
            'class':'answers',
            'html':answerhtml
        }));
//eturn answerhtml;
        
    },
    answerMarkupChkBox : function ( qID, answer) {
        var answers2 = [];
        var answerhtml2 = '';
        answerhtml2 += '<label for='+answer.title+' class="btn btn-default btn-lg">';
        answerhtml2 += '<input type="checkbox" value="'+answer.title+'" id="'+answer.title+'" data-qid="'+qID+'"/>';
        answerhtml2 += ' <span class="option-label-fsr">'+answer.title+'</span>'
                      + '</label>';
        /*
        answerhtml2 += ' <button type="button" class="btn btn-primary btn-lg" onClick="slideNext(\''
                    + qID +'\')">'+buttonHTML+'</button>';
        */
       // PAGE_ELEMENTS.handleCheckboxes();
        return ($('<div/>', {
            'class':'answers',
            'html':answerhtml2
        }))
    },
        
    
    answerMarkupSelect : function (questionId,answer){
       var sidebarhtml = '';
       sidebarhtml +=  '<li><h5 class="sidebar-list-heading">'
                      + '<span>'+answer.title+'</span><i class="zmdi zmdi-hc-lg zmdi-chevron-right arrowhead"></i></h5></li>';
              
    
    $('<li/>',{
        'id': 'list-'+questionId,
        'html' : sidebarhtml
    }).appendTo('#sidebarSelect');
    } 
    
}

