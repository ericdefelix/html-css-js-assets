<?php
/**
 * middleware to get the BG Image from FSR frontend 
 * and curl it to the backend
 */

$ds = DIRECTORY_SEPARATOR;
$storeFolder = 'temp';
$url = UTILS.url('fsr');
$token = isset ($_POST['token']) ? $_POST['token'] : false;
$id = isset ($_POST['id']) ? $_POST['id'] : false;

if (!$token || !$id){
    exit('{"status":"error"}');
}
if (!empty($_FILES)) {    
    $tempFile = $_FILES['image']['tmp_name'];          //3                   
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //    
    $targetFile =  $targetPath. $_FILES['image']['name'];  //5
    move_uploaded_file($tempFile,$targetFile); //6    
}else {
echo '{"status":"error"}';
}

// now upload the file;
// $file_name_with_full_path = realpath($targetFile);
if (function_exists('curl_file_create')) { 
    $cFile = curl_file_create($targetFile);     
} else { 
    $cFile = '@' . realpath($targetFile); 
    
}
$post = array(
    'id' => $id,
    'token' => $token,
    '_method' => 'PUT',
    'image'=>$cFile
);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result=curl_exec ($ch);
curl_close ($ch);
$jsonDecoded = json_decode($result, true);
if ($jsonDecoded && $jsonDecoded['response'] == 200) {
    unlink($targetFile);
    $jsonDecoded['status'] = 'success';
    exit (json_encode($jsonDecoded));
}else {
   echo '{"status":"error"}';
}
