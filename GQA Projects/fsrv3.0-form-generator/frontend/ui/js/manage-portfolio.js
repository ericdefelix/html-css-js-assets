var FSR_TABLE = {
	doc: $(document),
	token: undefined,
	results: undefined,
	fetched_fsr_id: undefined,
	prev_status_index: 0,

	init: function() {
		FSR_TABLE.get_data();
		// if (AUTH.check_session()) {
			
		// }
		// else {
		// 	// LOADER.show('Oops. Looks like your session expired. <a onclick="AUTH.log_out()">Click here to log In</a>');
		// }
	},
	get_data: function() {
		LOADER.show('working...');

		FSR_TABLE.token = localStorage.getItem('token'); // get saved token from localStorage
		$.ajax({
			url: UTILS.url('fsr'),
			type: 'get',
			data: { 
				token: FSR_TABLE.token,
				page: '',
				limit: 1000,
				sort: 'created_at',
			}
		})
		.done(function(data) {
			FSR_TABLE.results = data.data;
			FSR_TABLE.bind();
		})
		.fail(function() {
			NOTIF.show('danger','Something went wrong while pulling FSRs');
		})
		.always(function(){
			LOADER.hide();
		});
	},
	bind: function() {
		this.doc.on('click', '.btn-group-set-status .btn', function(event) {
			event.preventDefault();
			FSR_TABLE.set_fsr_status($(this));
		});

		this.doc.on('click', '.btn-status ', function() {
			FSR_TABLE.prev_status_index = $(this).next('.dropdown-menu').find('.btn.active').index();
		});

		var height = $(document).height() - ($('footer').height() + $('header').height() + 30);

		$('#fsrTable').bootstrapTable({
			data: FSR_TABLE.results,
			pagination: true,
			search: true,
			height: height,
			sortable: true,
			sortName: 'created_at',
			sortOrder: 'desc',
		    formatNoMatches: function() {
		        return 'No FSR(s) available';
		    }
		});
	},
	display_text: function(text) {
		if (text === 'live') { return 'Live'; }
		else if (text === 'draft') { return 'Draft'; }
		else if (text === 'hold') { return 'On Hold'; }
		else if (text === 'archive') { return 'Archived'; }
	},
	set_fsr_status: function(sender,event) {
		var input 	= sender.children('input'),
			txt 	= FSR_TABLE.display_text(input.val()),
			status  = 'fsrStatus-' + input.val(),
			value   = sender.children('input').val(),
			id 		= parseInt(sender.closest('[data-target-fsr]').data('target-fsr')),
			fsr     = {};

		LOADER.show('working...');
		console.log(txt);

		$.ajax({
			url: UTILS.url('preview'),
			data: {token: FSR_TABLE.token, fsr_id: id }
		})
		.done(function(data) {
			fsr = data.data;

			console.log(fsr);
			var len = fsr.pages.length;
			var check_if_allowed = function() {
				for (var i = len - 1; i >= 0; i--) {
					if (fsr.pages[i].hasOwnProperty('questions') === false && txt === 'Live') {
						return false;
						break;
					}
					else if (fsr.pages[i].hasOwnProperty('questions') === true && txt === 'Live'){
						// for (var j = fsr.pages[i].questions.length-1; j >= 0; j--) {
						// 	if (fsr.pages[i].questions[j].title !== '' && fsr.pages[i].questions[j].sub_title !== '' && fsr.pages[i].questions[j].content !== '' && fsr.pages[i].questions[j].type !== 'opt-in') {
						// 		return true;
						// 		break;
						// 	}

						// 	if (fsr.pages[i].questions[j].type !== 'opt-in') {
						// 		return false;
						// 		break;
						// 	}
						// }
						return true
					}
				}
			}
			var revert_active_class = function() {
				sender.removeClass('active');
				sender.parent().find('.btn:eq('+FSR_TABLE.prev_status_index+')').addClass('active');
			}

			if (len === 0 && txt === 'Live') {
				LOADER.hide();
				revert_active_class();
				NOTIF.show('info','Seems like that FSR has no pages to display');
			}
			else {
				if (check_if_allowed() === false) {
					LOADER.hide();
					revert_active_class();
					NOTIF.show('info','That FSR needs to be completed before setting it to Live');
				}
				else {
					set_fsr_allowed();
				}
			}

			console.log(data);

			LOADER.hide();
		})
		.fail(function() {
			NOTIF.show('danger','Something went wrong while validating FSR ID '+id);
			LOADER.hide();
		})
		.always(function() {
			LOADER.hide();
		});

		function set_fsr_allowed() {
	        $.ajax({
	            url: UTILS.url('fsr'),
	            type: 'post',
	            data: {
	                action: 'updatestatus',
	                token: FSR_TABLE.token,
	                id : id,
	                status: value
	            }
	        })
	        .done(function(data){
				sender.closest('.js_fsrChangeStatus').find('.js_fsrStatusLabel').text(txt);
				sender.closest('.js_fsrChangeStatus').attr('data-status',status);
	        })
	        .fail(function(){
	        	NOTIF.show('danger','Something went wrong while changing the status');
	        })      
	        .always(function(){
	        	LOADER.hide();
	        });
		}

	},
	active: {
		draft: function(status) { if (status === 'draft') { return ' active'} else { return '';} },
		live: function(status) { if (status === 'live') { return ' active'} else { return '';} },
		hold: function(status) { if (status === 'hold') { return ' active'} else { return '';} },
		archive: function(status) { if (status === 'archive') { return ' active'} else { return '';} }
	},
	create_fsr: function() {
		LOADER.show('Creating FSR...');
		$.ajax({
			url: UTILS.url('fsr'),
			type: 'POST',
			data: { 
				token: FSR_TABLE.token,
				action: 'new'
			}
		})
		.done(function(data) {
			localStorage.setItem('fsrid', data.id);
			window.location.href = 'generator-main.html';
		})
		.fail(function() {
			NOTIF.show('danger','Something went wrong while creating a new FSR');
		})
	},
	edit_fsr: function(fsr_id) {
		localStorage.setItem('fsrid', parseInt(fsr_id));
		window.location.href = 'generator-main.html';
	},
	copy_fsr: function(fsr_id) {
		LOADER.show('Duplicating FSR...');
		$.ajax({
			url: UTILS.url('fsr'),
			type: 'POST',
			data: { 
				token: FSR_TABLE.token,
				action: 'copy',
				id: fsr_id
			}
		})
		.done(function(data) {
			localStorage.setItem('fsrid', data.id);
			window.location.href = 'generator-main.html';
		})
		.fail(function() {
			NOTIF.show('danger','Something went wrong while duplicating the FSR');
		})
	},
	build: function() {
		this.init();
	}
}

var TABLE_ELEMENTS = {
	render_dropdown: function(value, row) {

        var template = 	'<div class="dropdown js_fsrChangeStatus" data-status="fsrStatus-'+ value +'" data-target-fsr="'+ row.id +'">' +
				            '<button type="button" class="btn btn-sm btn-status btn-block dropdown-toggle" data-toggle="dropdown">' +
				                '<span class="js_fsrStatusLabel">'+ FSR_TABLE.display_text(value) +'</span> <i class="zmdi zmdi-chevron-down zmdi-hc-lg"></i>' +
				            '</button>' +
				            '<div class="dropdown-menu dropdown-mapping">' +
				                '<div class="btn-group btn-group-vertical btn-group-set-status" data-toggle="buttons">' +
				                    '<label for="fsrStatusDraft-'+ row.id +'" class="btn'+ FSR_TABLE.active.draft(value)+'">' +
				                    	'<input type="radio" id="fsrStatusDraft-'+ row.id +'" name="fsrStatus'+ row.id +'" value="draft">Draft' +
				                    '</label>' +
				                    '<label for="fsrStatusLive-'+ row.id +'" class="btn'+ FSR_TABLE.active.live(value)+'">' +
				                    	'<input type="radio" id="fsrStatusLive-'+ row.id +'" name="fsrStatus'+ row.id +'" value="live"> Live' +
				                    '</label>' +
				                    '<label for="fsrStatusHold-'+ row.id +'" class="btn'+ FSR_TABLE.active.hold(value)+'">' +
				                    	'<input type="radio" id="fsrStatusHold-'+ row.id +'" name="fsrStatus'+ row.id +'" value="hold"> On Hold' +
				                    '</label>' +
				                    '<label for="fsrStatusArchive-'+ row.id +'" class="btn'+ FSR_TABLE.active.archive(value)+'">' +
				                    	'<input type="radio" id="fsrStatusArchive-'+ row.id +'" name="fsrStatus'+ row.id +'" value="archive"> Archived' +
				                    '</label>' +
				                '</div>' +
				            '</div>' +
				        '</div>';

        return template;
	},
	render_actions: function(value) {
		var template = 	'<div class="text-center">' +
			                '<div class="btn-group btn-group-sm">' +
			                    '<button type="button" class="btn btn-default" data-toggle="tooltip" data-title="Edit FSR"' +
			                    	'onclick="FSR_TABLE.edit_fsr('+ '\''+ value + '\''+')">' +
			                        '<i class="zmdi zmdi-edit zmdi-hc-lg"></i>' +
			                    '</button>' +
			                    '<button type="button" class="btn btn-default" data-toggle="tooltip" data-title="Duplicate FSR"' +
			                    	'onclick="FSR_TABLE.copy_fsr('+ '\''+ value + '\''+')">' +
			                        '<i class="zmdi zmdi-copy zmdi-hc-lg"></i>' +
			                    '</button>' +
			                '</div>' +
			            '</div>'

		return template;
	},
	render_link: function(value) {
		var template = '<a href="'+value+'">'+value+'</a>'
		return template;
	}
}

$(document).ready(function () {
	FSR_TABLE.build();
});;