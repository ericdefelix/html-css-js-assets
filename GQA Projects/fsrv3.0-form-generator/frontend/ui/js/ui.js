// Modified Bootstrap dropdown so it doesnt close if clicked 
var DROPDOWN_MANUAL = {
    obj: [],
    doc: $(document),
    bind: function () {
        this.doc.on('click', '[data-toggle="dropdown-custom"]', function (e) {
            e.preventDefault();
            DROPDOWN_MANUAL.toggle_menu($(this));
        });

        this.doc.on('click', 'html', function (event) {
            var dd = $(event.target).closest('.dropdown');

            if (dd.length == 0) {
                $('.dropdown').removeClass('open');
                DROPDOWN_MANUAL.obj = [];
            }
        });
    },

    toggle_menu: function (elem) {
        var dd = elem.parent('.dropdown');

        dd.addClass('open');
        DROPDOWN_MANUAL.obj.push(elem);

        if (DROPDOWN_MANUAL.obj.length == 2) {
            DROPDOWN_MANUAL.obj[0].parent('.dropdown').removeClass('open');
            DROPDOWN_MANUAL.obj[0].prev().remove();
        }
        if (DROPDOWN_MANUAL.obj.length > 2) {
            DROPDOWN_MANUAL.obj.shift();
            DROPDOWN_MANUAL.obj[0].parent('.dropdown').removeClass('open');
            DROPDOWN_MANUAL.obj[0].prev().remove();
        }
    },

    close_opened_menu: function (elem) {
        DROPDOWN_MANUAL.obj = [];
        elem.parent('.dropdown').removeClass('open');
        elem.remove();
    },
    build: function () {
        this.bind();
    }
}

// Toggle elements menu
var TOGGLE_MENU = {
    hidden      : false,
    doc         : $(document),
    sidebar     : $('#sidebarParent'),
    btn         : $('#btnSidebar'),
    scroll      : $('[data-sidebar-scroll]'),
    expand      : $('.js_expandMenu'),
    container   : $('.fsr-interface-create'),
    sb_width    : 0,
    prev_elem   : undefined,

    init: function() {
        this.sb_width = this.sidebar.outerWidth();

        TOGGLE_MENU.doc.on('click', '#btnSidebar', function() {
            TOGGLE_MENU.hidden == false ? TOGGLE_MENU.hide($(this)) : TOGGLE_MENU.show($(this));

            $(this).addClass('js_no-spam-clicking'); // no double click if sidebar is sliding
        });

        TOGGLE_MENU.doc.on('click', '.js_expandMenu', function(){  TOGGLE_MENU.display_sublist($(this)); });

        // Apply Custom Scrollbar
        TOGGLE_MENU.doc.find('[data-sidebar-scroll]').mCustomScrollbar({
            autoHideScrollbar:  false,
            axis:               'y',
            contentTouchScroll: true,
            scrollbarPosition:  'inside',
            scrollInertia:      200,
            scrollButtons:      {enable: false},
            theme:              'dark',
            live:               true,
            advanced:           { updateOnContentResize: true }
        });

        TOGGLE_MENU.resize();
    },
    set_height: function() {
        this.scroll.css('height', TOGGLE_MENU.sidebar.height() - (38+15) );
    },
    resize: function() {
        var resizeTimer;
        window.addEventListener("resize", function(){
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(
                function(){
                    TOGGLE_MENU.doc.find('[data-sidebar-scroll]').mCustomScrollbar('update');
                    TOGGLE_MENU.set_height();
                }, 200);
        });
    },
    hide: function(btn) {
        TOGGLE_MENU.sidebar.velocity(
            { right: [-(TOGGLE_MENU.sb_width-15),'0'] }, 
            {
                begin: function() {
                    TOGGLE_MENU.scroll.velocity({ opacity: 0 },{ duration: 300, easing: 'linear' });
                },
                duration: 500, 
                easing: [.99,.01,.33,1],
                complete: function(){
                    TOGGLE_MENU.sidebar.addClass('sidebar-hidden');
                    TOGGLE_MENU.hidden = true;
                    btn.removeClass('js_no-spam-clicking');
                    TOGGLE_MENU.container.addClass('js_adjust-content')
                }
            }
        );
    },

    show: function(btn) {
        TOGGLE_MENU.scroll.velocity({ opacity: [1,0] },{ display: 'block', duration: 300, delay: 400, easing: 'linear' });
        TOGGLE_MENU.sidebar.velocity(
            { right: ['0',-TOGGLE_MENU.sb_width] },
            {
                begin: function() {
                    TOGGLE_MENU.scroll.hide();
                    TOGGLE_MENU.sidebar.removeClass('sidebar-hidden');
                    TOGGLE_MENU.hidden = false;
                },
                complete: function() { 
                    btn.removeClass('js_no-spam-clicking');
                    TOGGLE_MENU.container.removeClass('js_adjust-content')
                },
                display: 'block', 
                duration: 500, 
                easing: [.99,.01,.33,1],
            }
        );
    },

    display_sublist: function(target) {
        if (target.hasClass('open')) {
            return false
        }
        else {
            target.addClass('js_no-spam-clicking');
            target.find('.arrowhead').velocity({rotateZ: '90deg'},{duration: 300, easing: [.4,.31,.33,1] });
            target.siblings('.js_sidebarSubList').show();
            target.siblings('.js_sidebarSubList').velocity({ 
                translateY: ['0%','-5%'], 
                opacity: [1,0]}, 
                {   
                    duration: 500, easing: [.4,.31,.33,1],
                    complete: function() {
                        TOGGLE_MENU.prev_elem = target;
                        target.addClass('open');
                        target.removeClass('js_no-spam-clicking');
                    }
                }
            );
            if (TOGGLE_MENU.prev_elem != undefined) {
                TOGGLE_MENU.prev_elem.removeClass('open');
                TOGGLE_MENU.prev_elem.siblings('.js_sidebarSubList').hide();
                TOGGLE_MENU.prev_elem.find('.arrowhead').velocity('reverse');
            }
        }
    },

    build: function() {
        if (TOGGLE_MENU.sidebar.length) {
            this.set_height();
            this.init();
        }
    }
}

var NOTIF = {
    doc: $(document),
    show: function(status, message) {
        $('.notif').attr('data-status',status);
        $('#notifMessage').html(message);

        this.doc.find('.notif')
        .velocity('stop')
        .velocity(
        {
            translateY: ['0','-20px'],
            opacity: [1,0],
        },
        {
            duration: 400,
            delay: 400,
            display: 'block',
            easing: [0, 0.96, 0.86, 0.99],
            complete: function() {
                $(this).velocity({ opacity: 0 },{
                    duration: 2000,
                    display: 'none',
                    easing: [0, 0.96, 0.86, 0.99],
                    delay: 3500,
                });
            }
        });
    },
    build: function() {
        this.doc.ready(function() {
            $('body').append('<div class="notif" data-status="" style="display: none;"><span class="notif-message" id="notifMessage"></span></div>');
        });
    }
}

var LOADER = {
    elem: $('.loader'),
    doc: $(document),
    width: null,
    height: null,
    bind: function() {
        LOADER.width = $(window).width();
        LOADER.height = $(window).height();

        $(window).resize(function() {
            $('.wrap').width(LOADER.width).height(LOADER.height);
        });
    },
    template: function(message) {
        var msg = message;
        if (message === undefined) { msg = 'working ...'; }
        return '<section class="loader"><div class="wrap"><div><i class="zmdi zmdi-settings zmdi-hc-spin"></i>' + msg + '</div></div></section>';
    },
    show: function(message) {
        this.bind();
        $(LOADER.template()).appendTo('body');
        LOADER.elem.show();

        this.doc.find('.wrap').width(LOADER.width).height(LOADER.height);
    },
    hide: function() {
        LOADER.elem = $('.loader');
        LOADER.elem.hide();
    }
}


var UTILS = {
    settings: {},
    url: function(endpoint) {
        var url = 'http://fsrgen.gqaustralia.com.au/' + endpoint;
        // var url = this.settings.apiUrl+'/' + endpoint;
        return url;
    },
    build: function() {
        $('body').tooltip({ 
            selector: '[data-toggle="tooltip"]', 
            container: $('body'), 
            animation: false,
            placement: 'left'
        });

        // Get current year for the footer
        (function(){
            var d = new Date(), n = d.getFullYear();
            $('#currentYear').html(n);            
        })();

        // Copy to clipboard
        if ($('.btn-copy').length) {
            (function(){
                var clipboard = new Clipboard('.btn-copy');

                clipboard.on('success', function(e) {
                    $(e.trigger).closest('.field-copy-url').addClass('copied');

                    var t;
                    t = setTimeout(function() {
                        $(e.trigger).closest('.field-copy-url').removeClass('copied');
                    }, 2000);
                    e.clearSelection();
                });

                clipboard.on('error', function(e) {
                    console.error('Action:', e.action);
                });
            })();
        }

        // LOADER.build();
        TOGGLE_MENU.build();
        DROPDOWN_MANUAL.build();
        LOADER.bind();
        NOTIF.build();
    }
}
$.ajax({
  dataType: "json",
  url: "settings.json",
  async: false,
  success: function(d){
      var env = d.environment || "production";
      UTILS.settings = d[env] || d;
  }
});
// Call functions 
$(document).ready(function() {
    UTILS.build();
});