'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FSRMeta = function () {
    function FSRMeta() {
        _classCallCheck(this, FSRMeta);

        // console.log('started');
        this.init();
    }

    _createClass(FSRMeta, [{
        key: 'init',
        value: function init() {
            this.fsrId = parseInt(localStorage.getItem('fsrid'));
            this.token = localStorage.getItem('token');
            if (!this.fsrId || this.fsrId == 0) {
                alert('Missing FSR Id');
            }
            if (!this.token || this.token == '') {
                alert('Missing token. Please login again');
            }
            // console.log(this.token, this.fsrId);
            this.setupDZ();
            this.setDisplay();
            
            /*
             this.token = this.login ((TOKEN)=> {
                 this.fsrId = 21;
                 this.token = TOKEN;
                 console.log(this.token);
                 this.setupDZ();
                 if (!this.fsrId || this.fsrId == 0) {
                     alert ('Missing FSR Id');
                 }
                 if (!this.token || this.token == '') {
                     alert ('Missing token. Please login again');
                 }
             })        
             */
        }
    }, {
        key: 'login',
        value: function login(callback) {
            $.ajax({
                url: UTILS.url('login'),
                type: 'post',
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                data: {
                    "username": '',
                    "password": ''
                }
            }).done(function (token) {
                callback(token.token);
            });
        }
    }, {
        key: 'getFSR',
        value: function getFSR(callback) {
            $.ajax({
                url: UTILS.url('fsr'),
                type: 'get',
                data: {
                    "id": this.fsrId,
                    "token": this.token
                }
            }).done(function (response) {
                callback(response);
            });
        }
    }, {
        key: 'updateStatus',
        value: function updateStatus(status) {
            var _this = this;

            LOADER.show();
            this.status = status;
            $.ajax({
                url: UTILS.url('fsr'),
                type: 'post',
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                data: {
                    "id": this.fsrId,
                    "token": this.token,
                    "action": "updatestatus",
                    "status": status
                }
            }).done(function (res) {
                _this.setDisplay();
                FSR_MODEL.status = this.status;
            }).always(function () {
                LOADER.hide();
            }).fail(function (){
               // alert('There has been an erro updating status');
                _this.setDisplay();
            });
        }
    }, {
        key: 'setDisplay',
        value: function setDisplay() {
            var _this2 = this;

            this.getFSR(function (response) {
                if (!$('.status-' + response.data.status).hasClass('active')) {
                    $('.status-' + response.data.status).addClass('active');
                }
                _this2.status = response.data.status;
                _this2.name = response.data.name;
                _this2.url = response.data.url;
                _this2.gtm_tag = response.data.gtm_tag;
                $('#copyTextURL').val(_this2.url);
                $('#copyTextName').val(_this2.name);
                $('#gtmTag').val(_this2.gtm_tag);
                if (response.data.bg_image != '') {
                    $('#fsrBGPreview').css('background-image', 'url(' + response.data.bg_image + ')');
                    $('#removeBG').show();
                } else {
                    $('#fsrBGPreview').css('background-image', 'url("ui/img/no-bg.jpg")');
                    $('#removeBG').hide();
                }
            });
        }
    }, {
        key: 'removeBG',
        value: function removeBG() {
            var parent = this;
            LOADER.show();
            $.ajax({
                url: UTILS.url('fsr'),
                type: 'post',
                data: {
                    "token": this.token,
                    "id": this.fsrId,
                    "_method": "DELETE"
                }
            }).done(function (res) {
                LOADER.hide();
                parent.setDisplay();
            }).fail(function () {
                LOADER.hide();
                parent.setDisplay();
                alert('There has been an error removing the background');
            });
        }
    }, {
        key: 'setupDZ',
        value: function setupDZ() {
            var parent = this;
            this.drop = new Dropzone("div#fsrBackgroundImage", {
                method: 'POST',
                paramName: 'image',
                url: "handleImg.php",
                clickable: true,
                maxFiles: 1,
                autoDiscover: false,
                acceptedFiles: 'image/jpeg',
                addRemoveLinks: 'false',
                dictRemoveFile: '',
                sending: function sending(file, xhr, formData) {
                    LOADER.show();
                    formData.append('token', parent.token);
                    formData.append('id', parent.fsrId);
                    formData.append('_method', 'PUT');
                },
                success: function success(file, response) {
                    LOADER.hide();
                    console.log(response.id);
                    if (response.response == 200) {
                        parent.setDisplay();
                    } else {
                        parent.setDisplay();
                        // alert ('there has been an error setting up the background')
                    }
                },
                complete: function complete(file) {
                    LOADER.hide();
                    this.removeFile(file);
                }
            });
        }
    }, {
        key: 'checkUnique',
        value: function checkUnique(url) {
            var _this3 = this;

            LOADER.show();
            $.ajax({
                url: UTILS.url('fsr'),
                type: 'get',
                data: {
                    "token": this.token,
                    "key": "url",
                    "value": url,
                    "action": "isunique"
                }
            }).done(function (res) {
                
                $('#copyTextURL').css('color', "red");
                alert('The URL is not unique');
            }).fail(function () {
                // unique 

                $('#copyTextURL').css('color', "green");
                _this3.url = url;
                _this3.updateFSR();
            }).always(function (){
                LOADER.hide();
            })
        }
    }, {
        key: 'updateFSR',
        value: function updateFSR() {

            $.ajax({
                url: UTILS.url('fsr'),
                type: 'post',
                data: {
                    "token": this.token,
                    "id": this.fsrId,
                    "name": this.name,
                    "url": this.url,
                    "gtm_tag": this.gtm_tag,
                    "action": "update",
                    "status": this.status
                }
            }).done(function (res) {
            }).fail(function () {
                alert('There has been an error updating the FSR');                
            }).always(function () {
                LOADER.hide();
            });
        }
    }]);

    return FSRMeta;
}(); // end of class


var meta = new FSRMeta();


$("label[id^='fsrStatus-']").each(function (i, el) {
    var setStatusTo = $('#' + el.id).attr('data-status');
    $('#' + el.id).click(function () {
        console.log(FSR_MODEL.status);
        if (FSR_CREATE.validate_fsr($(this).data('status'),$(this).index()) === false) {
            return false;
        }
        else {
            meta.updateStatus(setStatusTo);
        }
    });
});

$('#copyTextURL').on('blur', function (e) {
    if ($('#copyTextURL').val() != meta.url) {
        meta.checkUnique($('#copyTextURL').val());
    }
});
$('#copyTextName').on('blur', function () {
    if ($('#copyTextName').val()!= '') {
        if($('#copyTextName').hasClass('highlight'))
        { 
            $('#copyTextName').removeClass('highlight');
        }
        if ($('#copyTextName').val() != meta.name) {
            meta.name = $('#copyTextName').val();
            meta.updateFSR();
        }
    } else { 
        // NOTIF.show('info','FSR Name is required');
        $('#copyTextName').addClass('highlight');
        $('#copyTextName').focus();
    }
});

$('#removeBG').click(function () {
    meta.removeBG();
});


   
