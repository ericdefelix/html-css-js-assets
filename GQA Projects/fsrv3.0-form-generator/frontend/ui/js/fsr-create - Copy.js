var MAPPING_DATA = {};

// For easier sending of FSR pages in bulk
var FSR_MODEL = {
    fsr_id: 0 ,
    name: '',
    token: '',
    status: '',
    created_by: '',
    created_date: '',
    action: '',
    url: '',
    data: []
}

var NEW_PAGE = {
    sort_order: '',
    gtm_value: '',
    questions: []
}

var NEW_QUESTION = {
    title: '',
    sub_title: '',
    content: '',
    type: '',
    button_text: '',
    placeholder: '',
    validations: { required: true },
    mapped_to: '',
    answers: []
}

var unmapped_items = [];

var FSR_CREATE = {
    doc: $(document),
    must_save: false,
    temp_order: [], // Temporary storage of indices from sortable elements
    init: function() {
        FSR_CREATE.get_fsrid_token(); // Set FSR 
        FSR_CREATE.get_mappings(); // Create mapping reference 

        // When Tile-select is added to screen, binds Upload attachment behavior
        // this.doc.on('change', '.js_attachThumbnail', function() { FSR_CREATE.bind_option_thumbnails($(this),this); });

        // If sidebar is collapsed and element row is clicked display sidebar
        this.doc.on('click', '.fsr-canvas-group-item', function() {
            var $item = $('.fsr-canvas-group-item');
            if (TOGGLE_MENU.hidden == true ) { 
                $('#btnSidebar').click();
                $item.css('cursor','pointer');
            }
            else {
                $item.removeAttr('style');
            }
        });


        window.onbeforeunload = function (e) {
            e = e || window.event;
            // For IE and Firefox prior to version 4
            if(FSR_CREATE.must_save === true) {
                if (e) { e.returnValue = 'Sure?'; }
                // For Safari
                return 'Sure?';
            }
        };


    },
    get_fsrid_token: function() {
        FSR_MODEL.fsr_id = parseInt(localStorage.getItem('fsrid'));
        FSR_MODEL.token = localStorage.getItem('token');
    },
    get_mappings: function() {
        LOADER.show('working...');

        $.ajax({
            url: UTILS.url('mapping'),
            type: 'get',
            data: {token: FSR_MODEL.token}
        })
        .done(function(data) {
            MAPPING_DATA = data;
            FSR_CREATE.get_screens(); // Check if FSR already has pages
        })
        .fail(function(data) {
            if (data.status === 404) {
                NOTIF.show('danger','Session expired. Please log out and log in again');
            }
            else {
                NOTIF.show('danger','Something went wrong while pulling CRM Mapping');
            }
        })
        .always(function() {
            LOADER.hide();
        })
    },
    bind_option_thumbnails: function(input,elem) {
        var fr = new FileReader;
        var width = 100, height = 100; // Set a fixed dimension for thumbnails. Can be changed anytime
        var preview_container = input.siblings('.js_attachmentPreview');

        fr.onload = function() {
            var img = new Image;
            
            img.onload = function() { 
                $.ajax({
                    url: img.src, 
                    async: false, 
                    success: function(result){
                        preview_container.append("<img src='" + img.src + "' />").addClass('attached');
                        preview_container.children('img').css('width',preview_container.width());
                    }
                });
            };
            
            img.src = fr.result;
        };

        fr.readAsDataURL(elem.files[0]);
    },
    bind_drag_to_widgets: function() {
        // Make the widgets draggable
        $(".sidebar-list li").draggable({
            appendTo: "body",
            helper: "clone",
            scroll: false,
            refreshPositions: true,
            start: function(e,ui) {
                $('body').css('overflow-x', 'hidden'); // remove body scroll when draggable is out of bounds
                ui.helper.addClass("ui-draggable-widget-helper"); // styles the floating element
            },
            stop: function() {
                $('body').removeAttr('style'); // remove overflow hidden
            }
        });
    },
    bind_canvas_interactions: function() {
        // Make widgets droppable and sortable
        $(".fsr-canvas-group-item .fsr-canvas-row")
            .droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ':not(.ui-sortable-helper)',
                drop: function(event, ui) {
                    FSR_CREATE.update_screen($(this),ui.helper); // On drop gets an ID from the server then render it if successful
                    FSR_CREATE.must_save = true;
                }
            })
            .sortable({
                placeholder: 'ui-state-highlight',
                start: function(e,ui){
                    ui.placeholder.addClass('sortable-placeholder');
                    $('.sortable-placeholder').height(ui.helper.outerHeight()); // get height of placeholder to avoid quirky container resizing

                    $('body').css('overflow-x', 'hidden'); // remove body scroll when draggable is out of bounds

                    // Store old index of the element being sorted
                    FSR_CREATE.temp_order.length = 0; // clear temp storage first
                    FSR_CREATE.temp_order.push(ui.item.index()); // push index

                },
                stop: function(e,ui) {
                    $('body').removeAttr('style'); // remove overflow hidden
                },
                sort: function () {
                    $(this).removeClass("ui-state-default");
                },
                update: function(e,ui) {
                    // Store new index of the element being sorted
                    FSR_CREATE.temp_order.push(ui.item.index()); // push index
                    FSR_CREATE.refresh_elements($(this),true); // refresh model and element IDs
                }
        });
    },
    bind_save_question_text() {

        this.doc.on('blur', '.text-h1', function() {
            var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
                elem_index      = parseInt($(this).closest('[data-element-id]').index());

                FSR_MODEL.data[screen_index].questions[elem_index].title = $(this).val();
        });

        this.doc.on('blur', '.text-h2', function() {
            var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
                elem_index      = parseInt($(this).closest('[data-element-id]').index());

                FSR_MODEL.data[screen_index].questions[elem_index].sub_title = $(this).val();
        });

        this.doc.on('blur', '.text-normal', function() {
            var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
                elem_index      = parseInt($(this).closest('[data-element-id]').index());

                FSR_MODEL.data[screen_index].questions[elem_index].content = $(this).val();
        });

        this.doc.on('blur', '.text-input-placeholder', function() {
            var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
                elem_index      = parseInt($(this).closest('[data-element-id]').index());

                FSR_MODEL.data[screen_index].questions[elem_index].placeholder = $(this).val();
        });

        this.doc.on('blur', '.text-options', function() {
            var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
                elem_index      = parseInt($(this).closest('[data-element-id]').index()),
                answer_index    = parseInt($(this).closest('.fsr-widget').index());

            FSR_MODEL.data[screen_index].questions[elem_index].answers[answer_index].title = $(this).val();
        });

        this.doc.on('blur', '.text-list', function() {
            var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
                elem_index      = parseInt($(this).closest('[data-element-id]').index()),
                answer_index    = parseInt($(this).closest('.fsr-widget').index());

            FSR_MODEL.data[screen_index].questions[elem_index].answers = FSR_CREATE.parse_list($(this).val());
        });
    },
    parse_list: function(string) {
        var select_items = [],
            split_string = [];

        split_string = string.split('\n');

        for (var i = 0; i <= split_string.length - 1; i++) { select_items.push({ title: split_string[i] }) }
        return select_items;
    },
    clear_screens: function() {
        $('#listScreenNavigator').children().remove();
        $('#fsrCanvasGroup').children().remove();
    },
    get_screens: function() {
        $.ajax({
            url: UTILS.url('page'),
            type: 'get',
            data: { fsr_id: FSR_MODEL.fsr_id , token: FSR_MODEL.token },
        })
        .done(function(data) {

            // Update FSR MODEL - Temporary container for our pages
            if (data.response === 200) {
                FSR_MODEL.created_by = data.created_by;
                FSR_MODEL.created_date = data.created_date;
                FSR_MODEL.status = $('label[data-status].active').data('status');
                FSR_MODEL.name = $('#copyTextName').val();
                FSR_MODEL.url = $('#copyTextURL').val();

                FSR_MODEL.data = data.data;

                // Render the screens together with the questions
                FSR_CREATE.render_screens(data);
                FSR_CREATE.must_save = false;
            }
        })
        .fail(function(data){
            if (data.status === 404) {
                NOTIF.show('danger','Session expired. Please log out and log in again');
            }
        })
        .always(function(data) {
            LOADER.hide();
        });
    },
    render_screens: function(get_screens_response) {
        var pcount = FSR_MODEL.data.length,
            qcount = 0,
            acount = 0,
            screen_id = null,
            type = '',
            elem_id = null,
            answers = [];

            var title, subtitle,content,placeholder,mapping,mapping_type;

        //Render UI elements
        for (var i = 0; i <= pcount - 1; i++) {
            FSR_CREATE.add_screen_dom(FSR_MODEL.data[i]);
            screen_id = FSR_MODEL.data[i].id;

            if(!FSR_MODEL.data[i].hasOwnProperty('questions')) {
                FSR_MODEL.data[i].questions = [];
            }

            qcount = FSR_MODEL.data[i].questions.length;

            for (var j = 0; j <= qcount - 1; j++) {
                type = FSR_MODEL.data[i].questions[j].type;
                elem_id = FSR_MODEL.data[i].questions[j].id;

                title           = FSR_MODEL.data[i].questions[j].title;
                subtitle        = FSR_MODEL.data[i].questions[j].sub_title; 
                content         = FSR_MODEL.data[i].questions[j].content;
                placeholder     = FSR_MODEL.data[i].questions[j].placeholder;
                mapping         = FSR_MODEL.data[i].questions[j].attribute;
                ui_type         = FSR_MODEL.data[i].questions[j].type;

                if (typeof FSR_MODEL.data[i].questions[j].answers !== 'undefined') {
                    answers = FSR_MODEL.data[i].questions[j].answers;
                }

                FSR_CREATE.add_element(screen_id,type,elem_id,answers,ui_type,title,subtitle,content,mapping,placeholder);
            }
        }
    },
    add_screen: function() {
        LOADER.show('working...');

        $.ajax({
            url: UTILS.url('page'),
            type: 'post',
            data: {
                token : FSR_MODEL.token,
                fsr_id : FSR_MODEL.fsr_id,
                data: JSON.stringify(NEW_PAGE)
            }
        })
        .done(function(response) {
            FSR_CREATE.add_screen_dom(response); // Now create the DOM
            FSR_MODEL.data.push({ id: response.id, sort_order: '', gtm_value: '', questions: [] });
        })
        .fail(function(data) {
            if (data.status === 404) {
                NOTIF.show('danger','Session expired. Please log out and log in again');
            }
            else {
                NOTIF.show('danger', 'Something went wrong while adding a new page');
            }
        })
        .always(function(){
            LOADER.hide();
        });
    },
    add_screen_dom: function(response) {
        var $canvasGroup = $('#fsrCanvasGroup');
        var $listScreenNavigator = $('#listScreenNavigator');

        // Screen container markup and Screen list item markup
        var canvas_html =   '<li class="fsr-canvas-group-item" data-screen-id="'+ response.id +'">' +
                                '<div class="fsr-canvas-row"></div>' +
                                '<button type="button" class="btn btn-default btn-sm btn-remove" onclick="FSR_CREATE.remove_screen(this,'+response.id+')">' +
                                '<i class="zmdi zmdi-delete"></i> Remove</button>' +
                            '</li>';

        var nav_html = '<li class="list-screen-item" onclick="FSR_CREATE.scroll_to(this)">Screen</li>';

        // add screen to canvas
        $canvasGroup.append(canvas_html);

        // add item on screen navigator with a little bit of animated visual feedback
        $(nav_html)
            .appendTo($listScreenNavigator)
            .velocity({ scaleX: 1, scaleY: 1 },
            {
                begin: function() { 
                    var prop = 'scaleX(0) scaleY(0)';
                    $(this).css({ "webkitTransform": prop, "MozTransform": prop, "msTransform": prop, "OTransform": prop, "transform": prop });
                },
                duration: 400,
                easing: [0, 0.96, 0.86, 0.99]
            });

        FSR_CREATE.bind_canvas_interactions(); // re-initialize for dynamically created screens

        // display Screens tab if not active on Add Screen
        if (!$('#screensPanel').hasClass('active')) $('[href="#screensPanel"]').click();
    },
    remove_screen: function(target_btn,screen_id) {
        var ask_delete = confirm('Are you sure you want to remove this screen?');
        var screen_index = parseInt($(target_btn).closest('[data-screen-id]').index());

        if (ask_delete == true) {

            // update FSR Model
            FSR_MODEL.data.splice(screen_index,1);
            FSR_CREATE.remove_screen_dom(target_btn, screen_index);


            FSR_CREATE.submit_fsr();
        }
        else {
            return false;
        }
    },
    remove_screen_dom: function(target_btn,screen_index) {

        $(target_btn).closest('.fsr-canvas-group-item').remove(); // remove the target screen
        $('#listScreenNavigator').find(':eq('+ screen_index +')').remove(); // also remove the screen item on the list

        FSR_CREATE.must_save = true;
    },
    update_screen: function(target_fsr_row,dropped_element) {
        LOADER.show();

        // Get widget type of dropped element
        var screen_id =     parseInt(target_fsr_row.parent().data('screen-id')),
            type =          dropped_element.data('generic-type'),
            ui_type =       dropped_element.data('type'),
            screen_index =  target_fsr_row.parent().index(),

            // Will serve as reference for array item searching if page is not yet refreshed
            temp_elem_id =  parseInt(dropped_element.closest('[data-element-id]'));

        var update_request_format = {
            sort_order: '', 
            gtm_value: '',
            questions: []
        }

        var answer_count = 2; // minimum count for answers

        if (FSR_MODEL.data[screen_index].hasOwnProperty('questions') === false) {
            FSR_MODEL.data[screen_index].questions = [];
        }
 
        FSR_MODEL.data[screen_index].questions.push(NEW_QUESTION);
        update_request_format.questions.push(NEW_QUESTION);

        var len = parseInt(FSR_MODEL.data[screen_index].questions.length - 1);


        FSR_MODEL.data[screen_index].questions[len].type = type;

        if (type === 'radio' || type === 'checkbox') {
            FSR_MODEL.data[screen_index].questions[len].answers = [{ title: "Choice 1" },{ title: "Choice 2" }];
        }
        
        update_request_format.questions = FSR_MODEL.data[screen_index].questions;

        // console.log(update_request_format.questions);

        $.ajax({   
            url: UTILS.url('page'),
            type: 'post',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
            data: {
                _method: 'PATCH',
                token: FSR_MODEL.token,
                fsr_id : FSR_MODEL.fsr_id,
                page_id: screen_id,
                data: JSON.stringify(update_request_format)
            }
        })
        .done(function(response) {
            // Update Model
            FSR_MODEL.data[screen_index].questions = response.data[0].questions;

            // Render in DOM
            FSR_CREATE.add_element(screen_id,type,temp_elem_id,answer_count,ui_type);

            //Open dropdown menu upon add so that user will notice
            target_fsr_row.find('[data-element-id="'+ temp_elem_id +'"] [data-toggle="dropdown-custom"]').click();
        })
        .fail(function(response) {
            if (response.response === 404) {
                NOTIF.show('danger','Session expired. Please log out and log in again');
            }
            else {
                NOTIF.show('danger', 'Something went wrong while updating the screen');
            }
            LOADER.hide();
        })
        .always(function(response) {
            LOADER.hide();
        });
    },
    add_element: function(screen_id,type,elem_id,answers, ui_type, title, subtitle,content,mapping,placeholder) {

        var target_fsr_row = $('*[data-screen-id="'+screen_id+'"]').find('.fsr-canvas-row');


        // Action Controls Template // wa = widget actions
        var elem_remove  = '<button type=button" class="btn btn-transparent btn-sm btn-remove" data-toggle="tooltip" data-title="Remove Question"' +
                                'onclick="FSR_CREATE.remove_element(this)"><i class="zmdi zmdi-minus-circle"></i>' +
                            '</button>',

            elem_add     = '<button type="button" class="btn btn-transparent btn-sm" data-toggle="tooltip" data-title="Add Tile"' +
                                'onclick="FSR_CREATE.add_option(this)"><i class="zmdi zmdi-collection-plus"></i>' +
                            '</button>',

            elem_mapping = '<div class="dropdown" data-toggle="tooltip" data-title="Map Question">' +
                                '<button type="button" class="btn btn-transparent btn-sm btn-map dropdown-toggle" data-toggle="dropdown-custom">' + 
                                    '<i class="zmdi zmdi-settings" data-mapping-indicator="'+get_mapping_value()+'"></i>' +
                                '</button>' +
                                '<div class="dropdown-menu open pull-right dropdown-mapping">' +
                                    '<div class="btn-group btn-group-vertical" data-toggle="buttons">'+ create_mapping_markup() +'</div>' +
                                '</div>' +
                            '</div>';

        // Widget Template
        var element_html =  '<section class="fsr-region'+ is_mappable() +'"' + $.trim('data-element-id="'+elem_id+'">') + 
                                '<div class="fsr-widget" data-widget-type="'+ type +'">' +
                                    '<i class="widget-drag-handle"></i>'+
                                    '<div class="widget-input">'+
                                        create_element_label() +
                                        '<input type="text" class="text-h1" value="'+get_title()+'" placeholder="Main Text ..." id="title'+elem_id+'">'+
                                        '<input type="text" class="text-h2" value="'+get_subtitle()+'" placeholder="Secondary Text ..." id="subtitle'+elem_id+'">'+
                                        '<input type="text" class="text-normal" value="'+get_content()+'" placeholder="Optional Text ..." id="content'+elem_id+'">'+
                                        '<input type="text" class="text-input-placeholder" value="'+get_placeholder_text()+'" placeholder="Input Placeholder Text ..." id="button'+elem_id+'">'+
                                        has_options() +
                                    '</div>' +
                                    '<div class="widget-actions">' + create_action_buttons() + '</div>'
                                '</div>' +
                            '</section>';

        // Template Helpers
        function create_element_label() {

            if (type == '') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-normal"></i> Text Content</span></h6>';
            }
            else if (ui_type == 'text-box' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-textbox"></i> Text Box</span></h6>';
            }
            else if (ui_type == 'text' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-textbox"></i> Text Box</span></h6>';
            }
            else if (ui_type == 'text area' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-textbox"></i> Text Area</span></h6>';
            }
            else if (mapping == 'Email' && type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-email"></i> Email Box</span></h6>';
            }
            else if (ui_type == 'text-url' || type == 'upload') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-upload"></i> Upload Box</span></h6>';
            }
            else if (mapping == 'Mobile' && type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-contact"></i> Contact Number Box</span></h6>';
            }
            else if (ui_type == 'select-dropdown' || type == 'select') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-list"></i> List</span></h6>';
            }
            else if (ui_type == 'checkbox-select' || type == 'checkbox') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-tile-multiple"></i> Tile Select <small class="text-uppercase">Multiple</small></span></h6>';
            }
            else if (ui_type == 'radio-select' || type == 'radio') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-tile-single"></i> Tile Select <small class="text-uppercase">Single</small></span></h6>';
            }
            else {
                return '';
            }
        }

        function get_title() { if (typeof title === 'undefined') { return '' } else { return title; } }
        function get_subtitle() { if (typeof subtitle === 'undefined') { return '' } else { return subtitle; } }
        function get_content() { if (typeof content === 'undefined') { return '' } else { return content; } }
        function get_placeholder_text() { if (typeof placeholder === 'undefined') { return '' } else { return placeholder; } }
        function get_list() { 
            if (typeof answers === 'undefined') { return '' } 
            else {
                var arr = [];
                for (var i = 0; i <= answers.length - 1; i++) { 
                    arr.push(answers[i].title);
                }
                return arr.join('\n');
            }
        }

        function has_options() {
            if (type == 'radio' || type == 'checkbox' || type == 'radio-select' || type == 'checkbox-select') {
                return '<div class="options">'+FSR_CREATE.option_markup(answers)+'</div>';
            }
            else if (type === 'select') { 
                return '<textarea class="text-list" cols="30" rows="4" placeholder="One line per list/select item">'+get_list()+'</textarea>';
            }
            else { 
                return '';
            }
        }
               
        function create_action_buttons() {
            if (type == 'radio-select' || type == 'checkbox-select' || type == 'radio' || type == 'checkbox') { 
                return elem_remove + elem_add + elem_mapping;
            }
            else if (type == 'text-h1' || type == 'text-h2' || type == 'text-normal' || type == 'button') {
                return elem_remove;
            }
            else {
                return elem_remove + elem_mapping;   
            }
        }

        function create_mapping_markup() {

            var count = MAPPING_DATA.count,
                mapping = MAPPING_DATA.data,
                markup = [];

            for (var i = 0; i <= count - 1; i++) {
                markup.push('<label class="btn btn-default '+ is_mapping_active() +'" for="'+ elem_id + 'opt' + i +'"'+
                                'onclick="FSR_CREATE.update_mapping(\''+ 
                                    mapping[i].attribute + '\'' + ',' + 'this' + ',' + mapping[i].id +')">' +
                                '<input type="radio" id="'+ elem_id + 'opt' + i +'" name="opt-'+ elem_id +'"/><span>'+ mapping[i].attribute +'</span>' + 
                            '</label>');
            };
            return markup.join("");
        }

        function is_mappable() {
            if (typeof mapping === 'undefined' || mapping === 'Unmapped' || mapping === 'unmapped') { return ' unmapped'; }
            else { return '' }
        }

        function get_mapping_value() {
            if (typeof mapping === 'undefined') { return ' Unmapped'; }
            else { return mapping }
        }

        function is_mapping_active() {

        }

        // Render the widget container
        target_fsr_row.append(element_html);
    },
    remove_element: function(btn) {
        var screen_id       = parseInt($(btn).closest('[data-screen-id]').data('screen-id')),
            screen_index    = $(btn).closest('[data-screen-id]').index(),
            element_index      = $(btn).closest('[data-element-id]').index();

        var update_request_format = {
            id: screen_id,
            sort_order: '', 
            gtm_value: '',
            id: screen_id,
            questions: []
        }

        // Remove element from FSR model
        FSR_MODEL.data[screen_index].questions.splice(element_index, 1);

        update_request_format.questions = FSR_MODEL.data[screen_index].questions;

        // Update UI
        var row = $(btn).closest('.fsr-canvas-row');

        btn.closest('.fsr-region').remove();
        $('.tooltip').remove();
        FSR_CREATE.submit_fsr();
    },
    refresh_elements: function(row,isSorted) {
        var s_index = row.closest('[data-screen-id]').index(); // Get screen id

        var updated_array = FSR_MODEL.data[s_index].questions; // targeted elements group

        // get indices from a temp object set by jqueryUI's sortable start and update method
        var oldIndex = FSR_CREATE.temp_order[0],
            newIndex = FSR_CREATE.temp_order[1];

        // If sorted
        if (isSorted == true) {
            var swap = function(temp_array, oldIndex, newIndex) {
                var temp = temp_array[oldIndex];
                temp_array[oldIndex] = temp_array[newIndex];
                temp_array[newIndex] = temp;
            };
            swap(updated_array, oldIndex, newIndex);
        };
    },
    refresh_screens: function() {
        FSR_CREATE.clear_screens();
        FSR_CREATE.get_screens();
        // $.each($('#fsrCanvasGroup .fsr-canvas-group-item'), function(index) {
        //     $(this).attr('data-screen-id', 'sid' + index);

        //     FSR_MODEL.data[index].id = 'sid'+index;
        //     FSR_CREATE.refresh_elements($(this).find('.fsr-canvas-row'));
        // });
    },
    update_mapping: function(mapping_name, elem, mapping_id) {
        var parent_elem     = $(elem).closest('[data-element-id]'),
            parent_screen   = $(elem).closest('[data-screen-id]');

        var screen_index    = $(elem).closest('[data-screen-id]').index(),
            element_index   = $(elem).closest('[data-element-id]').index();

        FSR_MODEL.data[screen_index].questions[element_index].mapping_id = mapping_id;
        
        // Update UI status indicator
        parent_elem.removeClass('unmapped');
        parent_elem.find('[data-mapping-indicator]').attr('data-mapping-indicator', mapping_name);

        // FSR_CREATE.update_screens();
    },
    option_markup: function(answers) {
        // debugger
        var answer_count = answers.constructor === Array ? answers.length : answers, 
            options = '',
            template = '';

        for (var i = 0; i <= answer_count-1; i++) {

            template = '<div class="fsr-widget" data-option-id="">' +
                            '<input type="text" class="text-options" value="'+get_choices()+'" placeholder="Enter choice..." />' +
                            '<div class="widget-actions">' + 
                                '<button type="button" class="btn btn-transparent btn-sm" data-toggle="tooltip" data-title="Remove Option" onclick="FSR_CREATE.remove_option(this)">' +
                                    '<i class="zmdi zmdi-minus-circle"></i>' +
                                '</button>' +
                            '</div>' +
                        '</div>';

            options = options + template;

            function get_choices() {
                if (answers.constructor === Array) { return answers[i].title; } 
                else { return 'New Choice'; }
            }
        }
        return options;
    },
    add_option: function(btn) {
        // debugger
        // Update Model
        var elem_index     = $(btn).closest('[data-element-id]').index(),
            screen_index   = $(btn).closest('[data-screen-id]').index();

        FSR_MODEL.data[screen_index].questions[elem_index].answers.push({ title: 'Choice'});

        // Update UI
        $(btn).closest('.fsr-widget').find('.options').append(FSR_CREATE.option_markup(1));

        // this.doc.on('blur', '.text-options', function() {
        //     var screen_index    = parseInt($(this).closest('[data-screen-id]').index()),
        //         elem_index      = parseInt($(this).closest('[data-element-id]').index()),
        //         answer_index    = parseInt($(this).closest('.fsr-widget').index());

        //         FSR_MODEL.data[screen_index].questions[elem_index].answers[answer_index].title = $(this).val();
        // });
    },
    remove_option: function(btn) {
        // Update model
        var elem_index     = $(btn).closest('[data-element-id]').index(),
            screen_index   = $(btn).closest('[data-screen-id]').index(),
            target_index   = $(btn).closest('[data-option-id]').index();

        FSR_MODEL
            .data[screen_index]
            .questions[elem_index]
            .answers.splice(target_index,1);

        // Update UI
        $(btn).closest('.fsr-widget').remove();
        $('.tooltip').remove();
    },
    validate_fsr: function(status,status_index) {
        var flag;

        if ($('#fsrCanvasGroup').children().length === 0 && status === 'live') {
            NOTIF.show('info','You need to have screens before setting it to Live');
            flag = false;
        }
        else if ($('.unmapped').length > 0) {
            NOTIF.show('info','There are still unmapped questions. Please map them first before submitting');
            flag = false;
        }
        else if ($('#copyTextName').val() === '') {
            NOTIF.show('info','Please check the Details tab if FSR Name is left blank');
            flag = false;
        }
        else if ($('#copyTextName').val() === '' && $('.unmapped').length > 0) {
            NOTIF.show('info','You still got unmapped questions and you left the FSR Name blank');
            flag = false;
        }
        else if ($('.fsr-canvas-row:empty').length > 0) {
            if (status === 'live') {
                flag = false;
                NOTIF.show('info','All screens should have questions for this FSR to be Live');
            }
            else if (FSR_MODEL.status === 'live' && status !== 'live') {
                flag = true;
                // NOTIF.show('info','You added a blank screen to a Live FSR. Please set to On Hold before saving');
            }
            else if (typeof status !== 'undefined') {
                flag = false;
                NOTIF.show('info','You added a blank screen to a Live FSR. Please set status to On Hold before saving');
            }
        }
        else if (status === 'live' || FSR_MODEL.status === 'live') {
            var all_empty;
            $('.widget-input').each(function(index) {
                if ($(this).find('.text-h1').val() === '' && $(this).find('.text-h2').val() === '' && $(this).find('.text-normal').val() === '') {
                    NOTIF.show('info','There are still screens that have no text content');
                    flag = false;
                    return false;
                }
            });
        }

        LOADER.hide();
        return flag;
    },
    submit_fsr: function() {

        if (FSR_CREATE.validate_fsr() === false) {
            return false;
        }
        else {
            LOADER.show('working...');

            var pages = FSR_MODEL.data;

            for (var i = pages.length-1; i >= 0; i--) {

                delete pages[i].unique_name;

                var qlen = pages[i].questions.length-1;

                for (var j = qlen; j >= 0; j--) {
                    pages[i].questions[j].mapped_to = parseInt(pages[i].questions[j].mapping_id);

                    delete pages[i].questions[j].xml_equivalent;
                    delete pages[i].questions[j].attribute;
                    delete pages[i].questions[j].mapping_type;
                    delete pages[i].questions[j].mapping_id;
                }
            }

            $.ajax({   
                url: UTILS.url('fsr'),
                type: 'post',
                contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
                data: {
                    action: 'update',
                    token: FSR_MODEL.token,
                    id : FSR_MODEL.fsr_id,
                    name: $('#copyTextName').val(),
                    url: $('#copyTextURL').val(),
                    status: FSR_MODEL.status,
                    pages: JSON.stringify(pages)
                }
            })
            .done(function(response) {
                NOTIF.show('success','FSR Updated');
                FSR_CREATE.must_save = false;

                FSR_CREATE.refresh_screens();
            })
            .fail(function(response) {
                LOADER.hide();
                NOTIF.show('danger','Something went wrong while saving');
            })
            .always(function(response) {
                LOADER.hide();
            });
        }
    },
    cancel: function() {
        window.location.href = 'portfolio.html'; // replace with whatever URL
    },
    preview: function() {
        var url = $('#copyTextURL').val();
        var result = url.substring(url.lastIndexOf("/") + 1) || url;
        window.open(UTILS.settings.fsrUrl+'/index.html?url='+result+'&preview=true','_blank'); // See Preview
    },
    record_unmapped: function() {
        localStorage.setItem('unmapped', JSON.stringify(unmapped_items));
    },
    scroll_to: function(target_screen) {
        var i = $(target_screen).index(),
            screen_position = $('#fsrCanvasGroup li').eq(i).position().top;

        $('[href="#screensPanel"]').click(); // If a screen thumbnail is clicked display screens tab
        $('html').velocity('scroll', { offset: screen_position + 15, duration: 800, easing: [.59,.21,.33,1] }); // scroll to target screen
    },
    build: function() {
        this.init();
        this.bind_drag_to_widgets();
        this.bind_canvas_interactions();
        this.bind_save_question_text();
    }
    
}

$(document).ready(function() {
    FSR_CREATE.build();
});