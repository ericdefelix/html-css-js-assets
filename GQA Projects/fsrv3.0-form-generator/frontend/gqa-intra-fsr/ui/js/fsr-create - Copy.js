var MAPPING = {
    text:       ['First Name','Last Name','Post Code','Qualification','Undefined Field','Resume'],
    select:     ['Industry','Job Role','Undefined Field'],
    radio:      ['Age','Years Exp','Reason for Qualification','Heard about Us','Australia or Overseas Experience','Undefined Field'],
    checkbox:   ['Opt-in Newsletter','Undefined Field']
}

var MAPPING_DATA = {};

// For easier sending of FSR pages in bulk
var FSR_MODEL = {
    fsr_id: 0 ,
    name: '',
    token: '',
    status: '',
    created_by: '',
    created_date: '',
    action: '',
    url: '',
    data: []
}

var NEW_PAGE = {
    sort_order: '',
    gtm_value: '',
    questions: []
}

var NEW_QUESTION = {
    title: '',
    sub_title: '',
    content: '',
    type: '',
    button_text: '',
    validations: { required: true },
    mapped_to: '',
    answers: []
}

var FSR_CREATE = {
    doc: $(document),
    temp_order: [], // Temporary storage of indices from sortable elements
    init: function() {
        FSR_CREATE.get_fsrid_token(); // Set FSR ID
        FSR_CREATE.get_screens(); // Check if FSR already has pages
        FSR_CREATE.get_mappings();

        // When Tile-select is added to screen, binds Upload attachment behavior
        // this.doc.on('change', '.js_attachThumbnail', function() { FSR_CREATE.bind_option_thumbnails($(this),this); });

        // If sidebar is collapsed and element row is clicked display sidebar
        this.doc.on('click', '.fsr-canvas-group-item', function() {
            var $item = $('.fsr-canvas-group-item');
            if (TOGGLE_MENU.hidden == true ) { 
                $('#btnSidebar').click();
                $item.css('cursor','pointer');
            }
            else {
                $item.removeAttr('style');
            }
        });

        this.doc.on('click', '[data-status]', function() {
            FSR_MODEL.status = $(this).data('status');
            console.log(FSR_MODEL);
        });

        // Initialize dropzone.js plugin : For Drag and Drop background image
        // new Dropzone("div#fsrBackgroundImage", { 
        //     method: 'post',
        //     url: "/file/post",
        //     clickable: true,
        //     maxFiles: 1,
        //     autoDiscover: false,
        //     acceptedFiles: 'image/jpeg'
        // });
    },
    get_fsrid_token: function() {
        FSR_MODEL.fsr_id = parseInt(localStorage.getItem('fsrid'));
        FSR_MODEL.token = localStorage.getItem('token');
    },
    get_mappings: function() {
        $.ajax({
            url: UTILS.url('mapping'),
            type: 'get',
            data: {token: FSR_MODEL.token}
        })
        .done(function(data) {
            MAPPING_DATA = data;
        })
    },
    bind_option_thumbnails: function(input,elem) {
        var fr = new FileReader;
        var width = 100, height = 100; // Set a fixed dimension for thumbnails. Can be changed anytime
        var preview_container = input.siblings('.js_attachmentPreview');

        fr.onload = function() {
            var img = new Image;
            
            img.onload = function() { 
                $.ajax({
                    url: img.src, 
                    async: false, 
                    success: function(result){
                        preview_container.append("<img src='" + img.src + "' />").addClass('attached');
                        preview_container.children('img').css('width',preview_container.width());
                    }
                });
            };
            
            img.src = fr.result;
        };

        fr.readAsDataURL(elem.files[0]);
    },
    bind_drag_to_widgets: function() {
        // Make the widgets draggable
        $(".js_sidebarSubList li").draggable({
            appendTo: "body",
            helper: "clone",
            scroll: false,
            refreshPositions: true,
            start: function(e,ui) {
                $('body').css('overflow-x', 'hidden'); // remove body scroll when draggable is out of bounds
                ui.helper.addClass("ui-draggable-widget-helper"); // styles the floating element
            },
            stop: function() {
                $('body').removeAttr('style'); // remove overflow hidden
            }
        });
    },
    bind_canvas_interactions: function() {
        // Make widgets droppable and sortable
        $(".fsr-canvas-group-item .fsr-canvas-row")
            .droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ':not(.ui-sortable-helper)',
                drop: function(event, ui) {
                    FSR_CREATE.update_screen($(this),ui.helper); // On drop gets an ID from the server then render it if successful
                }
            })
            .sortable({
                placeholder: 'ui-state-highlight',
                start: function(e,ui){
                    ui.placeholder.addClass('sortable-placeholder');
                    $('.sortable-placeholder').height(ui.helper.outerHeight()); // get height of placeholder to avoid quirky container resizing

                    $('body').css('overflow-x', 'hidden'); // remove body scroll when draggable is out of bounds

                    // Store old index of the element being sorted
                    FSR_CREATE.temp_order.length = 0; // clear temp storage first
                    FSR_CREATE.temp_order.push(ui.item.index()); // push index

                },
                stop: function(e,ui) {
                    $('body').removeAttr('style'); // remove overflow hidden
                },
                sort: function () {
                    $(this).removeClass("ui-state-default");
                },
                update: function(e,ui) {
                    // Store new index of the element being sorted
                    FSR_CREATE.temp_order.push(ui.item.index()); // push index
                    FSR_CREATE.refresh_elements($(this),true); // refresh model and element IDs
                }
        });
    },
    bind_save_question_text() {

        this.doc.on('blur', '.text-h1', function() {
            var screen_index    = $(this).closest('[data-screen-id]').index(),
                elem_index      = $(this).closest('[data-element-id]').index();

                FSR_MODEL.data[screen_index].questions[elem_index].title = $(this).val();
        });

        this.doc.on('blur', '.text-h2', function() {
            var screen_index    = $(this).closest('[data-screen-id]').index(),
                elem_index      = $(this).closest('[data-element-id]').index();

                FSR_MODEL.data[screen_index].questions[elem_index].sub_title = $(this).val();
        });

        this.doc.on('blur', '.text-normal', function() {
            var screen_index    = $(this).closest('[data-screen-id]').index(),
                elem_index      = $(this).closest('[data-element-id]').index();

                FSR_MODEL.data[screen_index].questions[elem_index].content = $(this).val();
        });

        this.doc.on('blur', '.text-options', function() {
            var screen_index    = $(this).closest('[data-screen-id]').index(),
                elem_index      = $(this).closest('[data-element-id]').index(),
                answer_index    = $(this).closest('[data-option-id]').index();

                FSR_MODEL.data[screen_index].questions[elem_index].answers[answer_index].title = $(this).val();
        });
    },
    populate_text_fields(response) {
        FSR_MODEL.data = response.data;

        var len = FSR_MODEL.data.length - 1;

        for (var i = 0; i <= len; i++) {
            try {
                var qlen = FSR_MODEL.data[i].questions.length - 1;
            } catch(e) {
                return false;
            }

            for (var j = 0; j <= qlen; j++) {
                var target = $('.fsr-canvas-group-item:eq('+i+') .fsr-canvas-row').children('.fsr-region:eq('+j+')');
                
                var mapping = FSR_MODEL.data[i].questions[j].xml_equivalent;

                target.find('.zmdi-settings').attr('data-mapping-indicator',mapping);
                target.removeClass('unmapped');

                target.find('.text-h1').val(FSR_MODEL.data[i].questions[j].title);
                target.find('.text-h2').val(FSR_MODEL.data[i].questions[j].sub_title);
                target.find('.text-normal').val(FSR_MODEL.data[i].questions[j].content);


                // if (FSR_MODEL.data[i].questions[j].answers !== 'undefined') {
                //     try {
                //         var answer_count = FSR_MODEL.data[i].questions[j].answers.length - 1;
                //     } catch(e) {
                //         return false;
                //     }

                //     for (var k = answer_count; k >= 0; k--) {
                //         target.find('.options .fsr-widget:eq('+k+') input').val(FSR_MODEL.data[i].questions[j].answers[k].title);
                //     }
                // }

            }
        }


        // console.log(response);
    },
    get_screens: function() {
        LOADER.show('working ...');

        $.ajax({
            url: UTILS.url('page'),
            type: 'get',
            data: { fsr_id: FSR_MODEL.fsr_id , token: FSR_MODEL.token },
        })
        .done(function(data) {

            // Update FSR MODEL - Temporary container for our pages
            if (data.response === 200) {
                FSR_MODEL.created_by = data.created_by;
                FSR_MODEL.created_date = data.created_date;
                FSR_MODEL.status = $('label[data-status].active').data('status');
                FSR_MODEL.name = $('#copyTextName').val();
                FSR_MODEL.url = $('#copyTextURL').val();

                FSR_MODEL.data = data.data;

                // Render the screens together with the questions
                FSR_CREATE.render_screens();
                FSR_CREATE.populate_text_fields(data);

                console.log(data);
            }
        })
        .fail(function(data){
            if (data.status === 404) {
                alert('Session expired. Please log out and log in again');
            }
        })
        .always(function(data) {
            LOADER.hide();
        });
    },
    render_screens: function() {
        var pcount = FSR_MODEL.data.length,
            qcount = null,
            acount = null,
            screen_id = null,
            type = '',
            elem_id = null,
            answer_count = 2;

        //Render UI elements
        for (var i = 0; i <= pcount - 1; i++) {
            FSR_CREATE.add_screen_dom(FSR_MODEL.data[i]);
            screen_id = FSR_MODEL.data[i].id;

            if (FSR_MODEL.data[i].questions !== undefined) {
                qcount = FSR_MODEL.data[i].questions.length;

                for (var j = 0; j <= qcount - 1; j++) {
                    type = FSR_MODEL.data[i].questions[j].type;
                    elem_id = FSR_MODEL.data[i].questions[j].id;

                    if (typeof FSR_MODEL.data[i].questions[j].answers !== 'undefined') {
                        answer_count = FSR_MODEL.data[i].questions[j].answers.length;
                    }

                    FSR_CREATE.add_element(screen_id,type,elem_id,answer_count);
                }
            }
        }
    },
    add_screen: function() {
        LOADER.show('working...');

        $.ajax({
            url: UTILS.url('page'),
            type: 'post',
            data: {
                token : FSR_MODEL.token,
                fsr_id : FSR_MODEL.fsr_id,
                data: JSON.stringify(NEW_PAGE)
            }
        })
        .done(function(response) {
            FSR_CREATE.add_screen_dom(response); // Now create the DOM

            FSR_MODEL.data.push({ id: response.id, sort_order: '', gtm_value: '', questions: [] });
        })
        .fail(function() {
            LOADER.hide();
        })
        .always(function(xhr){
            LOADER.hide();
        });
    },
    add_screen_dom: function(response) {
        var $canvasGroup = $('#fsrCanvasGroup');
        var $listScreenNavigator = $('#listScreenNavigator');

        // Screen container markup and Screen list item markup
        var canvas_html =   '<li class="fsr-canvas-group-item" data-screen-id="'+ response.id +'">' +
                                '<div class="fsr-canvas-row"></div>' +
                                '<button type="button" class="btn btn-default btn-sm btn-remove" onclick="FSR_CREATE.remove_screen(this,'+response.id+')">' +
                                '<i class="zmdi zmdi-delete"></i> Remove</button>' +
                            '</li>';

        var nav_html = '<li class="list-screen-item" onclick="FSR_CREATE.scroll_to(this)">Screen</li>';

        // add screen to canvas
        $canvasGroup.append(canvas_html);

        // add item on screen navigator with a little bit of animated visual feedback
        $(nav_html)
            .appendTo($listScreenNavigator)
            .velocity({ scaleX: 1, scaleY: 1 },
            {
                begin: function() { 
                    var prop = 'scaleX(0) scaleY(0)';
                    $(this).css({ "webkitTransform": prop, "MozTransform": prop, "msTransform": prop, "OTransform": prop, "transform": prop });
                },
                duration: 400,
                easing: [0, 0.96, 0.86, 0.99]
            });

        FSR_CREATE.bind_canvas_interactions(); // re-initialize for dynamically created screens

        // display Screens tab if not active on Add Screen
        if (!$('#screensPanel').hasClass('active')) $('[href="#screensPanel"]').click();
    },
    remove_screen: function(target_btn,screen_id) {
        var ask_delete = confirm('Are you sure you want to remove this screen?');
        var screen_index = parseInt($(target_btn).closest('[data-screen-id]').index());

        if (ask_delete == true) {
            // var data = [];

            // update FSR Model
            FSR_MODEL.data.splice(screen_index,1);
            FSR_CREATE.remove_screen_dom(target_btn, screen_index);

            FSR_CREATE.submit_fsr();
        }
        else {
            return false;
        }
    },
    remove_screen_dom: function(target_btn,screen_index) {

        $(target_btn).closest('.fsr-canvas-group-item').remove(); // remove the target screen
        $('#listScreenNavigator').find(':eq('+ screen_index +')').remove(); // also remove the screen item on the lis

        // FSR_CREATE.refresh_screens();
    },
    update_screen: function(target_fsr_row,dropped_element) {
        LOADER.show();

        // Get widget type of dropped element
        var screen_id =     parseInt(target_fsr_row.parent().data('screen-id')),
            type =          dropped_element.data('generic-type'),
            ui_type =       dropped_element.data('type'),
            screen_index =  target_fsr_row.parent().index(),

            // Will serve as reference for array item searching if page is not yet refreshed
            temp_elem_id =  parseInt(dropped_element.closest('[data-element-id]'));

        var update_request_format = {
            sort_order: '', 
            gtm_value: '',
            questions: []
        }

        var answer_count = 2; // minimum count for answers

        if (FSR_MODEL.data[screen_index].hasOwnProperty('questions') === false) {
            FSR_MODEL.data[screen_index].questions = [];
        }
 
        FSR_MODEL.data[screen_index].questions.push(NEW_QUESTION);
        update_request_format.questions.push(NEW_QUESTION);


        var len = parseInt(FSR_MODEL.data[screen_index].questions.length - 1);
        FSR_MODEL.data[screen_index].questions[len].type = type;

        if (type === 'radio' || type === 'checkbox') {
            FSR_MODEL.data[screen_index].questions[len].answers.push({ title: "Choice 1" },{ title: "Choice 2" });
        }
        
        update_request_format.questions = FSR_MODEL.data[screen_index].questions;

        $.ajax({   
            url: UTILS.url('page'),
            type: 'post',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
            data: {
                _method: 'PATCH',
                token: FSR_MODEL.token,
                fsr_id : FSR_MODEL.fsr_id,
                page_id: screen_id,
                data: JSON.stringify(update_request_format)
            }
        })
        .done(function(response) {
            // Render in DOM
            FSR_CREATE.add_element(screen_id,type,temp_elem_id,answer_count,ui_type);

            //Open dropdown menu upon add so that user will notice
            target_fsr_row.find('[data-element-id="'+ temp_elem_id +'"] [data-toggle="dropdown-custom"]').click();
        })
        .fail(function(response) {
            console.log('fail')
            LOADER.hide();
        })
        .always(function(response) {
            LOADER.hide();
        });
    },
    add_element: function(screen_id,type,elem_id,answer_count,ui_type) {

        var target_fsr_row = $('*[data-screen-id="'+screen_id+'"]').find('.fsr-canvas-row');


        // Action Controls Template // wa = widget actions
        var elem_remove  = '<button type=button" class="btn btn-transparent btn-sm btn-remove" data-toggle="tooltip" data-title="Remove Question"' +
                                'onclick="FSR_CREATE.remove_element(this)"><i class="zmdi zmdi-minus-circle"></i>' +
                            '</button>',

            elem_add     = '<button type="button" class="btn btn-transparent btn-sm" data-toggle="tooltip" data-title="Add Tile"' +
                                'onclick="FSR_CREATE.add_option(this)"><i class="zmdi zmdi-collection-plus"></i>' +
                            '</button>',

            elem_mapping = '<div class="dropdown" data-toggle="tooltip" data-title="Map Question">' +
                                '<button type="button" class="btn btn-transparent btn-sm btn-map dropdown-toggle" data-toggle="dropdown-custom">' + 
                                    '<i class="zmdi zmdi-settings" data-mapping-indicator="'+get_mapping_value()+'"></i>' +
                                '</button>' +
                                '<div class="dropdown-menu open pull-right dropdown-mapping">' +
                                    '<div class="btn-group btn-group-vertical" data-toggle="buttons">'+ create_mapping_markup() +'</div>' +
                                '</div>' +
                            '</div>';

        // Widget Template
        var element_html =  '<section class="fsr-region'+ is_mappable() +'"' + $.trim('data-element-id="'+elem_id+'">') + 
                                '<div class="fsr-widget" data-widget-type="'+ type +'">' +
                                    '<i class="widget-drag-handle"></i>'+
                                    '<div class="widget-input">'+
                                        create_element_label() +
                                        '<input type="text" class="text-h1" placeholder="Main Text ..." id="title'+elem_id+'">'+
                                        '<input type="text" class="text-h2" placeholder="Secondary Text ..." id="subtitle'+elem_id+'">'+
                                        '<input type="text" class="text-normal" placeholder="Optional Text ..." id="content'+elem_id+'">'+
                                        has_options() +
                                    '</div>' +
                                    '<div class="widget-actions">' + create_action_buttons() + '</div>'
                                '</div>' +
                            '</section>';

        // Template Helpers
        function create_element_label() {
            if (ui_type == 'text-box' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-textbox"></i> Text Box</span></h6>';
            }
            else if (ui_type == 'text-area' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-textbox"></i> Text Area</span></h6>';
            }
            else if (ui_type == 'text-email' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-email"></i> Email Box</span></h6>';
            }
            else if (ui_type == 'text-url' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-upload"></i> Upload Box</span></h6>';
            }
            else if (ui_type == 'text-contact' || type == 'text') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-contact"></i> Contact Number Box</span></h6>';
            }
            else if (ui_type == 'select-dropdown' || type == 'select') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-list"></i> List</span></h6>';
            }
            else if (ui_type == 'checkbox-select' || type == 'checkbox') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-tile-multiple"></i> Tile Select <small class="text-uppercase">Multiple</small></span></h6>';
            }
            else if (ui_type == 'radio-select' || type == 'radio') {
                return '<h6 class="sidebar-list-heading"><span><i class="fsr-ico-tile-single"></i> Tile Select <small class="text-uppercase">Single</small></span></h6>';
            }
            else {
                return '';
            }
        }

        function has_options() {
            if (type == 'radio' || type == 'checkbox' || type == 'radio-select' || type == 'checkbox-select') {
                return '<div class="options">'+FSR_CREATE.option_markup(answer_count)+'</div>';
            }
            else { 
                return '';
            }
        }
               
        function create_action_buttons() {
            if (type == 'radio-select' || type == 'checkbox-select' || type == 'radio' || type == 'checkbox') { 
                return elem_remove + elem_add + elem_mapping;
            }
            else if (type == 'text-h1' || type == 'text-h2' || type == 'text-normal' || type == 'button') {
                return elem_remove;
            }
            else {
                return elem_remove + elem_mapping;   
            }
        }

        function create_mapping_markup() {
            var markup = [];
            var elementName = MAPPING[type.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); })];

            if (typeof elementName !== 'undefined') {
                for (var i = 0; i <= elementName.length - 1; i++) {
                    markup.push('<label class="btn btn-default '+ is_mapping_active() +'" for="'+ elem_id + 'opt' + i +'"'+
                                    'onclick="FSR_CREATE.update_mapping(\''+ 
                                        elementName[i] + '\'' + ',' + 'this' + ')">' +
                                    '<input type="radio" id="'+ elem_id + 'opt' + i +'" name="opt-'+ elem_id +'"/><span>'+ elementName[i] +'</span>' + 
                                '</label>');
                };
                return markup.join("");
            }
        }

        function is_mappable() { return ' unmapped'; }


        function get_mapping_value() {

        }

        function is_mapping_active() {

        }

        // Render the widget container
        target_fsr_row.append(element_html);
    },
    remove_element: function(btn) {
        var screen_id       = parseInt($(btn).closest('[data-screen-id]').data('screen-id')),
            screen_index    = $(btn).closest('[data-screen-id]').index(),
            element_index      = $(btn).closest('[data-element-id]').index();

        var update_request_format = {
            id: screen_id,
            sort_order: '', 
            gtm_value: '',
            id: screen_id,
            questions: []
        }

        // Remove element from FSR model
        FSR_MODEL.data[screen_index].questions.splice(element_index, 1);

        update_request_format.questions = FSR_MODEL.data[screen_index].questions;

        // Update UI
        var row = $(btn).closest('.fsr-canvas-row');

        btn.closest('.fsr-region').remove();
        $('.tooltip').remove();

        // FSR_CREATE.submit_fsr();
    },
    refresh_elements: function(row,isSorted) {
        var s_index = row.closest('[data-screen-id]').index(); // Get screen id

        var updated_array = FSR_MODEL.data[s_index].questions; // targeted elements group

        // get indices from a temp object set by jqueryUI's sortable start and update method
        var oldIndex = FSR_CREATE.temp_order[0],
            newIndex = FSR_CREATE.temp_order[1];

        //Update DOM
        // $.each(row.children('.fsr-region'), function(e_index) {
        //      $(this)
        //         .attr('data-element-id', 'eid'+e_index);

        //      $.each($(this).find('.btn-group .btn'),function(m_index) {
        //          $(this)
        //             .attr('for', 'sid'+ s_index + 'eid' + e_index + 'opt' + m_index);

        //          $(this).find('input[type="radio"]')
        //             .attr('id', 'sid'+ s_index + 'eid' + e_index + 'opt' + m_index)
        //             .attr('name', 'opt-sid'+ s_index + 'eid' + e_index);
        //      });
        // });

        // If sorted
        if (isSorted == true) {
            var swap = function(temp_array, oldIndex, newIndex) {
                var temp = temp_array[oldIndex];
                temp_array[oldIndex] = temp_array[newIndex];
                temp_array[newIndex] = temp;
            };
            swap(updated_array, oldIndex, newIndex);
        };

        // Update data model based from the sorting
        // for (var i = 0; i <= updated_array.length - 1; i++) {
        //     updated_array[i].elem_id = 'eid' + i;
        // };
    },
    refresh_screens: function() {
        $.each($('#fsrCanvasGroup .fsr-canvas-group-item'), function(index) {
            $(this).attr('data-screen-id', 'sid' + index);

            FSR_MODEL.data[index].id = 'sid'+index;
            FSR_CREATE.refresh_elements($(this).find('.fsr-canvas-row'));
        });
    },
    update_mapping: function(mapping_name, elem) {
        var parent_elem     = $(elem).closest('[data-element-id]'),
            parent_screen   = $(elem).closest('[data-screen-id]');

        var screen_index    = $(elem).closest('[data-screen-id]').index(),
            element_index   = $(elem).closest('[data-element-id]').index();

        function find_id(array, attr, value) {
            for(var i = 0; i < array.length; i += 1) {
                if(array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        }

        console.log(find_id(MAPPING_DATA.data,'xml_attribute',mapping_name));

        // FSR_MODEL.data[screen_index].questions[element_index].data = {};
        FSR_MODEL.data[screen_index].questions[element_index].mapped_to = find_id(MAPPING_DATA.data,'xml_attribute', mapping_name) + 1;
        delete FSR_MODEL.data[screen_index].questions[element_index].xml_equivalent;
        
        // Update UI status indicator
        parent_elem.removeClass('unmapped');
        parent_elem.find('[data-mapping-indicator]').attr('data-mapping-indicator', mapping_name);

        // FSR_CREATE.update_screens();
    },
    option_markup: function(answer_count,value, option_id) {

        var template = '<div class="fsr-widget" data-option-id="'+option_id+'">' +
                            // '<div class="upload-thumbnail" data-toggle="tooltip" data-title="Attach Thumbnail Image">' +
                            //     '<input type="file" class="js_attachThumbnail" accept="image/x-png" data-option-thumbnail-id="">' +
                            //     '<span class="attachment-preview js_attachmentPreview"></span>' +
                            //     '<i class="zmdi zmdi-image-o zmdi-hc-3x"></i>' +
                            // '</div>' +
                            '<input type="text" class="text-options" placeholder="Enter choice..." />' +
                            '<div class="widget-actions">' + 
                                '<button type="button" class="btn btn-transparent btn-sm" data-toggle="tooltip" data-title="Remove Option" onclick="FSR_CREATE.remove_option(this)">' +
                                    '<i class="zmdi zmdi-minus-circle"></i>' +
                                '</button>' +
                            '</div>' +
                        '</div>';

        var options = '';

        for (var i = 0; i <= answer_count-1; i++) {
            options = options + template;
        }
        return options;
    },
    add_option: function(btn) {
        // Update Model
        var elem_index     = $(btn).closest('[data-element-id]').index(),
            screen_index   = $(btn).closest('[data-screen-id]').index();

        FSR_MODEL.data[screen_index].questions[elem_index].answers.push({ title: 'Choice'});

        // Update UI
        $(btn).closest('.fsr-widget').find('.widget-input').append(FSR_CREATE.option_markup(1));
    },
    remove_option: function(btn) {
        // Update model
        var elem_index     = $(btn).closest('[data-element-id]').index(),
            screen_index   = $(btn).closest('[data-screen-id]').index(),
            target_index   = $(btn).closest('[data-option-id]').index();

        FSR_MODEL
            .data[screen_index]
            .questions[elem_index]
            .answers.splice(target_index,1);

        // Update UI
        $(btn).closest('.fsr-widget').remove();
        $('.tooltip').remove();
    },
    validate_fsr: function() {
        // check if all fields are mapped
        var data = JSON.stringify(FSR_MODEL);
        alert(data);
    },
    submit_fsr: function() {
        LOADER.show('working...');

        var pages = FSR_MODEL.data;

        // This is to remove the unique_name property before sending
        for (var i = pages.length - 1; i >= 0; i--) {
            delete pages[i].unique_name;
        }

        console.log(pages);

        $.ajax({   
            url: UTILS.url('fsr'),
            type: 'post',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
            data: {
                action: 'update',
                token: FSR_MODEL.token,
                id : FSR_MODEL.fsr_id,
                name: $('#copyTextName').val(),
                url: $('#copyTextURL').val(),
                status: FSR_MODEL.status,
                pages: JSON.stringify(pages)
            }
        })
        .done(function(response) {

        })
        .fail(function(response) {
            console.log('fail')
            LOADER.hide();
        })
        .always(function(response) {
            LOADER.hide();
        });
        debugger;
    },
    cancel: function() {
        window.location.href = 'portfolio.html'; // replace with whatever URL
    },
    scroll_to: function(target_screen) {
        var i = $(target_screen).index(),
            screen_position = $('#fsrCanvasGroup li').eq(i).position().top;

        $('[href="#screensPanel"]').click(); // If a screen thumbnail is clicked display screens tab
        $('html').velocity('scroll', { offset: screen_position + 15, duration: 800, easing: [.59,.21,.33,1] }); // scroll to target screen
    },
    build: function() {
        // if (AUTH.check_session()) { 
            this.init();
        // }
        // else { LOADER.show('Oops. Looks like your session expired. <a onclick="AUTH.log_out()">Click here to log In</a>'); }

        this.bind_drag_to_widgets();
        this.bind_canvas_interactions();
        this.bind_save_question_text();
    }
    
}

$(document).ready(function() {
    FSR_CREATE.build();
});