<?php

/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 22/08/2016
 * Time: 3:40 PM
 */
class FSRTest extends TestCase
{

    /**
     * @test
     *
     * TEST: GET /FSR
     */
    public function testFSRFetchAll()
    {
        echo "Checking for the FetchAll in FSR";

        $this->get('/fsr')
            ->seeJsonStructure([
                'response',
                'page',
                'count',
                'data' => [
                            '*' => ['id','status','url','name']
                ]
            ]);
    }

    /**
     * @test
     *
     * TEST: GET /FSR [id]
     */
    public function testFSRFetchOne()
    {

        echo "Checking for FetchOne in FSR";
        $this->get('/fsr',["id" => 1] )
            ->seeJsonStructure([
                'response',
                'data' => ['id','status','url','name']
            ]);
    }

    /**
     * @test
     *
     * TEST: POST /FSR [action, tolken]
     */
    public function testFSRCreate()
    {
        echo "Create new FSR";
        $this->post('/fsr', ["action" => "new", "token" => 'testingtoken'])
            ->seeJsonStructure([
                'response',
                'id'
            ]);
    }

}