<?php

/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 22/08/2016
 * Time: 3:40 PM
 */
class MappingTest extends TestCase
{

    /**
     * @test
     *
     * TEST: GET /mapping
     */
    public function testMappingFetchAll()
    {
        echo "Checking for the FetchAll in Mapping";

        $this->get('/mapping')
            ->seeJsonStructure([
                'response',
                'count',
                'data' => [
                            '*' => ['id','attribute']
                ]
            ]);
    }




}