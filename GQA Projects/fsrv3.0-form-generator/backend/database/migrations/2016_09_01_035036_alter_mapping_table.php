<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mapping', function (Blueprint $table) {
            $table->string('attribute')->after('id');
            $table->dropColumn('status');
            $table->enum('active',[1,0])->default(1)->comment('will only respond with name of crm object to map to');
            $table->dropColumn('mapped_to');
            $table->string('xml_attribute');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mapping', function (Blueprint $table) {
            $table->dropColumn('attribute');
            $table->enum('status',['active', 'inactive'])
                ->default('active')
                ->comment('will only respond with name of crm object to map to');
            $table->dropColumn('active');
            $table->string('mapped_to');
            $table->dropColumn('xml_attribute');
        });
    }
}
