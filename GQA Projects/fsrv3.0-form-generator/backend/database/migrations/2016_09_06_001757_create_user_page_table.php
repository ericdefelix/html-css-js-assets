<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_page', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('page_id')->unsigned();
        });
        Schema::table('user_page', function (Blueprint $table){
            $table->foreign('user_id')
                    ->references('id')
                    ->on('user')
                    ->onUpdate('cascade');
            $table->foreign('page_id')
                    ->references('page_id')
                    ->on('page')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_page', function (Blueprint $table){
          $table->dropForeign(['page_id']);
          $table->dropForeign(['user_id']);
        });
        Schema::drop('user_page');
    }
}
