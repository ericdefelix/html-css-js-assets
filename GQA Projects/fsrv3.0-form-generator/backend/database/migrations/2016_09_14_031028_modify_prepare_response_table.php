<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPrepareResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('response', function (Blueprint $table) {
            //
            $table->string('session')->after('id')->unique();
            $table->integer('fsr')->after('session')->unsigned();
            $table->text('response')->after('fsr');
            $table->foreign('fsr')
                ->references('fsr_id')
                ->on('fsr')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('response', function (Blueprint $table) {
            // remove foreign first
            $table->dropForeign(['fsr']);
            $table->dropColumn(['response', 'fsr', 'session']);
        });
    }
}
