<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCacheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cache', function (Blueprint $table) {
            $table->increments('id');
            $table->text('structure');
            $table->integer('fsr')->unsigned();
            $table->timestamps();
        });
        
        Schema::table('cache', function(Blueprint $table) {
        $table->foreign('fsr')
                ->references('fsr_id')
                ->on('fsr')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cache', function(Blueprint $table) {
            $table->dropForeign(['fsr']);
        });
        Schema::drop('cache');
    }
}
