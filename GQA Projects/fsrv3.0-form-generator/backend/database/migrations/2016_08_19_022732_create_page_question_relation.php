<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageQuestionRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_question', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page')->unsigned();
            $table->integer('question')->unsigned();
        });
        
        Schema::table('page_question', function(Blueprint $table) {
        $table->foreign('page')
                ->references('page_id')
                ->on('page')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        
        $table->foreign('question')
                ->references('question_id')
                ->on('question')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('page_question', function(Blueprint $table) {
            $table->dropForeign('page_question_page_foreign'); // doing it the manual way 
            $table->dropForeign('page_question_question_foreign'); // doing it the manual way 
        });
        Schema::drop('page_question');
    }
}
