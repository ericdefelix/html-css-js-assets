<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignkeyMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mapping', function (Blueprint $table) {
            $table->dropForeign(['question']);
            $table->dropColumn('question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mapping', function (Blueprint $table) {
            $table->integer('question')->unsigned();
            $table->foreign('question')
                ->references('question_id')
                ->on('question')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }
}
