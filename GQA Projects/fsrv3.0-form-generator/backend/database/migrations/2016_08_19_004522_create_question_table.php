<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->increments('question_id');
            $table->string('question');
            $table->text('validation')->comment('serialised values - required, email, number etc');
            $table->string('event_trigger')->comment('JS triggers on validation successful');
            $table->enum('type', ['text', 'select','checkbox','radio']);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question');
    }
}
