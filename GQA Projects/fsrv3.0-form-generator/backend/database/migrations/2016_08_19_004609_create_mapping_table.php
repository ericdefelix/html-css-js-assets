<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question')->unsigned();
            $table->string('mapped_to');
            $table->enum('status', ['active', 'inactive'])
                    ->default('active')
                    ->comment('will only respond with name of crm object to map to');
        });
        
        Schema::table ('mapping', function(Blueprint $table) {
           $table->foreign('question')
                  ->references('question_id')
                  ->on('question')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mapping', function(Blueprint $table) {
            $table->dropForeign(['question']);
        });
        Schema::drop('mapping');
    }
}
