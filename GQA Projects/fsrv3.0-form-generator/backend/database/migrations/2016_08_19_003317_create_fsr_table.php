<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFsrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fsr', function (Blueprint $table) {
            $table->increments('fsr_id');
            $table->string('name');
            $table->string('url', 128);
            $table->string('keywords',255);
            $table->enum('status', ['draft', 'live', 'hold', 'archive'])->default('draft');
            $table->string('gtm_tag', 64);
            $table->string('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fsr');
    }
}
