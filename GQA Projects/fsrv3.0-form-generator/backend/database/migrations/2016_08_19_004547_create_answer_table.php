<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->increments('answer_id');
            $table->integer('question')->unsigned();
            $table->string('answer');                         
        });        
        
        Schema::table('answer', function (Blueprint $table) {
        $table->foreign('question')
                ->references('question_id')
                ->on('question')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answer', function(Blueprint $table) {
            $table->dropForeign(['question']);
        });
        Schema::drop('answer');
    }
}
