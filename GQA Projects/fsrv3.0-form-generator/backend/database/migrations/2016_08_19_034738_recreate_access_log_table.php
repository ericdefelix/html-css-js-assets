<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateAccessLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->dateTime('logged_in');
            $table->dateTime('logged_out');
            $table->integer('page_id');
            $table->timestamps();
        });

        Schema::table('access_log',function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('user');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('access_log',function(Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::drop('access_log');
    }
}
