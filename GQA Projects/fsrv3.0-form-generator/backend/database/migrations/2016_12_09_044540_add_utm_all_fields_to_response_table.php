<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUtmAllFieldsToResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('response', function (Blueprint $table) {
            $table->string('source')->after('response');
            $table->string('medium')->after('source');
            $table->string('campaign')->after('medium');
            $table->string('term')->after('campaign');
            $table->string('content')->after('term');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('response', function (Blueprint $table) {
            $table->dropColumn('source');
            $table->dropColumn('medium');
            $table->dropColumn('campaign');
            $table->dropColumn('term');
            $table->dropColumn('content');
        });
    }
}
