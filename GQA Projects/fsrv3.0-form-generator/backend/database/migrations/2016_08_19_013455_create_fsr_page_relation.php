<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFsrPageRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fsr_page', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fsr')->unsigned();
            $table->integer('page')->unsigned();
        });
        
        Schema::table('fsr_page', function(Blueprint $table) {
        $table->foreign('fsr')
                ->references('fsr_id')
                ->on('fsr')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        
        $table->foreign('page')
                ->references('page_id')
                ->on('page')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fsr_page', function(Blueprint $table) {
            $table->dropForeign(['fsr']);
            $table->dropForeign(['page']);
        });
        Schema::drop('fsr_page');
    }
}
