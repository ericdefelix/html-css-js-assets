<?php
/**
 * FSR creation event
 * @category Events
 * @package
 * @author  Mohammad Abbas <it@gqaustralia.com.au>
 * @license GQA
 * @link
 */

namespace App\Events;

class FsrEditedEvent extends Event
{
    
    private $id;
    
    /**
     * Create a new event instance.
     * @param int $id the id of the created fsr
     */
    public function __construct($id)
    {
        $this->id = $id;
    }
    
    /**
     * Get the ID of the newly created FSR
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
