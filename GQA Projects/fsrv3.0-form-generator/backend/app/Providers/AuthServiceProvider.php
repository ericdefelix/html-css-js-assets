<?php

namespace App\Providers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $login;
    protected $mAuth;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public function __construct()
    {
        //$this->mAuth = new WebService();
    }


    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        //$api = $this->mAuth->Authenticate(Request $request);

        /*Auth::viaRequest($api, function (Request $request) {
            if ($request->input('token')) {
                return User::where('token', $request->input('token'))->first();
            }*/
        //});
    }
}
