<?php 
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\SyncResponse;

class CommandServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.response.sync', function()
        {
            return new SyncResponse;
        });

        $this->commands(
            'command.response.sync'
        );
    }
}