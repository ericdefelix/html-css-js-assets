<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Services\ResponseService;

class SyncResponse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'response:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
//    public function handle()
//    {
//        //
//    }
    
     /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function fire()
  {
      $responseService = new ResponseService();
         $updates = $responseService->getAllNonSyncedResponse();
		 $this->info('Total Leads Found: '. count($updates));
         
         foreach ($updates as $update) {
			 $this->info('Running for: '. $update['id']);
             $client = new \GuzzleHttp\Client();
             $response = $client->request('POST', 'http://www.gqaustralia.edu.au/v1.5/savedata.php', [
                'form_params' => [
                    'lead' => serialize($update['data']),
                    'resume' => $update['resume']
                ],
                'allow_redirects' => [
                    'protocols'       => ['http'], // only allow https URLs
                ]
            ]);
            if($response->getStatusCode() == 200) {
				
               $data = $response->getBody();
               $data = json_decode($data,true);
			   
               $this->info($data['result']);
			   if(isset($data['result']) && ($data['result'] == 'success' || $data['result'] == 'duplication')){
                   $responseService->updateIsSync($update['id']);
               }
            } else {
				$this->info('Sync for '. $update['id'].' Failed');		
			}
        
         }
    $this->info('Run Succesfully');
  }

  /**
   * Get the console command arguments.
   *
   * @return array
   */
  protected function getArguments()
  {
    return array(
      //array('example', InputArgument::REQUIRED, 'An example argument.'),
    );
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return array(
//      array('example', null, InputOption::VALUE_OPTIONAL,
//        'An example option.' ,null),
    );
  }
}
