<?php

namespace App\Locale\en;

/**
 * Description of Messages
 *
 * @author mohammada
 */
class Messages {
    //put your code here
    
    const FSR_NOT_FOUND = 'No FSR found with the given details';
    const NO_FSRS = 'No FSRs found';
    const REQUEST_NOT_DONE = 'Unable to complete the request';
    const CACHE_NOT_UPDATED = 'Unable to create / update cache';
    
}
