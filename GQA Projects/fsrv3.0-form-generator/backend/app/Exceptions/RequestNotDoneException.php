<?php

namespace App\Exceptions;

class RequestNotDoneException extends \Exception
{
    
    public function __construct()
    {
        parent::__construct(app('translator')->trans('Requests.NotDone'));
    }
}
