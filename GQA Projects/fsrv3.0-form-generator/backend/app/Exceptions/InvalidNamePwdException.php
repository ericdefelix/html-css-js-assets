<?php

namespace App\Exceptions;

class InvalidNamePwdException extends \Exception
 {
    public function __construct()
    {
        parent::__construct(app('translator')->trans('Auth.NotFound'));
    }

 }