<?php

namespace App\Exceptions;

class CacheNotUpdatedException extends \Exception
{
    /**
     * Create the Exception
     */
    public function __construct()
    {
        parent::__construct(eapp('translator')->trans('Cache.NotUpdated'));
    }
}
