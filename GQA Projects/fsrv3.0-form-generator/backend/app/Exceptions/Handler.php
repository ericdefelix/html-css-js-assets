<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  $e the main exception thrown
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  $request
     * @param  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // global uncaught ModelNotFoundExceptions handling
        
        if ($e instanceof ModelNotFoundException) {
                return response()->json(
                    array (
                        'response' => 404,
                        'message' => 'No records found'
                        )
                );
        }
        // global uncaught NotFound handling
        if ($e instanceof FsrNotFoundException
            || $e instanceof NoFSRsException
            || $e instanceof PageNotFoundException
            || $e instanceof NoPagesFoundException
            || $e instanceof PageNotSavedException
            || $e instanceof ResponseNotFoundException
        ) {
            return response()->json(
                array (
                    'response' => 404,
                    'message' => $e->getMessage()
                )
            );
        }
        // global uncaught not processable handling
        if ($e instanceof RequestNotDoneException
            || $e instanceof CacheNotUpdatedException
            || $e instanceof MissingParamException
           
        ) {
            return response()->json(
                array (
                    'response' => 500,
                    'message' => $e->getMessage()
                    )
            );
        }

        // global uncaught Invalid username password combination
        if ($e instanceof InvalidNamePwdException)
        {
            return response()->json(
                array (
                    'response' => 404,
                    'message' => $e->getMessage()
                )
            );
        }
        //global uncaught User not found handling
        if ($e instanceof UserNotFoundException) {
            return response()->json(
                array (
                    'response' => 404,
                    'message' => 'No records found'
                )
            );
        }

        // global uncaught NotFound handling
        if ($e instanceof MappingsNotFoundException) {
            return response()->json(
                array (
                    'response' => 404,
                    'message' => $e->getMessage()
                )
            );
        }
            
        return parent::render($request, $e);
    }
}