<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 1/09/2016
 * Time: 3:52 PM
 */
namespace App\Exceptions;

class MappingsNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct(app('translator')->trans('Mapping.NotFound'));
    }
}
