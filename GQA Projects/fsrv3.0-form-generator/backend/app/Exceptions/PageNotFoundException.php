<?php

namespace App\Exceptions;

class PageNotFoundException extends \Exception
{
    /**
     * Create the Exception
     */
    public function __construct()
    {
        parent::__construct(app('translator')->trans('Page.NotFound'));
    }
}
