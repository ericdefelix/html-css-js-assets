<?php

namespace App\Exceptions;

class NoPagesFoundException extends \Exception
{
    /**
     * Create the Exception
     */
    public function __construct()
    {
        parent::__construct(app('translator')->trans('Page.NotFoundForFSR'));
    }
}
