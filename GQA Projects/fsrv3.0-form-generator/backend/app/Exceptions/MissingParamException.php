<?php

namespace App\Exceptions;

class MissingParamException extends \Exception
{
    protected $param;
    
    public function __construct($param)
    {
        $this->param = $param;
        parent::__construct(app('translator')->trans('Requests.MissingParam')
                . $this->param);        
    }
}
