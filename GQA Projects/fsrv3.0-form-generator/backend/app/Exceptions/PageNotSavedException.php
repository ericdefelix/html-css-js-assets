<?php

namespace App\Exceptions;

class PageNotSavedException extends \Exception
{
    
    public function __construct($unique = false)
    {
        if ($unique) {
            parent::__construct(app('translator')->trans('Page.NameNotUnique'));
        }else {
            parent::__construct(app('translator')->trans('Page.PageError'));
        }
    }
}
