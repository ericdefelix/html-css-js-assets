<?php

namespace App\Exceptions;

class NoFSRsException extends \Exception
{
    
    public function __construct()
    {
       //parent::__construct('Sorry. No FSR was ed');
        parent::__construct(app('translator')->trans('FSR.NoFSRs'));
    }
}
