<?php

namespace App\Exceptions;

class FsrNotFoundException extends \Exception
{
    /**
     * Create the Exception
     */
    public function __construct()
    {
        parent::__construct(app('translator')->trans('FSR.NotFound'));
    }
}
