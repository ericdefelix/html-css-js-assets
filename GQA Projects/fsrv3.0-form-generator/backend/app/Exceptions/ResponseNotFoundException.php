<?php

namespace App\Exceptions;

class ResponseNotFoundException extends \Exception
{
    /**
     * Create the Exception
     */
    public function __construct()
    {
        parent::__construct(app('translator')->trans('Response.NotFound'));
    }
}
