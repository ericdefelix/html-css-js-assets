<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 26/08/2016
 * Time: 1:58 PM
 */
namespace App\Http\Middleware;

use App\Exceptions\UserNotFoundException;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class AuthMiddleware
{
    public function handle(Request $request,Closure $next)
    {

      try{
          if(!empty($request->input('token'))){

              $user = User::where('token',$request->input('token'))->first();


              if($user instanceof User){
                  if($user->validity == 1){ //if token is valid, checks the last modified time of token and if it exceeded 15 minutes , the session is expired
                      $now = new \DateTime('now',new \DateTimeZone('Australia/Sydney'));

                      $modifiedTime =  new \DateTime($user->updated_at);
                     $dteDiff = $now->getTimestamp() - $modifiedTime->getTimestamp();

                      if($dteDiff < 1000){
                          $user->updated_at = new \DateTime('now');
                          $user->save();
                         // $result = DB::insert("insert into user (updated_at) values ('now()' )");
                          return $next($request);
                      } else{
                          $user->validity = 0;
                          $user->save();
                          throw new \Exception("Session Expired");
                      }
                  }else{
                      throw new \Exception("Not a valid token");
                  }

              }else{
                  throw new UserNotFoundException();

              }


          }else{
              throw new \Exception ("Token not found");
          }
      }
      catch(\Exception $e)
      {
         return response()->json(
             array(
                 'response' =>404,
                 'message'=>$e->getMessage()

             ),404

         );
      }
    }


}