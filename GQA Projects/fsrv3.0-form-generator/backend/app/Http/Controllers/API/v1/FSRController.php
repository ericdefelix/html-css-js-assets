<?php
/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 16/08/2016
 * Time: 11:13 AM
 */
namespace App\Http\Controllers\API\v1;

use App\Exceptions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FSRController extends Controller
{
    public function __construct()
    {
     //   $this->middleware('tokenize');
      //  echo app('translator')->trans('Page.NotFoundForFSR');
    }

        /**
     * Deal with the GET request
     */
    public function index(Request $request)
    {

        if ($request->has('action')) {
            if ($request->action == 'isunique') {
                $service = new \App\Services\FSRService;
                $unique = $service->isUnique($request);
                if ($unique) {
                    return response()->json(
                        array(
                        'response' => 200,
                        'id'       => $unique,
                        'message' => app('translator')->trans('Messages.NotUnique')
                        ),
                    200
                    );
                } else {
                    return response()->json(
                        array(
                        'response' => 404,
                        'message' => app('translator')->trans('FSR.IsUnique')
                        ),
                    404
                    );
                }
            }else {
                throw new Exceptions\RequestNotDoneException;
            }
        }
        if ($request->has('id')) {
            return $this->getSingular($request->id);
        } else {
            $options = array(
                "sortBy"    => $request->input('sort') ? : 'created_at',
                "sortOrder" => $request->input('sort_order')? : 'DESC',
                "page"      => $request->input('page')? : 1,
                "limit"     => $request->input('limit')? : 1000
            );
            return $this->getAll($options);
        }
    }

    /**
     * Get Single FSR
     * @param int $id
     */
    
    private function getSingular($id)
    {
        $service = new \App\Services\FSRService();        
        $fsr = $service->findOne($id);
        $fsr->url = getenv('URL'). str_replace(getenv('URL'), '', $fsr->url);
        return response()->json(
            array(
                'response' => 200,
                'data' => $fsr
            ),
            200
        );
    }
    
    /**
     * Find all the FSRs saved
     * @param array $options
     * @return type
     */
    private function getAll($options)
    {
        $service = new \App\Services\FSRService();
       
        $allFSRs = $service->findAll($options);
        
          foreach($allFSRs['data'] as $fsr){
              
             $fsr->url = getenv('URL'). str_replace(getenv('URL'), '', $fsr->url);
                     
          }
          
        return response()->json(
            array (
                'response' => 200,
                'count'    => $allFSRs['count'],
                'page'     => $options['page'],
                'data'     => $allFSRs['data'],
            ),
            200
        );
    }
    
    /**
     * Deal with POST of request
     * initialises the FSR and makes it ready to accept
     */
    
    public function handlePost(Request $request)
    {
        $action = $request->input('action');
        switch ($action) {            
            case "new":
                return $this->create();
            break;
            case "update":
                if (!$request->has('id')){
                    throw new Exceptions\MissingParamException('id');
                }
                return $this->update($request->input('id'), $request);
            break;
            case "updatestatus":
                if (null === $request->input('id')){
                    throw new Exceptions\MissingParamException('id');
                }
                if (!$request->has('status')){
                    throw new Exceptions\MissingParamException('status');
                }
                return $this->update(
                        $request->input('id'), 
                        $request, 
                        $request->input('status')
                    );
            break;
            case "copy":
                if (!$request->has('id')){
                    throw new Exceptions\MissingParamException('id');
                }
                return $this->copyFSR($request->input('id'));
            break;
            // No action was specified
            default :
                throw new Exceptions\RequestNotDoneException;
            break;
        }
    }
    
    private function create()
    {
        $service = new \App\Services\FSRService();
        $createId = $service->createFSR();
        return response()->json(
            array (
                'response' => 200,
                'id' => $createId
            ),
            200
        );
    }

    /**
     * PUT request
     */
    public function store(Request $request)
    {
        if (!$request->has('id')) {
            throw new Exceptions\MissingParamException('id');
        } else {
            $service = new \App\Services\FSRService;
            $save = $service->uploadBGImage($request);
            if ($save) {
                return response()->json(
                    array (
                        'response' => 200,
                        'id'       => $save,
                        'message'  => app('translator')->trans('FSR.Updated')
                    ),
                    200
                );
            }
        }
    }

    /**
     * POST/PATCH
     */
    public function update($id, $request, $status = false)
    {
        $service = new \App\Services\FSRService();
        if ($status) {
            $opts = array ('status' => $status);
        }  else {
            $opts = $service->generateUpdatesQry($request);
        }
        if ($request->has('pages')) {
            $pages = is_array(json_decode($request->pages, true)) ? 
                json_decode($request->pages, true) : false;
        }else {
            $pages = false;
        }
        $update = $service->updateFSR($id, $opts, $pages);
        return response()->json(
            array (
                'response' => 200,
                'id'       => $update,
                'message'  => app('translator')->trans('FSR.Updated')
            ),
            200
        );
    }
    /**
     * DELETE action
     */
    public function delBg(Request $request)
    {
        if (!$request->has('id')) {
            throw new Exceptions\MissingParamException('id');
        } else {
            $service = new \App\Services\FSRService();
            $del = $service->updateFSRImage($request->id, '');
            return response()->json(
            array (
                'response' => 200,
                'id'       => $del,
                'message'  => app('translator')->trans('FSR.Updated')
            ),
            200
        );
        }
    }
    /**
     * Gets the entire FSR structure from cache
     * @param \Illuminate\Http\Request $request
     * @return type 
     * @throws Exceptions\MissingParamException
     */
    
    public function getFSR(Request $request)
    {
        
        try
        {
           
            $fsrId = ($request->has('fsr_id'))? $request->input('fsr_id'):0;
            if (!empty($fsrId)) {
                $cacheService = new \App\Services\CacheService();
                $FSR = $cacheService->getCache($fsrId);
                 //print_r($FSR);
               // exit;
           // var_dump(JSON.stringify($FSR->structure));
            
                    return response()->json(
                        array(
                            'response'=>200,
                            'data'=> json_decode($FSR->structure)
                        ),200
                        );
                } else {
             
                throw new Exceptions\MissingParamException('fsr_id'); 
            }
        } catch (Exception $ex) {
            
            return response()->json(
                    array(
                        'response'=>404,
                        'message'=> $ex->getMessage()
                    ),404
                   );

        } 
        
        
    }
    
    /**
     * Copy the entire FSR structure
     * @param frsId
     * @return id 
     */
    public function copyFSR($fsrId)
    {
        
        if (!empty($fsrId)) {

           $cacheService = new \App\Services\CacheService();
           $FSR = $cacheService->getCache($fsrId);                
           $data = json_decode($FSR->structure,true);
           if (isset ($data['keywords']) && is_array($data['keywords'])) {
               $data['keywords'] = serialize($data['keywords']);
           }
           $pages = $data['pages'];
           unset($data['fsr_id']);
           unset($data['name']);
           unset($data['url']);
           unset($data['pages']);
           $service = new \App\Services\FSRService();
           $createId = $service->createFSR();

           $update = $service->copyFSR($createId, $data,$pages);
           return response()->json(
                array (
                    'response' => 200,
                    'id'       => $update,
                    'message'  => app('translator')->trans('FSR.Updated')
                ),
                   200
           );

        }
    }
}
