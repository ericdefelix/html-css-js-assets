<?php
/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 16/08/2016
 * Time: 11:13 AM
 */

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\WebService;
use App\Http\Controllers\Controller;

class UserController extends Controller
{


    /**
     * Deal with the GET request
     */
    public function index()
    {
    }

    /**
     * Deal with POST of request
     */
    public function create()
    {
    }

    /**
     * Function to log user to the user table once he/she has logged in
     * @param $username
     * @param $token
     * @return bool
     * @throws \Exception
     */
    public function store($username, $token)
    {
        $user = new User();
        $user->username = $username;
        $user->token =$token;
        $user->validity = 1;


        if ($user->save()) {
            return true;
        } else {
            throw new \Exception("Could not register login attempt");
        }
    }

    /**
     * POST/PATCH
     */
    public function update()
    {
    }

    /**
     * DELETE action
     */
    public function delete()
    {
    }

    /**
     * function to log user in
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $authService = new \App\Services\AuthService();

        try {
            $user = $authService->isValidUser($request->input('username'), $request->input('password'));
            if ($this->store($request->input('username'), $user['token'])) {
                return response()->json(
                    array(
                        'response'=>200,
                        'token'=> $user['token'],
                        'name' => $user['name']
                    ),
                    200
                );
            }
        } catch (\Exception $e) {
            return response()->json(
                array(
                    'response'=>404,
                    'message'=>$e->getMessage()
                ),
                404
            );
        }
    }

    /**
     * Log out function
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function logout(Request $request)
    {
        try {
            $authService = new \App\Services\AuthService();
            if ($authService->findBy($request->input('token'))) {
                return response()->json(
                    array(
                      'response'=> 200
                    ),
                    200
                );
            }
        } catch (\Exception $e) {
            return response()->json(
                array(
                  'response'=> 404,
                  'message'=> $e->getMessage()
                ),
                404
            );
        }
    }
}
