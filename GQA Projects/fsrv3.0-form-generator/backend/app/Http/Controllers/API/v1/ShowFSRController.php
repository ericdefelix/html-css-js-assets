<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FSRService;

class ShowFSRController extends Controller
{
    public function index($id)
    {
        
        $fsrId = $id;
            if (!empty($fsrId)) {
                $cacheService = new \App\Services\CacheService();
                $FSR = $cacheService->getCache($fsrId);
                if(!isset($FSR)){
                  throw new Exceptions\FsrNotFoundException();
                }
                return $FSR;
             } else {
              throw new Exceptions\MissingParamException('fsr_id'); 
            }
    }
    
    /**
     * 
     * @param Request $request
     * @return type $id
     * @throws \Exception
     * @throws \App\Exceptions\FsrNotFoundException
     */
    public function getID(Request $request)
    {
       
        try
        {
            $fsrUrl = ($request->has('url')) ? $request->input('url') : '';
            if (isset($fsrUrl)) {
                $fsrService =new FSRService;
                $id = $fsrService->getId($fsrUrl);
                $session = $fsrService->getUniqueID($id);
                //print_r($id);
                $FSR = $this->index($id);
                 return response()->json(
                        array(
                            'response'=>200,
                            'token'=> $session,
                            'data'=> json_decode($FSR->structure)
                        ),200
                        );
            } else {
                throw new \App\Exceptions\FsrNotFoundException;
            }
       } catch(\Exception $e) {
           return response()->json(
                   array(
                       'response' => 404,
                       'message' => $e->getMessage()
                   )
                   );
       }
          
    }
}
