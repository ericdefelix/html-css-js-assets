<?php
/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 16/08/2016
 * Time: 11:13 AM
 */

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MappingController extends Controller
{

    /**
     * MappingController constructor.
     * Middleware authorizes each request before controller action
     */
    public function __construct()
    {
        $this->middleware('tokenize');
    }
    /**
     * Deal with the GET request
     */
    public function index(Request $request)
    {
          
          $mapService = new \App\Services\MappingService();
          $dataType = $request->has('type')? $request->input('type'):'';
         
          if($dataType != '') {
              $mappedElements = $mapService->findMappedByType($dataType);
          } else {
            $mappedElements = $mapService->findAllMapped();
          }
          return response()->json(
              array(
                  'response'=>200,
                  'count'=>$mappedElements['count'],
                  'data'=>$mappedElements['data']

              ),
              200
          );
    }

    /**
     * Deal with POST of request
     */
    public function create()
    {
        
    }

    /**
     * PUT request
     */
    public function store()
    {
    }

    /**
     * POST/PATCH
     */
    public function update()
    {
    }

    /**
     * DELETE action
     */
    public function delete()
    {
    }
}
