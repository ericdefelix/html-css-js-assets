<?php
/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 16/08/2016
 * Time: 11:13 AM
 */

namespace App\Http\Controllers\API\v1;

use App\Exceptions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AuthService;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('tokenize');
        
    }

    /**
     * Deal with the GET request
     */
    public function index(Request $request)
    {
        
        if ($request->has('fsr_id')) {
            
            $response =  $this->getPages($request->fsr_id);
            return response()->json($response,200);            
            
        } elseif ($request->has('id')) {
            
            return $this->getPage($request->id);
            
        }
    }
    
    private function getPages($fsrId)
    {
        $service = new \App\Services\PageService;
        return ($service->getPages($fsrId));
    }
    
    private function getPage($pageId)
    {
        $service = new \App\Services\PageService;
        return ($service->getPage($pageId));
    }


    /**
     * Handle the POST (create) requests
     * @param \Illuminate\Http\Request $request
     * @return type
     * @throws Exceptions\MissingParamException
     */
    public function create(\Illuminate\Http\Request $request)
    { 
        
        $token = $request->has('token') ? $request->input('token') : '';
        if (!$request->has('data')) {
            throw new Exceptions\MissingParamException('data');
        }
        if (!$request->has('fsr_id')) {
            throw new Exceptions\MissingParamException('FSR ID');
        }
        $service = new \App\Services\PageService;
        $authService = new AuthService();
        
        $create = $service->createPage( 
                    $request->fsr_id, 
                    json_decode($request->data, true));
       //$createdContent = $service->getPage($create);
        
        $authService->pageOn($create,$token);
        
        return response()->json([
            'response'  => 200,
            'id'      => $create
        ],200);
        
    }

    /**
     * PUT request
     */
    public function store()
    {
    }

    /**
     * POST/PATCH
     */
    public function update(\Illuminate\Http\Request $request)
    {
        if (!$request->has('data')) {
            throw new Exceptions\MissingParamException('data');
        }
        if (!$request->has('fsr_id')) {
            throw new Exceptions\MissingParamException('FSR ID');
        }
        if (!$request->has('page_id')) {
            throw new Exceptions\MissingParamException('Page ID');
        }
        $token =  ($request->has('token')) ? $request->input('token') : '';
        $authService = new AuthService();
        $service = new \App\Services\PageService;
        $authService->pageOn($request->input('page_id'),$token);
        $create = $service->updatePage(
                    $request->page_id,
                    $request->fsr_id,
                    json_decode($request->data, true));
        $createdContent = $service->getPage( $request->page_id);
        return response()->json([
            'response'  => 200,
            'data'      =>$createdContent['data']
        ],200);
    
    }
    
    
     /**
     * DELETE action
     */
    public function delete()
    {
    }
}
