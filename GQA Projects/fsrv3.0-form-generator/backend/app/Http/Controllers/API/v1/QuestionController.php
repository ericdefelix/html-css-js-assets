<?php
/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 16/08/2016
 * Time: 11:13 AM
 */

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Services\QuestionService;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('tokenize');
    }
    /**
     * 
     * Function to list all questions
     * return the array of questions
     */
    public function index(\Illuminate\Http\Request $request)
    {
        try
        {
            $questionService = new QuestionService();
            if(null !== $request->input('id'))
            {
                $question = $questionService->findBy($request->input('id'));
                if (isset($question)){
                return response()->json(
                    array(
                        'response'=>200,
                        'data'=> $question
                    )
                );
                } 
            } else {
                $listQuestions = $questionService->findAll();
                if (isset($listQuestions)) {
                    return response()->json(
                        array(
                            'response'=>200,
                            'count'=>$listQuestions['count'],
                            'data'=>$listQuestions['data']
                        ),200
                        );
                }   
            }
            
        }
        catch(\Exception $e)
        {
            return response()->json(
                    array(
                        'response'=>404,
                        'message'=>$e->getMessage()
                    ),404
                    );
        }
    }
    
    
    /**
     * Deal with POST of request
     */
    public function create()
    {
        
    }

    /**
     * PUT request
     */
    public function store()
    {
    }

    /**
     * POST/PATCH
     */
    public function update()
    {
    }

    /**
     * DELETE action
     */
    public function delete()
    {
    }
}
