<?php
/**
 * Created by PhpStorm.
 * User: prasanth
 * Date: 16/08/2016
 * Time: 11:13 AM
 */
namespace App\Http\Controllers\API\v1;

use App\Exceptions;
use App\Http\Controllers\Controller;

class ResponseController extends Controller
{
    /**
     * call middleware
     */
    public function __construct()
    {
       // $this->middleware('tokenize');
    }
      
    /**
     * Deal with POST of request
     * initialises the FSR and makes it ready to accept
     */
    
    public function updateResponse(\Illuminate\Http\Request $request)
    {
        if (!$request->has('id')){
            throw new Exceptions\MissingParamException('id');
        }elseif (!$request->has('token')){
            throw new Exceptions\MissingParamException('session token'); 
        }else {
            if ($request->has('data')) {
                if (!is_array($request->data)) {
                    throw new Exceptions\MissingParamException ('invalide data format');
                }else { // everything is there :D
                    $service = new \App\Services\ResponseService();
                    $update = $service->updateResponse($request);
                    if ($update) {
                        return response()->json(
                            array (
                                'response' => 200,
                            ),
                            200
                        );
                    }else {
                         return response()->json(
                            array (
                                'response' => 404,
                                'message'  => "Unable to save data"
                            ),
                            404
                        );
                    }
                }
            }else {
                throw new Exceptions\MissingParamException('data');
            }
        }
        return $this->update($request->input('id'), $request);
    }
}
