<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//date_default_timezone_set('Australia/Sydney');

// FSR Management
$app->get('fsr/', 'API\v1\FSRController@index');
$app->post('fsr/', 'API\v1\FSRController@handlePost');
$app->put('fsr/', 'API\v1\FSRController@store');
$app->delete('fsr/', 'API\v1\FSRController@delBg');
$app->get('preview/','API\v1\FSRController@getFSR');
 
// USER Management
$app->post('login/','API\v1\UserController@login');
$app->get('logout/','API\v1\UserController@logout');

// Page Management
$app->get('page/', 'API\v1\PageController@index');
$app->post('page/', 'API\v1\PageController@create');
$app->patch('page/', 'API\v1\PageController@update');
$app->put('page/', 'API\v1\PageController@update');

// Mapping
$app->get('mapping/','API\v1\MappingController@index');

// Questions
$app->get('question/','API\v1\QuestionController@index');
//$app->get('front/fsr','API\v1\ShowFSRController@index');
$app->get('front/fsr','API\v1\ShowFSRController@getID');

// Response
$app->post('/front/response/','API\v1\ResponseController@updateResponse');
/*

$app->get('fsr/{id}', function($id){
    $rout = new App\Http\Controllers\API\v1\FSRController;
    $rout->index($id);
    });
*/
$app->get('/', function () {
   return $this->app->version();
});



