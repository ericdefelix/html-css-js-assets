<?php

namespace App\Listeners;

use App\Events\FsrCreatedEvent;
use App\Events\FsrEditedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\CacheService;
use App\Services\FSRService;

class FsrEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the Creation event.
     *
     * @param  $event
     * @return void
     */
    public function handle($event)
    {
        if ($event instanceof FsrCreatedEvent) {
            $service = new CacheService;
            $fsrService =new FSRService;
             $fsrService->updateFsrName($event->getId());
            $service->createCache($event->getId());
           
            
        }
        elseif ($event instanceof FsrEditedEvent ){
            $service = new CacheService;
            $service->updateCache($event->getId());
        }
        
    }
    
    
}
