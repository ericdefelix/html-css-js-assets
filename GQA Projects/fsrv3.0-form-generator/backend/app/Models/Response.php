<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Response
 *
 * @author mohammada
 */

class Response extends Model
{
    protected $table = 'response';
    protected $fillable = ['response'];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    public function fsr()
    {
        return $this->belongsTo('App\Models\FSR');
    }
    

}