<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of FSR
 *
 * @author mohammada
 */

class Question extends Model
{
    protected $table = 'question';
    protected $primaryKey = 'question_id';
    protected $fillable = ['question', 'sub_title', 'validation', 'event_trigger', 'type', 'button_text', 'placeholder'];
    
    public $timestamps = false;
    
    public function pages()
    {
        return $this->belongsToMany('App\Models\Page', 'page_question', 'question', 'page' );
    }
    
    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'question');
    }
    public function mapping()
    {
        return $this->belongsTo('App\Models\Mapping');
    }
    
    
}