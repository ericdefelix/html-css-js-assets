<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 25/08/2016
 * Time: 10:38 AM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    
    public function pages()
    {
        return $this->belongsToMany('App\Models\Page','user_page','user_id','page_id');
    }
}