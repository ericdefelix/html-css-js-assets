<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 29/08/2016
 * Time: 11:09 AM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends Model
{
    protected $table = 'access_log';
}