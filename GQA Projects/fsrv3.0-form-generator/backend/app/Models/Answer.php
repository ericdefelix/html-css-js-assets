<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of FSR
 *
 * @author mohammada
 */

class Answer extends Model
{
    protected $table = 'answer';
    protected $primaryKey = 'answer_id';
    protected $fillable = ['answer', 'question'];
    
    public $timestamps = false;

    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }
    
}