<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of FSR
 *
 * @author mohammada
 */

class FSR extends Model
{
    protected $table = 'fsr';
    protected $primaryKey = 'fsr_id';
    protected $guarded = ['fsr_id','name','url','created_at','updated_at','created_by'];
    
    public function pages()
    {
        return $this->belongsToMany('App\Models\Page', 'fsr_page', 'fsr', 'page')
                ->withPivot('sort_order');
    }
    
    public function response()
    {
        return $this->hasMany('App\Models\Response');
    }
    
}