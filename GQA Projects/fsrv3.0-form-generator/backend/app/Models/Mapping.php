<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 31/08/2016
 * Time: 3:03 PM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Mapping extends Model
{
   protected $table = 'mapping';
   
    public $timestamps = false;
   
   public function question()
   {
       return $this->hasOne('App\Models\Question','mapping_id');
   }
}