<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of FSR
 *
 * @author mohammada
 */

class Page extends Model
{
    protected $table = 'page';
    protected $primaryKey = 'page_id';
    
    
    public function fsrs()
    {
        return $this->belongsToMany('App\Models\FSR', 'fsr_page', 'page', 'fsr');
                
    }
    
    public function questions()
    {
        return $this->belongsToMany('App\Models\Question', 'page_question', 'page', 'question' );
    }
    
    public function users()
    {
        return $this->belongsToMany('App\Models\User','user_page','page_id','user_id');
    }
}