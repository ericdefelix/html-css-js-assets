<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use DB;
use App\Models\Question;
use Illuminate\Database\Eloquent\Model;

class AnswerService
{
    /**
     * handles answers
     * @param type $questionID
     * @param type $opts
     * @throws Exception
     * @throws \App\Exceptions\MissingParamException
     */
    public function handleAnswers ($questionID, $opts)
    {
        if (isset($opts['answers']) && is_array($opts['answers'])) {
            $currentAnswers = \App\Models\Question::where('question_id', '=', $questionID)->first();
            if ($currentAnswers == null) {
                throw new \Exception('Question not found');
            }
            $ansIDs = array();
            foreach ($currentAnswers->answers as $currentAnswer){
                $ansIDs[] = $currentAnswer->answer_id;
            }            
            foreach ($opts['answers'] as $answer){
                if (isset ($answer['id'])) { // edit
                    // check exists
                    if (in_array($answer['id'], $ansIDs)) {
                        
                        if (isset($answer['title']) && !empty($answer['title'])) {
                            $answ = \App\Models\Answer::where('answer_id', '=', $answer['id'])
                                ->update(['answer' => $answer['title']]);
                        }
                        foreach ($ansIDs as $key => $ansID){
                            if ($ansID == $answer['id']) {
                                unset($ansIDs[$key]);
                            }
                        }
                    }else { // it's not there so lets add the answer
                        throw new \Exception('The answer ID does not belong to that question '.$answer['id']);
                    }
                    
                }else { // $answer['id'] is not set
                    if (isset($answer['title']) && !empty($answer['title'])) {
                            $answerObj = new \App\Models\Answer;
                            $answerObj->answer = $answer['title'];
                            $answerObj->question = $questionID;
                            $saved = $answerObj->save();
                    }else { // no title - throw exception
                        throw new \App\Exceptions\MissingParamException('Answer title');
                    }
                }
            }
            // delete the remaining answers
            if (is_array($ansIDs) && count($ansIDs) > 0) {
                foreach ($ansIDs as $toDelId){
                $answ = \App\Models\Answer::where('answer_id', '=', $toDelId)
                    ->delete();
                }
            }
        }
    }
      
}
