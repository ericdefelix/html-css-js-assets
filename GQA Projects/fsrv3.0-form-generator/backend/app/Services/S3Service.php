<?php

/**
 * The S3 Upload class
 *
 */

namespace App\Services;

use Aws\S3\S3Client;

class S3Service {

   /**
    * Settings
    *
    */

    private $s3;
    private $s3Key 	= 'AKIAJT2I7FIKLM7SWLZQ';
    private $s3Secret 	= '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';
    private $region 	= 'ap-southeast-2';
    private $bucket 	= 'gqwebsite';
    public $uploadDir 	= 'fsrV3/';
    public $encodeFileNames = true;
    public $allowSSL = false;
    public $sslCertFilePath = 'cacert.pem';

    public function __construct ()
    {
        if ($this->allowSSL){
                $sslParam = 'ssl.certificate_authority';
                $sslVal = $this->sslCertFilePath;
        }else {
                $sslParam = 'scheme';
                $sslVal = 'http';
        }
        $this->s3 = S3Client::factory(array(
            'credentials' => array (
                'key' => $this->s3Key,
                'secret' => $this->s3Secret
                ),
            'region'  => $this->region,
            'version' => 'latest',
            'scheme' => 'http',
            )
        );
    }


    public function uploadFile ($fileName, $filePath, $contentType, $bucket = false)
    {
        if (!$bucket) $bucket = $this->bucket;
        if ($this->encodeFileNames){
                $fileName = urlencode ($fileName);
        }
        try {
            $result = $this->s3->putObject(array(
                'Bucket'       => $bucket,
                'Key'          => $this->uploadDir.$fileName,
                'SourceFile'   => $filePath ,
                'ContentType'  => $contentType,
                'ACL'          => 'public-read',
                ));
            } catch (Aws\S3\Exception\S3Exception $e) {
                      echo $e->getMessage();
            }
        return $result['ObjectURL'];
    }	
}// end of class