<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use DB;
use App\Exceptions;
use App\Events;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\AuthService;
use App\Models\Response;
use App\Models\Question;

/**
 * Description of FSRService
 *
 * @author mohammada
 */


class ResponseService
{
    /**
     * Function to get an update the response (response) value
     * @param $request - not that it is already validated
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($request)
    {
        try {
            $response = Response::where(
                    [
                        'fsr' => $request->id,
                        'session' => $request->token
                    ])->firstOrFail();
            if ( $response instanceof \App\Models\Response ) {
                // update
                $serialzed = serialize($request->data);
                $response->update(['response' => $serialzed]);
                return true;
            } else {
                throw new \App\Exceptions\ResponseNotFoundException;
            }
        }  catch ( ModelNotFoundException $e ){
            throw new \App\Exceptions\ResponseNotFoundException();
        }
    }

    /**
     *
     * @param array $options
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findAll(Array $options)
    {
            $fsrQB = DB::table('fsr');
            $totalCount = $fsrQB->count();
            if ($totalCount == 0) {
                throw new \App\Exceptions\NoFSRsException();
            }
            $result = $fsrQB
                    ->select("fsr_id as id", "name", "status", "url", "created_by", "created_at as created_by")
                    ->orderBy ($options['sortBy'], $options['sortOrder'])
                    ->skip ($this->getOffset($options['page'], $options['limit']))
                    ->take ($options['limit'])
                    ->get();
            return array('count' => $totalCount, 'data' =>$result);
    }

    /**
     * Creates new FSR
     * @return type
     * @throws \App\Exceptions\RequestNotDoneException
     */
    public function createFSR()
    {
       try {
            $userService = new AuthService();
            $user = $userService->findUser($_REQUEST['token']);
             $id = $fsrQB = DB::table('fsr')
                ->insertGetId(array(
                    'created_by' => $user->username,
                    'created_at' => date('Y-m-d H:i:s')
                ));
            // fire the event (listener is Listeners/FsrEventListener)
            event ( new Events\FsrCreatedEvent($id));
            return $id;
       } catch (Exception $e) {
           throw new \App\Exceptions\RequestNotDoneException();
       }
    }
    /**
     * updates the database name and url of FSR with id linked to it
     * @param type $id
     * @throws \App\Exceptions\FsrNotFoundException
     */
    public function updateFsrName($id)
    {

        try
        {
            $fsrDetails = FSR::where('fsr_id',$id)->first();
            if ($fsrDetails instanceof FSR ) {
            $fsrDetails->name ='FSR'.$id;
            $fsrDetails->url = 'https://www.gqaustralia.edu.au/FSR'.$id;
            $fsrDetails->save();
            }
        } catch(Exception $e) {
            throw new \App\Exceptions\FsrNotFoundException();
        }
    }

    /**
     * Updates the FSR with relevant information (includes mass update and status update)
     * @param type $id
     * @param array $opts
     * @return type
     * @throws Exceptions\RequestNotDoneException
     * @throws Exceptions\MissingParamException
     */
    public function updateFSR ($id, $opts, $pages = false)
    {
        $qry = \App\Models\FSR::where('fsr_id', '=', $id);
        if (null === $qry->first()) {
            // FSR to edit does not exist
            throw new Exceptions\RequestNotDoneException();
        }
        $opts['updated_at'] = date ('Y-m-d H:i:s');
        if ($opts === null) {
            // No update parameters sent
            throw new Exceptions\MissingParamException('any update parameters');
        }
        $update = $qry->update($opts);
        if ($pages) {
            $pageService = new PageService;
            foreach ($pages as $page){
                $pageService->updatePage($page['id'], $id, $page);
            }
        }
        if ($update) {
            event ( new Events\FsrEditedEvent($id));
        }
        return $id;
    }

    /**
     *
     * @param type $request
     * @return type
     */
    public function generateUpdatesQry($request)
    {
        $opts = array();
        $vars = array ('name', 'url', 'gtm_tag', 'keywords', 'status');
        foreach ($vars as $var)
        if ($request->has($var)) {
            $opts[$var] = $request->input($var);
        }
        if (isset ($opts['keywords']) && is_array($opts['keywords'])) {
            $opts['keywords'] = serialize($opts['keywords']);
        }
        return $opts;
    }
    /**
     *
     * @param type $request
     * @return type
     * @throws Exceptions\MissingParamException
     * @throws Exceptions\RequestNotDoneException
     */
    public function uploadBGImage($request)
    {
        if (!$request->hasFile('image')) {
            throw new Exceptions\MissingParamException('image');
        }else {
            if (!$request->image->isValid()) {
                throw new Exceptions\RequestNotDoneException;
            }else {
                $s3 = new S3Service;
                $result = $s3->uploadFile(
                    $request->image->getClientOriginalName(),
                    $request->image->path(),
                    $request->image->getMimeType()
                );
                $updateId = $this->updateFSRImage($request->id, $result);
                return $updateId;
            }
        }
    }

    public function isUnique ($request)
    {
        if (!$request->has('key')) {
            throw new Exceptions\MissingParamException('key');
        }
        if (!$request->has('value')) {
            throw new Exceptions\MissingParamException('value');
        }
        $uniqueTest = DB::table('fsr')
                ->select('fsr_id')
                ->where($request->key, 'like', $request->value)
                ->first();
        if (!isset($uniqueTest)) {
            return null;
        }else {
            return $uniqueTest->fsr_id;
        }

    }
    /**
     *
     * @param type $fsrId
     * @param type $imagheURL
     * @return type
     */
    public function updateFSRImage($fsrId, $imagheURL)
    {
        $opts = array ('bg_image' => $imagheURL);
        return $this->updateFSR($fsrId, $opts);
    }
    /**
     * Utility function to get the offset for SQL OFFSET
     * @param type $page
     * @param type $limit
     * @return type
     */
    private function getOffset ($page, $limit)
    {
        return ($limit *($page - 1));
    }

    /**
     * Function to get an update the response (response) value
     * @param $request - note that it is already validated
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateResponse($request)
    {
        try {
            $response = Response::firstOrNew([
                        'fsr' => $request->id,
                        'session' => $request->token
                    ]);
            if ( $response instanceof \App\Models\Response ) {
                $response->fsr = $request->id;
                $response->session = $request->token;
                $response->response = json_encode($request->data);
                // addition for the capturing the parametr
                if(!empty($request->utm_source))
                $response->source = $request->utm_source;
                if(!empty($request->utm_medium))
                $response->medium = $request->utm_medium;
                if(!empty($request->utm_campaign))
                $response->campaign = $request->utm_campaign;
                if(!empty($request->utm_term))
                $response->term = $request->utm_term;
                if(!empty($request->utm_content))
                $response->content = $request->utm_content;
                $response->save();
                return true;
            } else {
                throw new \App\Exceptions\ResponseNotFoundException;
            }
        }  catch ( ModelNotFoundException $e ){
            throw new \App\Exceptions\ResponseNotFoundException();
        }
    }

    public function getAllNonSyncedResponse() {
        $count = Response::where('is_synced', 0)->count();
        $res = array();
        if($count > 0) {
            foreach (Response::where([['is_synced', 0],['response','<>','']])->cursor() as $response) {

                $resp = array();
                $responseArray = json_decode($response->response,true);
                // additional parameters
                $source = $response->source;
                $resp = $this->processResponseForSync($responseArray, $source);
                $resp["id"] = $response->id;
                $res[] = $resp;
            }
        }
        return $res;
    }

    private function processResponseForSync($responseArray, $source = "") {
        $syncData = [
            'fname' => '',
            'lname' => '',
            'email' => '',
            'mobile' => '',
            'desc' => '',
            'state' => '',
            'faculty' => '',
            'country' => '',
            'source' => '',
            'zc_gad' => '',
            'refcode' => '',
            'gclickid' => '',
            'keyword' => '',
            'voucher' => '',
            'experience' => '',
            'experience_place' => '',
            'has_quals' => '',
            'quals_age' => '',
            'qual_demanded' => '',
            'heardaboutus' => '',
            'reason' => '',
            'enquiryTime' => ''
            ];
        $syncMap = [
            'fname' => 'First name',
            'lname' => 'Last name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'desc' => '',
            'state' => 'Post Code',
            'faculty' => 'Industry',
            'country' => 'Country',
            'source' => 'Source',
            'zc_gad' => '',
            'refcode' => '',
            'gclickid' => '',
            'keyword' => 'Keyword',
            'voucher' => '',
            'experience' => 'Years of Experience',
            'experience_place' => 'Australian or Overseas experience',
            'has_quals' => 'Other Qualifications',
            'quals_age' => 'Age of qualifications',
            'qual_demanded' => 'Qualification Name',
            'heardaboutus' => 'Heard About Us',
            'reason' => 'Reason For Qualification',
            'enquiryTime' => 'Enquiry Time'
            ];
        $resume = '';
        if (!empty($responseArray)) {
            foreach ($responseArray as $data) {
                $question_id = $data["question_id"];
                $answer = (isset($data['answer'])) ? $data['answer'] : '';
                if (is_array($answer))
                    $answer = implode(",", $answer);
                $questionObject = Question::find($question_id);
                if ($questionObject !== NULL) {
                    $mapping = $questionObject->mapping->attribute;
                    $maps = array_keys($syncMap, $mapping);
                    if (count($maps) > 0) {
                        foreach ($maps as $map) {
                            if (empty($syncData[$map]))
                                $syncData[$map] = $this->cleanData($answer);
                            else
                                $syncData[$map] .= $this->cleanData($questionObject->question) . " : " . $this->cleanData($answer) . " , ";
                        }
                    } else {
                        if (trim($questionObject->question) != '')
                            $syncData['desc'] .= $this->cleanData($questionObject->question) . " : " . $this->cleanData($answer) . " ,  ";
                    }
                    if ($mapping == 'Resume')
                        $resume = $answer;
                }else {
                    $syncData['desc'] .= "$question_id : " . $this->cleanData($answer) . " , ";
                }
            }
        }
        // updated to pass the value
        if (empty($syncData['source']))
            $syncData['source'] = $this->cleanData($source);
        else
            $syncData['source'] .= " , " . $this->cleanData($source);
        $resp['data'] = $syncData;
        $resp['resume'] = $resume;
        return $resp;
    }

    /**
     * For getting removed special character message from answers
     * @param String $answer
     * @return String $answer
     */
    public function cleanData($answer)
    {
        $answer = preg_replace("/\%%[^)]+\%%/","",$answer);
        $answer = preg_replace('/[&]/', 'and', $answer);
        $answer = preg_replace('/[^A-Za-z0-9\-@:,?. ]/', '', $answer);
        return $answer;
    }

    public function updateIsSync($id){
        $response = Response::find($id);
        if($response !== NULL) {
            $response->is_synced = 1;
            $response->save();
        }
    }
}
