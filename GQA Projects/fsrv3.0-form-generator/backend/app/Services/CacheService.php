<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use DB;
use App\Exceptions;

/**
 * Deals with the caching needs of the app
 * 
 * @author mohammada
 */


class CacheService 
{

    /**
     * Function to create new cache for an FSR
     * @param $id
     * @throws \App\Exceptions\CacheNotUpdatedException
     */
    public function createCache($id)
    {        
        try {
            $fsrService = new FSRService();
            $fsrInfo = $fsrService->findOne($id);
            $fsr =  DB::table('cache')->insert(
                    array (
                        'structure' => $fsrInfo,
                        'fsr'   => $id,
                        'created_at' => date('Y-m-d H:i:s')
                        )
                    );
        }  catch (Exception $e){
            throw new Exceptions\CacheNotUpdatedException;
        }
    }
    
    /**
     * Function to update the FSR cahce
     * @param $id
     * @throws \App\Exceptions\CacheNotUpdatedException
     */
    public function updateCache($FsrID)
    {        
        try {
            $test = DB::table('cache')->where('fsr',$FsrID)->first();
            if ($test == null) {
                $this->createCache($FsrID);
            }
            $fsrService = new FSRService();
            $fsrInfo = $fsrService->findOne($FsrID);
            $fsrInfo = ($fsrInfo->toArray());
            $pagesServer = new PageService();
            $data = $pagesServer->getPages($FsrID, false);
            $pages = (!empty($data['data'])) ? $data['data'] : [];
            $fsrInfo['pages'] = $pages;
            $fsr =  DB::table('cache')->where('fsr',$FsrID)
                    ->update(
                        array (
                            'structure' => json_encode($fsrInfo),
                            'updated_at' => date('Y-m-d H:i:s')
                            )
                        );
        }  catch (Exception $e){
            throw new Exceptions\CacheNotUpdatedException;
        }
    }
    
    public function getCache($id)
    {
        $this->updateCache($id);
        $output = DB::table('cache')->where('fsr',$id)->first();
        if ($output == null) {
            throw new Exceptions\FsrNotFoundException();
        }
        return $output;
    }
}
