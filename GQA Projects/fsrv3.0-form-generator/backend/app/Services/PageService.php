<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use DB;
use App\Exceptions;
use App\Events;
use Illuminate\Database\Eloquent\ModelNotFoundException;


/**
 * Description of FSRService
 *
 * @author mohammada
 */


class PageService
{
    /**
     * Function to get pages contained in an FSR
     * @param $fsrId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPages($fsrId, $fatal = true)
    {
        try {
            $fsr =  \App\Models\FSR::where('fsr_id', '=', $fsrId)->firstOrFail();
            $pageCount = count($fsr->pages);
            if ($pageCount == 0) {
                if ($fatal) {
                    throw new Exceptions\NoPagesFoundException;
                }
            }
            $responseObj = array (
                'response'  => 200,
                'count'     => $pageCount,
                );
            foreach ($fsr->pages as $page){
               $data = array (
                    'id'          => $page->page_id,
                    'sort_order'  => $page->pivot->sort_order,
                    'gtm_value'   => $page->gtm_event,
                    'unique_name' => $page->unique_name,
                    'status'      => $page->status,
                );
                foreach ($page->questions as $question){

                    $questions = [
                        'id'          => $question->question_id,
                        'title'       => $question->question,
                        'sub_title'   => $question->sub_title,
                        'content'     => $question->content,
                        'button_text' => $question->button_text,
                        'placeholder' => $question->placeholder,
                        'type'        => $question->type,
                        'validation'  => unserialize($question->validation) ? : [],
                        'mapping_id' => $question->mapping_id,
                        'attribute'   =>$question->mapping->attribute,
                        'active'      => $question->mapping->active,
                        'xml_equivalent' => $question->mapping->xml_attribute,
                        'mapping_type'        => $question->mapping->type

                    ];
                    foreach ($question->answers as $answer){
                        $answers = [
                            'id'    => $answer->answer_id,
                            'title' => $answer->answer,
                        ];
                        $questions['answers'][] = $answers;
                    }
                    $data['questions'][] = $questions;
                }
                $responseObj['data'][] = $data;
            }
            return ($responseObj);
        } catch ( ModelNotFoundException $e ){
            throw new \App\Exceptions\FsrNotFoundException;
        }
    }

    /**
     * Function to get details of a certian page
     * @param $fsrId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPage($pageId)
    {
        try {
            $page =  \App\Models\Page::where('page_id', '=', $pageId)->firstOrFail();
            if ($page == null) {
                throw new Exceptions\PageNotFoundException;
            }
            $responseObj = array (
                'response'  => 200,
                );
               $data = array (
                    'id'          => $page->page_id,
                    'gtm_value'   => $page->gtm_event,
                    'unique_name' => $page->unique_name,
                    'status'      => $page->status,
                );
                foreach ($page->questions as $question){
                    $questions = [
                        'id'        => $question->question_id,
                        'title'     => $question->question,
                        'sub_title' => $question->sub_title,
                        'content'   => $question->content,
                        'button_text' => $question->button_text,
                        'placeholder' => $question->placeholder,
                        'type'      => $question->type,
                        'validation'=> unserialize($question->validation) ? : [],
                        'mapping_id' => $question->mapping_id,
                        'attribute' =>$question->mapping->attribute,
                        'active'=> $question->mapping->active,
                        'xml_equivalent' => $question->mapping->xml_attribute,
                        'mapping_type' => $question->mapping->type
                    ];
                    foreach ($question->answers as $answer){
                        $answers = [
                            'id'    => $answer->answer_id,
                            'title' => $answer->answer,
                        ];
                        $questions['answers'][] = $answers;
                    }
                    $data['questions'][] = $questions;
                }
                $responseObj['data'][] = $data;

            return ($responseObj);
        } catch ( ModelNotFoundException $e ){
            throw new \App\Exceptions\FsrNotFoundException;
        }
    }
    /**
     * Finds all FSRs
     * @param array $options
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findAll(Array $options)
    {
            $fsrQB = DB::table('FSR');
            $totalCount = $fsrQB->count();
            if ($totalCount == 0) {
                throw new \App\Exceptions\NoFSRsException();
            }
            $result = $fsrQB
                    ->select("fsr_id as id", "name", "status", "url", "created_by", "created_at as created_by")
                    ->orderBy ($options['sortBy'], $options['sortOrder'])
                    ->skip ($this->getOffset($options['page'], $options['limit']))
                    ->take ($options['limit'])
                    ->get();
            return array('count' => $totalCount, 'data' =>$result);
    }

    /**
     * Handles creating Pages
     * @param int $fsrID
     * @param array $opts
     * @return int
     * @throws \App\Exceptions\RequestNotDoneException
     */
    public function createPage($fsrID, $opts)
    {

        try {
            
            // print_r($opts);
            $pageQB = DB::table('page');

            $createQry = $this->generateUpdatePageQry($opts);
            $createQry['created_at'] = date('Y-m-d H:i:s');
            $id = $pageQB->insertGetId(
                $createQry
            );

            $fsr = \App\Models\FSR::where('fsr_id', '=', $fsrID)->firstOrFail();
            $fsr->pages()->sync(array($id => ['sort_order' => $opts['sort_order']]), false);

            if (is_array($opts['questions']) && count($opts['questions']) > 0) {
                $pageObj = \App\Models\Page::where('page_id', '=', $id)->firstOrFail();
                $questionService = new QuestionService;
                foreach ($opts['questions'] as $question){
                    if (isset ($question['id'])) { // question alreasy exists in DB
                        $questionService->updateQuestion($question['id'], $question);
                        $pageObj->questions()->sync([$question['id']], false);
                    }else {
                        $questionID = $questionService->createQuestion($question);
                        $pageObj->questions()->sync([$questionID], false);
                    }
                }
            }
            event ( new Events\FsrEditedEvent($fsrID));
            return $id;
       } catch (Exception\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
           throw new \App\Exceptions\RequestNotDoneException();
       }
    }


    public function updatePage ($pageId, $fsrId, $opts)
    {
        $qry = \App\Models\Page::where('page_id', '=', $pageId);
        if (null === $qry->first()) {
            throw new Exceptions\PageNotFoundException;
        }
        $opts['updated_at'] = date ('Y-m-d H:i:s');
        if ($opts === null) {
            // No update parameters sent
            throw new Exceptions\MissingParamException('any update parameters');
        }
        // update starts
        try {
            $pageQB = DB::table('page')->where('page_id', '=', $pageId);
            $createQry = $this->generateUpdatePageQry($opts);
            $createQry['updated_at'] = date('Y-m-d H:i:s');
            $update = $pageQB->update($createQry);
            $fsr = \App\Models\FSR::where('fsr_id', '=', $fsrId)->firstOrFail();
            //$fsr->pages()->sync([$pageId], false);
            $fsr->pages()->sync(array($pageId => ['sort_order' => $opts['sort_order']]), false);
            $questionService = new QuestionService;
            $questionService->syncQuestions($pageId, $opts);
            // fire the update event
            event ( new Events\FsrEditedEvent($fsrId));

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw new Exceptions\FsrNotFoundException;
        }
        return ($pageId);

    }

    /**
     *
     * @param type $request
     * @return type
     */
    public function generateUpdatePageQry($data)
    {
        //$test = $this->isUnique($data);
        $opts = array();
        $vars = array ('unique_name', 'status', 'gtm_event');
        foreach ($vars as $var)
        if (array_key_exists ($var, $data)) {
            $opts[$var] = $data[$var];
        }
        return $opts;
    }


    /**
     *
     * @param type $request
     * @return type
     */
    public function generateUpdateQuestionQry($data)
    {
        $opts = array();
        $vars = array ('question', 'event_trigger', 'type');
        foreach ($vars as $var)
        if (array_key_exists ($var, $data)) {
            $opts[$var] = $data[$var];
        }
        return $opts;
    }

    /**
     *
     * @param type $opts
     * @return boolean
     * @throws Exceptions\PageNotSavedException
     */
    public function isUnique ($opts)
    {
        if (!isset ($opts['unique_name'])) {
            return true;
        }else{
            $uniqueTest = DB::table('page')
                ->select('page_id')
                ->where('unique_name', 'like', $opts['unique_name'])
                ->first();
            if (!isset($uniqueTest)) {
                return true;
            }else {
                throw new Exceptions\PageNotSavedException(true);
            }
        }

    }
}
