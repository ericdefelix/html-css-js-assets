<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use DB;
use App\Models\Question;
use Illuminate\Database\Eloquent\Model;

class QuestionService
{
   /**
    * Function to get all questions
    * @return \Symfony\Component\HttpFoundation\Response
    * @throws \Exception e
    */
    public function findAll()
   {
       $questions = Question::all();
       $cnt = $questions->count();
       if ($cnt == 0){
          throw new \Exception('No Questions found');
       }
      $response = array('count' =>$cnt);
      foreach ($questions as $question){
          
          $quest =[
              'id' => $question->question_id,
              'title' => $question->question,
              'sub_title' => $question->sub_title,
              'content' => $question->content,
              'button_text' => $question->button_text,
              'placeholder' => $question->placehodler,
              'type' => $question->type,
              'validation' => unserialize($question->validation)? :[],
              'mapping_id'=>$question->mapping_id,
              'attribute' =>$question->mapping->attribute,
              'mapping_type'=>$question->mapping->type,
              'active'=> $question->mapping->active,
              'xml_equivalent' => $question->mapping->xml_attribute
          ];
          foreach ($question->answers as $answer){
          $answers = [
             'id' => $answer->answer_id,
             'title' => $answer->answer
          ]; 
          $quest['answers'][] =$answers;
          }
          $response['data'][] =$quest;
      }
      return $response;
   }
   
   
   /**
    * 
    * @param type $id
    * @return \Symfony\Component\HttpFoundation\Response
    * @throws \Exception 
    */
   public function findBy($id)
   {
     
       $question =Question::where('question_id',$id)->first();
       
       if (isset($question)){
           $mappingID = $question->mapping_id;
           $maps = \App\Models\Mapping::where ('id',$mappingID)->first();
           $quest  = [
              'id' => $question->question_id,
              'title' => $question->question,
              'sub_title' => $question->sub_title,
              'content'=>$question->content,
              'button_text' => $question->button_text,
              'placeholder' => $question->placeholder,
              'type' => $question->type,
              'validation' => unserialize($question->validation)? :[],
               'mapping_id'=> $question->mapping_id,
              'attribute' => $maps->attribute,
              'mapping_type' => $maps->type,
              'xml-equivalent' => $maps ->xml_attribute,
              'active' => $maps->active
           ];
          
            foreach ($question->answers as $answer){
            $answers = [
             'id' => $answer->answer_id,
             'title' => $answer->answer
          ]; 
          $quest['answers'][] =$answers;
        }
        //$response['data'] =$question;
      
        return $quest;
       } else {
           throw new \Exception("No question found with the corresponding ID");
       }       
   }
   
   /**
    * Create a new questions and returns the created question ID
    * @param type $opts
    * @return int
    */
    public function createQuestion ($opts)
    {
        $mapping = new MappingService;
        $mapTo = (isset ($opts['mapped_to'])) ? $opts['mapped_to'] : false;
        $mappingID = $mapping->mapTo($mapTo);
        $checkMapId = $mapping->mappingExists($mappingID);
        if (isset($opts['validations'])) {
            $opts['validations'] = serialize ($opts['validations']);
        }else {
            $opts['validations'] = serialize([]);
        }
               
        $questionsObj = [
            'question'  => isset($opts['title'])? $opts['title'] : '',
            'sub_title' => isset($opts['sub_title'])? $opts['sub_title'] : '',
            'content' => isset($opts['content'])? $opts['content'] : '',
            'button_text' => isset($opts['button_text'])? $opts['button_text'] : '',
            'placeholder' => isset($opts['placeholder'])? $opts['placeholder'] : '',
            'validation' => $opts['validations'],
            'event_trigger' => isset($opts['event_trigger']) ? $opts['event_trigger'] : '',
            'mapping_id'  => $mappingID,
            'type' => isset($opts['type']) ? $opts['type'] : 'text'
        ];
        $answerService = new AnswerService;
        $qID = DB::table('question')->insertGetId($questionsObj);
        $answerService->handleAnswers($qID, $opts);
        return($qID);
    }
    /**
     * Edit questions
     * @param array $opts
     * @return type
     */
    public function updateQuestion ($qID, $opts)
    {
        $mapping = new MappingService;
        $mapTo = (isset ($opts['mapped_to'])) ? $opts['mapped_to'] : false;
        $mappingID = $mapping->mapTo($mapTo);
        $checkMapId = $mapping->mappingExists($mappingID);
        if (isset($opts['validation'])) {
            $opts['validations'] = serialize ($opts['validation']);
        }else {
            $opts['validations'] = serialize([]);
        }
        $answerService = new AnswerService;
        $questionsObj = [
            'question'  => isset($opts['title'])? $opts['title'] : '',
            'sub_title' => isset($opts['sub_title'])? $opts['sub_title'] : '',
            'content' => isset($opts['content'])? $opts['content'] : '',
            'button_text' => isset($opts['button_text'])? $opts['button_text'] : '',
            'placeholder' => isset($opts['placeholder'])? $opts['placeholder'] : '',
            'validation' => $opts['validations'],
            'event_trigger' => isset($opts['event_trigger']) ? $opts['event_trigger'] : '',
            'mapping_id'  => $mappingID,
            'type' => isset($opts['type']) ? $opts['type'] : 'text'
        ];
        
        DB::table('question')->where('question_id', '=', $qID)->update($questionsObj);
        $answerService->handleAnswers($qID, $opts);
        return($qID);
    }
    /**
     * Syncronise questions of a particular page
     * @param type $pageID
     * @param type $opts
     * @return boolean
     * @throws \App\Exceptions\MissingParamException
     */
    public function syncQuestions ($pageID, $opts)
    {
        if (!is_array($opts['questions']) || !isset ($opts['questions'])) {
            throw new \App\Exceptions\MissingParamException ("questions for page $pageID. "
                . "Can be empty array to remove questions" );
        }
        $currentQuestions = [];
        $pageQB = \App\Models\Page::where('page_id', '=', $pageID)->first();
        // get the current questions
        foreach ($pageQB->questions as $question){
             $pageQB->questions()->detach([$question->question_id]);
        }
        foreach ($opts['questions'] as $question){
            if (isset ($question['id'])) {
                $this->updateQuestion($question['id'], $question);
                $pageQB->questions()->sync([$question['id']], false);
                //$answerService->handleAnswers($question['id'], $question);
            }else {
                $qID = $this->createQuestion($question);
                $pageQB->questions()->sync([$qID], false);
                // $answerService->handleAnswers($qID, $question);
            }
        }
        return true;
    }    
}
