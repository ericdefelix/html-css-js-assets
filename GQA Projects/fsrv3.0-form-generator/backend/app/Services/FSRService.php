<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\Page;
use App\Models\Question;
use DB;
use App\Exceptions;
use App\Events;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\AuthService;
use App\Models\FSR;
use App\Models\Response;

/**
 * Description of FSRService
 *
 * @author mohammada
 */


class FSRService 
{
    /**
     * Function to get an FSR by passing unique identifier
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findOne($id)
    {                
        try {
            $fsr =  \App\Models\FSR::where('fsr_id', '=', $id)->firstOrFail();
            if ( $fsr instanceof \App\Models\FSR ) {
                return $fsr;
            } else {
                throw new \App\Exceptions\FsrNotFoundException();
            }
        }  catch ( ModelNotFoundException $e ){
            throw new \App\Exceptions\FsrNotFoundException();                    
        }
    }

    /**
     *
     * @param array $options
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findAll(Array $options)
    {
            $fsrQB = DB::table('fsr');
            $totalCount = $fsrQB->count();
            if ($totalCount == 0) {
                throw new \App\Exceptions\NoFSRsException();
            }
            $result = $fsrQB
                    ->select("fsr_id as id", "name", "status", "url", "created_by", "created_at")
                    ->orderBy ($options['sortBy'], $options['sortOrder'])
                    ->skip ($this->getOffset($options['page'], $options['limit']))
                    ->take ($options['limit'])
                    ->get();
            return array('count' => $totalCount, 'data' =>$result);
    }
    
    /**
     * Creates new FSR
     * @return type
     * @throws \App\Exceptions\RequestNotDoneException
     */   
    public function createFSR()
    {
       try {
            $userService = new AuthService();
            $user = $userService->findUser($_REQUEST['token']);
             $id = $fsrQB = DB::table('fsr')
                ->insertGetId(array(
                    'created_by' => $user->username,
                    'created_at' => date('Y-m-d H:i:s')
                ));
            // fire the event (listener is Listeners/FsrEventListener)
            event ( new Events\FsrCreatedEvent($id));
            return $id;        
       } catch (Exception $e) {
           throw new \App\Exceptions\RequestNotDoneException();
       }
    }
    /**
     * updates the database name and url of FSR with id linked to it
     * @param type $id
     * @throws \App\Exceptions\FsrNotFoundException
     */
    public function updateFsrName($id)
    {
       
        try
        {
            $names = DB::table('fsr')->pluck('name');
            $urls = DB::table('fsr')->pluck('url');
            do{
                $rand_num = sprintf("%06d", mt_rand(1, 999999)); //generatign random six digit numbers for FSR name and URL
                $FSRname = 'free-skills-review-'.$rand_num;
                $FSRurl = $FSRname;
               
            }while(in_array($FSRname,$names));
            $fsrDetails = FSR::where('fsr_id',$id)->first();
            if ($fsrDetails instanceof FSR) {   
                if (!(in_array($FSRname,$names)) || !(in_array($FSRurl,$urls)) ) { // this condition is to make sure that no FSR exists in table with the above name or url
                     
                        $fsrDetails->name = $FSRname ;
                        $fsrDetails->url = $FSRurl;
                        $fsrDetails->save();
                        
                    } else {
                       $FSRname = 'FSR'.uniqid();
                       $FSRurl = uniqid();
                       $fsrDetails->name = $FSRname ;
                       $fsrDetails->url = $FSRurl;
                       $fsrDetails->save();
                    }
            } else {
                throw new \App\Exceptions\FsrNotFoundException();
            }
           
        } catch(Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Updates the FSR with relevant information (includes mass update and status update)
     * @param type $id
     * @param array $opts
     * @return type
     * @throws Exceptions\RequestNotDoneException
     * @throws Exceptions\MissingParamException
     */
    public function updateFSR ($id, $opts, $pages = false)
    {
        $qry = \App\Models\FSR::where('fsr_id', '=', $id);
        if (null === $qry->first()) {
            // FSR to edit does not exist
            throw new Exceptions\RequestNotDoneException();
        }
        $FSRObj = $qry->first();
        //print_r($FSRObj);
        $opts['updated_at'] = date ('Y-m-d H:i:s');
        if ($opts === null) {
            // No update parameters sent
            throw new Exceptions\MissingParamException('any update parameters');
        }
        //print_r($opts);exit;
        
        //print_r($qry);
        $update = $qry->update($opts);
        if ($pages !== false) {
            foreach ($FSRObj->pages as $pageToDetach){
                $FSRObj->pages()->detach([$pageToDetach->page_id]);
            }
            $pageService = new PageService;
            foreach ($pages as $page){
                
                if (!isset ($page['id'])) {
                    $pageService->createPage($id, $page);
                }else {
                    $pageService->updatePage($page['id'], $id, $page);
                }
            }
        }
        if ($update) {
            event ( new Events\FsrEditedEvent($id));
        }
        return $id;
    }
    
    /**
     * 
     * @param type $request
     * @return type
     */
    public function generateUpdatesQry($request)
    {
        $opts = array();
        $vars = array ('name', 'url', 'gtm_tag', 'keywords', 'status');
        $opts['gtm_tag'] = '';
        foreach ($vars as $var)
        if ($request->has($var)) {
            $opts[$var] = $request->input($var);
        }
				
        $opts['url'] = str_replace(getenv('URL'),'',$opts['url']);
        if (isset ($opts['keywords']) && is_array($opts['keywords'])) {
            $opts['keywords'] = serialize($opts['keywords']);
        }
        return $opts;
    }
    /**
     * 
     * @param type $request
     * @return type
     * @throws Exceptions\MissingParamException
     * @throws Exceptions\RequestNotDoneException
     */
    public function uploadBGImage($request)
    {
        if (!$request->hasFile('image')) {
            throw new Exceptions\MissingParamException('image');
        }else {
            if (!$request->image->isValid()) {
                throw new Exceptions\RequestNotDoneException;
            }else {
                $s3 = new S3Service;
                $result = $s3->uploadFile(
                    $request->image->getClientOriginalName(), 
                    $request->image->path(), 
                    $request->image->getMimeType()
                );
                $updateId = $this->updateFSRImage($request->id, $result);
                return $updateId;
            }
        }
    }
    
    public function isUnique ($request)
    {
        if (!$request->has('key')) {
            throw new Exceptions\MissingParamException('key');
        }
        if (!$request->has('value')) {
            throw new Exceptions\MissingParamException('value');
        }
        $uniqueTest = DB::table('fsr')
                ->select('fsr_id')
                ->where($request->key, 'like', $request->value)
                ->first();
        if (!isset($uniqueTest)) {
            return null;
        }else {
            return $uniqueTest->fsr_id;
        }
        
    }
    /**
     * 
     * @param type $fsrId
     * @param type $imagheURL
     * @return type
     */
    public function updateFSRImage($fsrId, $imagheURL)
    {
        $opts = array ('bg_image' => $imagheURL);
        return $this->updateFSR($fsrId, $opts);
    }
    /**
     * Utility function to get the offset for SQL OFFSET
     * @param type $page
     * @param type $limit
     * @return type
     */
    private function getOffset ($page, $limit)
    {
        return ($limit *($page - 1));
    }
    
    
    public function getUniqueId($fsrID)
    {
        $session = uniqid('',true);
        //$result = DB::table('response')->insert(['session' => $session, 'fsr' => $fsrID]);
        $response = new Response();
        $response->fsr = $fsrID;
        $response->session = $session;
        
        if (!$response->save()) {
        
            throw new \Exception('Not able to insert unique session ID into the database');
        }
       
        return $session;
    }
    
    /**
     * 
     * @param type $url
     * return type $id
     * @throws \Exception
     */
    
    public function getId($url)
    {
       
        $fsrArray = DB::select("SELECT fsr_id FROM fsr WHERE url LIKE '$url'");
        //print_r ($fsrID);
        foreach( $fsrArray as $fsr) {
        $fsrID = $fsr->fsr_id;
        }
        if (!isset($fsrID)) {
             throw new \Exception('FSR not found with the given URL');
        
        }
        return $fsrID  ;
    }
    
    /**
     * Copy the FSR with relevant information (includes mass update and status update)
     * @param type $id
     * @param array $opts
     * @return type
     * @throws Exceptions\RequestNotDoneException
     * @throws Exceptions\MissingParamException
     */
    public function copyFSR ($id, $opts, $pages = false)
    {
        $qry = \App\Models\FSR::where('fsr_id', '=', $id);
        if (null === $qry->first()) {
            // FSR to edit does not exist
            throw new Exceptions\RequestNotDoneException();
        }
        $FSRObj = $qry->first();
        //print_r($FSRObj);
        $opts['updated_at'] = date ('Y-m-d H:i:s');
        if ($opts === null) {
            // No update parameters sent
            throw new Exceptions\MissingParamException('any update parameters');
        }
        //print_r($pages);
        $update = $FSRObj->update($opts);
        if ($pages !== false) {          
            foreach ($pages as $page){
                $pageObj = Page::find($page["id"]);
                $pageCopy = $pageObj->replicate();
                $pageCopy->save();
                $FSRObj->pages()->sync(array($pageCopy->page_id => ['sort_order' => $page["sort_order"]]), false);
                if(!empty($page['questions'])) {
                    foreach ($page['questions'] as $question) {
                        $questionObj = Question::find($question["id"]);
                        $questionCopy = $questionObj->replicate();
                        $questionCopy->save();
                        $pageCopy->questions()->sync(array($questionCopy->question_id), false);
                        if(!empty($questionObj->answers)){
                            foreach ($questionObj->answers as $answer) {
                                $answerCopy = $answer->replicate();
                                $answerCopy->question = $questionCopy->question_id;
                                $answerCopy->save();
                            }
                        }
                        
                    }
                }


            }
        }
        if ($update) {
            event ( new Events\FsrEditedEvent($id));
        }
        return $id;
    }
    
}
