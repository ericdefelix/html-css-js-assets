<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 23/08/2016
 * Time: 3:24 PM
 */
namespace App\Services;

class WebService
{
    const MAUTH_URL = 'http://auth.gqaus.com.au/index.php';
    //const MAUTH_URL = 'http://52.64.99.141/auth/index.php';



    public function authenticate($username, $password)
    {
        try {
            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', self::MAUTH_URL, ['query' => ['us'=> $username,
                    'pw'=> md5($password)]]);

            $body = $response->getBody();

            $result = json_decode($body, true);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            throw $e;
        }
        
        return $result;
    }
}
