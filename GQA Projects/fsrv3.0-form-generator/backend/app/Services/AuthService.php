<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 23/08/2016
 * Time: 3:11 PM
 */

namespace App\Services;

use App\Models\User;
use App\Models\AccessLog;
use DB;
use Illuminate\Validation\ValidationException;

class AuthService
{
    /**
	* @param $username
    * @param $password
    * @return string
    * @throws \Exception
    */
    public function isValidUser($username, $password)
    {
        try {
                if ($this->validateInput($username, $password)) {    
             
                    if ($this->chkPrivilege($username)) {
                        $res= $this->validateCredentials($username, $password);

                        if (!empty($res)) {
                            return $res;
                        }
                    }
                }
                
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * function to validate input parameters
     * @param $username
     * @param $password
     * @return bool
     * @throws ValidationException
     */

    private function validateInput($username, $password)
    {
        if (null !== $username && null !== $password) {
            if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
                return true;
            } else {
                throw new \Exception("Username must be your email ID");
            }
        } else {
                throw new \Exception('Username and Password must not be empty');
        }
    }


    /**
     * function to check validity username password combination
     * @param $username
     * @param $password
     * @return \App\Services\WebService
     * @throws \Exception
     */

    private function validateCredentials($username, $password)
    {
        $webService = new \App\Services\WebService();
        $credentials = $webService->authenticate($username, $password);
        if ($credentials['code'] == '205') {
             return $credentials;
        } else {
            throw new \Exception('Invalid Username And/or Password Combination');
        }
    }

    /**
     * function to check validity of token and set it to 0
     * @param $token
     * @return bool
     * @throws \App\Exceptions\UserNotFoundException
     * @throws \Exception
     */

    public function findBy($token)
    {
        $user  =  \App\Models\User::where('token', $token)->first();
        // print_r($user);
        if ($user instanceof \App\Models\User) {
            $user->validity = 0;
            if ($user->save()) {
                 $userLog = \App\Models\AccessLog::where('user_id', $user->id)->first();
                if ($userLog instanceof \App\Models\AccessLog) {
                    //$userLog->logged_in = $user->created_at;
                    $userLog->logged_out = new \DateTime('now',new \DateTimeZone('Australia/Sydney'));
                } else {
                    $userLog = new AccessLog();
                    $userLog->user_id = $user->id;
                    $loginTime = $user->created_at;
                    $userLog->logged_in = $loginTime->setTimezone(new \DateTimeZone('Australia/Sydney'));
                    $userLog->logged_out = new \DateTime('now',new \DateTimeZone('Australia/Sydney'));
                }
                //}else{
                if ($userLog->save()) {
                      return true;
                } else {
                      throw new \Exception("Unable to update Access Log");
                }
            } else {
                throw new \Exception("Unable to update user token validity");
            }
        } else {
            throw new \App\Exceptions\UserNotFoundException();
        }
    }
    
    /**
     * function to match user to the page currently on
     * @param type $id
     * @param type $token
     * @return boolean
     * @throws \App\Services\Exception
     * @throws \Exception
     */
    public function pageOn($id, $token)
    {
              
        try
        {
            $userPage = User::where('token', $token)->first();
            if ($userPage instanceof User){
                if ($userPage->pages()->sync([$id])){
                     return true;
                } else {
                    throw new \Exception("Unable to save the page id to the corresponding user");
                }
            } else {
            throw new \Exception("User doesn't exist");
        }
        } catch (Exception $ex) {
            throw $ex;
        }      
    }  
    
    public function findUser($token)
    {
        $user  =  \App\Models\User::where('token', $token)->first();
        if ($user instanceof User) {
            return $user;
        } else {
            throw new \App\Exceptions\UserNotFoundException();
        }
        
    }
    
    private function chkPrivilege($username)
    {
       
        $level = DB::select("SELECT * FROM privilege WHERE username LIKE '$username'");
//        print_r($level);
//        exit;
        if ($level[0]->role != 'SA') {
            throw new \Exception('User does not have sufficient privileges');
        }
        return true;
    }
}
