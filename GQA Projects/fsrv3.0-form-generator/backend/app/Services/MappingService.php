<?php
/**
 * Created by PhpStorm.
 * User: annie.cherian
 * Date: 1/09/2016
 * Time: 3:14 PM
 */
namespace App\Services;

use App\Models\Mapping;
use DB;
use App\Exceptions;

/**
 * Function to find all CRM mappings
 * return \Symfony\Component\HttpFoundation\Response
 */
class MappingService
{

    public function findAllMapped()
    {
        $map = Mapping::all();
        $count = $map->count();
        if ($count ==0) {
            throw new \MappingsNotFoundException();
        }
        return array('count'=>$count , 'data'=>$map);
    }
    
    public function findMappedByType($type)
    {   
        $map2 = Mapping::where('type','like',$type)->get();
        
        $count = $map2->count();
        if ($count ==0) {
            throw new \MappingsNotFoundException();
        }
        return array('count'=>$count , 'data'=>$map2);
    }
    
    public function mapTo ($mapOpts)
    {
        if (!$mapOpts) { // default for custom mapping
            $mappedToId = 1;
        }else {
            if (is_array($mapOpts)) {
                if (isset($mapOpts['id'])) {
                    $mappedToId = $mapOpts['id'];
                    if (empty($mapOpts['id'])) {
                        $mappedToId = 1;
                    }
                    /* 
                     * @todo for automatic mapping inserts / updates
                    $mapQB = Mapping::where('id','=',$mappedToId)
                    ->update($this->createMappingQry($mapOpts));
                     * 
                     */
                }else { // no id so we create
                    /* 
                     * @todo for automatic mapping inserts / updates
                    $mapQB = new Mapping([$this->createMappingQry($mapOpts)]);
                    $mappedToId = $mapQB->save()->id;
                     * 
                     */
                    $mappedToId = 105;
                    // throw new \App\Exceptions\MappingsNotFoundException;
                }
            }else { // just a plain ID of mapped to
                $mappedToId = $mapOpts;
            }
        }
        
        return $mappedToId;
    }
    
    public function mappingExists ($mappingID)
    {
        try {
            $map = Mapping::where('id', '=', $mappingID)->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
              throw new \App\Exceptions\MappingsNotFoundException;
        }
    }
    
    private function createMappingQry ($data)
    {
        $opts = array();
        $vars = array ('attribute', 'active', 'xml_attribute');
        foreach ($vars as $var)
        if (array_key_exists ($var, $data)) {
            $opts[$var] = $data[$var];
        }
        return $opts;
    }
}
