<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //put your code here
    
return [

    'NotFoundForFSR' => "Unable to load pages for the given FSR",
    'NotFound' => "No page found for the given ID",
    'NameNotUnique' => "Unable to save page. Name already exists",
    'PageError'  => "Unable to save page"

];

