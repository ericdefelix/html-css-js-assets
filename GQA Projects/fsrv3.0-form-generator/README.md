# Free Skills Review v3

Codebase consists of both FrontEnd (HTML5,CSS,JS) and Backend (PHP5) code. The system follows RESTful architecture

## Usage

Commit all your HTML/JS/CSS code to FronEnd folder and all your REST APIs to backend folder.

## Recommendation for Virtual Host local development

For FrontEnd:
```
<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host2.example.com
    DocumentRoot "C:/xampp/htdocs/fsrv3.0/frontend"
    ServerName frontend.fsr.local
    ErrorLog "logs/dummy-host2.example.com-error.log"
    CustomLog "logs/dummy-host2.example.com-access.log" common
</VirtualHost>
```
For Backend:

```
<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host2.example.com
    DocumentRoot "C:/xampp/htdocs/fsrv3.0/backend/public"
    ServerName backend.fsr.local
    ErrorLog "logs/dummy-host2.example.com-error.log"
    CustomLog "logs/dummy-host2.example.com-access.log" common
</VirtualHost>
```