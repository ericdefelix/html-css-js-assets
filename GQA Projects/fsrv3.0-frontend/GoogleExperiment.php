<?php

function httpGet($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//	curl_setopt($ch,CURLOPT_HEADER, false); 
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function httpPost($url, $params)
{
    $postData = '';
    //create name value pairs seperated by &
    foreach ($params as $k => $v) {
        $postData .= $k . '=' . $v . '&';
    }
    $postData = rtrim($postData, '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'] . '/';
    return $protocol . $domainName;
}
if (isset($_GET["url"])) {
    $settingsJson = httpGet(siteURL() . 'settings.json');
    $settingsArray = json_decode($settingsJson, true);
    $settings = $settingsArray[$settingsArray["environment"]];
    if (isset($settings['apiUrl'])) {
        $fsrJson = httpGet($settings['apiUrl'] . '/front/fsr?url=' . $_GET["url"]);
        $fsrArray = json_decode($fsrJson, true);

        $gtm_value = $fsrArray["data"]["gtm_tag"];
        echo <<< EOT
            <!-- Google Analytics Content Experiment code -->
            <script>function utmx_section(){}function utmx(){}(function(){var
            k='$gtm_value',d=document,l=d.location,c=d.cookie;
            if(l.search.indexOf('utm_expid='+k)>0)return;
            function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
            indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
            length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
            '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
            '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
            '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
            valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
            '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
            </script><script>utmx('url','A/B');</script>
            <!-- End of Google Analytics Content Experiment code -->
EOT;
    }
}
