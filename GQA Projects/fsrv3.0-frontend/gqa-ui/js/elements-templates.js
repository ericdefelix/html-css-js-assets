// elements
var PAGE_ELEMENTS  = {
    init : function (fsrObj){
        // console.log(fsrObj)

        var i;

        this.fsr = fsrObj;
        this.mainContainer = $('main');

        var pages = this.fsr.pages;
        var hasThankYouPage = 0, $body = $('body');

        // Check if the FSR has a thank you page at the end 
        for (var i = 0, len = fsrObj.pages.length; i < len; i++) {
            var questions = fsrObj.pages[i].questions;

            for(var j = 0, len2 = questions.length; j < len2; j++) {
                // matchFound = questions[j].attribute == "Text Content";

                if(questions[j].attribute == "Text Content"){
                    hasThankYouPage = 1;
                    $body.addClass('has-thankyou-page');
                    break;
                }
                else {
                    hasThankYouPage = 0;
                    break;
                }
            }   
        }

        var count = Object.keys(pages).length - hasThankYouPage;
        this.answers = {};
        var parent = this;
        var cnt = 1;

        var btn_text, is_lastpage_flag = false;

        pages.forEach(function(page, page_index){
            
            PAGE_ELEMENTS.pageMarkup(page.id);
            PAGE_ELEMENTS.elementValidation[page.id] = {};
            buttonFlag = false;
            
            // if (fsrObj.pages[fsrObj.pages.length - 1].length === 1) {
            //     console.log('true')
            // }

            PAGE_ELEMENTS.pageScrollMarkup(page.id);
            page.questions && page.questions.forEach(function(questionObj,question_index){
                PAGE_ELEMENTS.elementValidation[page.id][questionObj.id] = {type:questionObj.type,validation:questionObj.validation};
                qid = questionObj.id;
                qtype = questionObj.type;

                if(qtype != 'content'){
                    buttonFlag = true;
                }

                parent.answers.qid = [];

                // Grab the placeholder if Text Content mapping is present but has no sibling elements that have mappings
                if (fsrObj.pages[page_index].questions.length === 1 && qtype === 'content') {
                    buttonFlag = true;
                    btn_text = fsrObj.pages[page_index].questions[question_index].placeholder;
                }

                // Determine if the evaluated page is last
                if (page_index === count) { is_lastpage_flag = true; }
                else { is_lastpage_flag = false; }

                PAGE_ELEMENTS.questionMarkup(page.id, questionObj, is_lastpage_flag);
            });

            if (cnt < count && buttonFlag) {
                if (cnt === 1) { PAGE_ELEMENTS.appendButton(page.id, btn_text); }
                else { PAGE_ELEMENTS.appendButton(page.id, 'Next'); }
            }
            if (cnt === count && buttonFlag) { PAGE_ELEMENTS.submitButton(page.id); }

            cnt++;  
        });

        validateInput.store_strings();
    },
    elementValidation:{},
    pageScrollMarkup: function(pageId) {
        $('<div/>', {'class':'fp-custom-scroll'}).appendTo('#sec-'+pageId);
    },
    pageMarkup :function (pageId){
        $('<section/>', {
            'class':'section list-screen-item',
            'id' : 'sec-'+pageId
        }).appendTo(PAGE_ELEMENTS.mainContainer);
    },
    
    questionMarkup : function (pageId, questionObj, is_lastpage_flag){
        var temphtml = '';
        var buttonhtml ='';
        var i = 0;

        var get_target_type = function() {
            if (isMobile.phone == true) { return 'sidebar'; }
            else { return 'dropdown' }
        }

        var get_if_dropdown = function() {
            if (isMobile.phone == true) { return ''; }
            else { return 'dropdown' }
        }

        var render_if_dropdown = function() {
            if (isMobile.phone == true) { return ''; }
            else { return '<div class="dropdown-menu"><ul class="list-select"></ul></div>' }
            // <input type="text" class="typeahead form-control typeahead-form-control" data-provide="typeahead" autofocus autocomplete="off"/>
        }

        function removeSpaces(str) {
            str = str.replace(/\s+/g, '');
            return str;
        }
        
        temphtml += questionObj.hasOwnProperty('title') ? 
            '<h1 class="dyn-text">'+questionObj.title+'</h1>' : '';
        temphtml += questionObj.hasOwnProperty('sub_title') ? 
            '<h2 class="dyn-text">'+questionObj.sub_title+'</h2>' : '';
        temphtml += questionObj.hasOwnProperty('content') ? 
            '<h3 class="dyn-text">'+questionObj.content+'</h3>' : '';
        // temphtml += questionObj['attribute'] === 'Text Content' ? 
        temphtml += (questionObj['attribute'] === 'Text Content' && is_lastpage_flag === true ) ? 
            '<i class="countdown-timer">Redirecting in <strong id="counterText">1</strong> second(s)</i>' : '';


        switch (questionObj.type) {
            case 'text' :
            case 'email' :
            case 'contact' :

                temphtml += '<div class="form-group">';
            
                //checks if a validation is there for this particular field, if yes adds a class called class-required to the input element
                if(questionObj.validation["required"] == true){

                    if (questionObj.validation['undefined field'] === true) {
                        temphtml += '<textarea class="form-control class-required" row="3" cols="30" data-element="textType" placeholder="'+questionObj.placeholder+'" id="'+removeSpaces(questionObj.xml_equivalent)+'"></textarea>'
                    }
                    else {
                        temphtml += '<input type="text" class="form-control class-required" data-element="textType" placeholder="'+questionObj.placeholder+'" id="'+removeSpaces(questionObj.xml_equivalent)+'">'
                    }
                }
                else {
                    temphtml += '<input type="text" class="form-control " data-element="textType" placeholder="'+questionObj.placeholder+'" id="'+removeSpaces(questionObj.xml_equivalent)+'">'
                }
           
                temphtml += '<label class="validation_label"></label>'   
                         +'</div>';
                var answerVal = $('#answer_'+questionObj.id).val();

                $('<div/>', {
                'class':'elements',
                'id':'element-'+questionObj.id,
                'html': temphtml
                 }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));
            break;
            
            case 'select' :
                temphtml += '<div class="form-group '+get_if_dropdown()+'"><input type="hidden" class="form-control class-required" placeholder="'+questionObj.placeholder+'" id="select-'+questionObj.id+'">'
                            +'<button type="button" class="form-control btn-lg btn-block btn-chevron btn-select" data-toggle="'+get_target_type()+'" id="question-'+questionObj.id+'">'
                            +buttonhtml+' <span class="selectDisplayText">'+questionObj.placeholder+'</span><i class="chevron bottom"></i>'
                            +'</button>'+render_if_dropdown()
                            +'<label class="validation_label"></label></div';
                $('<div/>', {
                'class':'elements',
                'id':'element-'+questionObj.id,
                'html': temphtml
                }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));

                if(questionObj.hasOwnProperty('answers')) { //Checks if the question has answers, in that case function is called to read the title for each answer
                    questionObj.answers.forEach(function(answer){

                        var required = questionObj.validation['required'];
                        PAGE_ELEMENTS.answerMarkupSelect(questionObj.id,answer,required);
                    });
                }
                else if (questionObj.attribute === 'Post Code'){
                    console.log('has post code')
                    // $.get('postcodes.json', function(data){
                    //     $(".typeahead").typeahead({ source:data });
                    // },'json');
                }
            break;
            
            case 'checkbox' :
                if(questionObj.hasOwnProperty('answers')) {
                     
                    temphtml += '<div class="form-group">';
              
                    temphtml += '<div class="btn-group btn-group-stack" data-toggle="buttons" id="question-'+questionObj.id+'">'
                             +'</div>';
                      

                    temphtml += '<label class="validation_label"></label></div>';
                   
                    $('<div/>', {
                        'class':'elements',
                        'id':'element-'+questionObj.id,
                        'html': temphtml
                    }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));

                    questionObj.answers.forEach(function(answer){
                        var required = questionObj.validation['required'];
                        var htmlEle =  PAGE_ELEMENTS.answerMarkupChkBox(answer,required);
                        htmlEle.appendTo($('#question-'+questionObj.id));
                    });
               }
            break;
            
            case 'radio' :
                if(questionObj.hasOwnProperty('answers')) {
                     
                    temphtml += '<div class="form-group">';
                
                    temphtml += '<div class="btn-group btn-group-stack radio-group-fsr" data-toggle="buttons" id="question-'+questionObj.id+'">'
                                +'</div>'
                                +'<label class="validation_label"></label></div>';

                    $('<div/>', {
                        'class':'elements',
                        'id':'element-'+questionObj.id,
                        'html': temphtml
                    }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));

                    questionObj.answers.forEach(function(answer){
                        var required = questionObj.validation['required'];
                        var htmlEle =  PAGE_ELEMENTS.answerMarkupRadio(answer,required);
                        htmlEle.appendTo($('#question-'+questionObj.id));
                    });
               }
            break;
            
            case 'content':
                $('<div/>', {
                'class':'elements text-content',
                'id':'element-'+questionObj.id,
                'html': temphtml
                 }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));
            break;

            case 'upload' :
                var accepted_files = 'application/pdf,application/vnd.ms-word,image/x-png, image/jpeg, image/jpg';

                temphtml += '<div class="form-group"><div class="upload-control">';
                if(questionObj.validation["required"] == true){ //checks if a validation is there for this particular field, if yes adds a class called class-required to the input element
                    temphtml += '<input type="file" class="form-control class-required" data-value="" data-element= "uploadType" accept="'+accepted_files+'" placeholder="'+questionObj.placeholder+'" id="'+questionObj.xml_equivalent+questionObj.id+'">'
                } 
                else {
                    temphtml += '<input type="file" class="form-control" data-target="btnupload" data-value="" data-element= "uploadType" accept="application/pdf,application/vnd.ms-word" placeholder="'+questionObj.placeholder+'" id="'+questionObj.xml_equivalent+questionObj.id+'">'
                }
                temphtml += '<label for="'+questionObj.xml_equivalent+questionObj.id+'" class="btn form-control btn-lg btn-block btn-upload"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> '+questionObj.placeholder+'</label>'
                         +'<button data-target="btnnothanks" type="button" class="btn btn-block btn-lg btn-transparent nothanks">No, thank you</button><label class="validation_label"></label></div>';

                var answerVal = $('#answer_'+questionObj.id).val();

                //temphtml += '<button type="button" class="btn btn-primary btn-lg btnevent" data-target="btnupload">'+buttonhtml+'</button>'; 
     
                $('<div/>', {
                'class':'elements',
                'id':'element-'+questionObj.id,
                'html': temphtml
                 }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));
            break;

            default:
                temphtml += '<div class="form-group">';

                //checks if a validation is there for this particular field, if yes adds a class called class-required to the input element
                if(questionObj.validation["required"] == true) {
                    temphtml += '<input type="text" class="form-control class-required"  data-element= "textType" placeholder="'+questionObj.placeholder+'" id="'+questionObj.xml_equivalent+'">'
                } 
                else {
                    temphtml += '<input type="text" class="form-control  "  data-element= "textType" placeholder="'+questionObj.placeholder+'" id="'+questionObj.xml_equivalent+'">'
                }

                temphtml += '<label class="validation_label"></label>'
                    +'</div>';
                var answerVal = $('#answer_'+questionObj.id).val();

                $('<div/>', {
                    'class':'elements',
                    'id':'element-'+questionObj.id,
                    'html': temphtml
                }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));
            break;
            
            case 'opt-in':
                
                temphtml += '<div class="form-group">';
                temphtml += '<div class="optstack custom-checkbox" id="question-'+questionObj.id+'">'
                         + '</div>';

                $('<div/>', {
                'class':'elements',
                'id':'element-'+questionObj.id,
                'html': temphtml
                }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));
                    if(questionObj.hasOwnProperty('answers')) { //Checks if the question has answers, in that case function is called to read the title for each answer
                        questionObj.answers.forEach(function(answer){
                            var htmlEle = PAGE_ELEMENTS.answerMarkupoptIn(answer);
                            htmlEle.appendTo($('#question-'+questionObj.id));
                        });
                    }
            break;
        }
    },
    answerMarkupoptIn : function (answer) {
        var answerhtml2 = '';

        answerhtml2 += '<input type="checkbox" checked class="optbox" name="options" value="Yes" id="opt'+answer.id+'"/>';
        answerhtml2 += '<label for="opt'+answer.id+'" class="btn-opt" data-element="btnoptin">'+answer.title+'</label>';

        return ($('<div/>', { 'class':'answers', 'html':answerhtml2 }));
    },
    answerMarkupRadio : function ( answer,req) {
        
        var answerhtml = '';
        answerhtml += '<label for='+answer.title+' class="btn btn-default btn-lg" data-element="btnradio">';
        if( req == true){
            answerhtml += '<input type="radio" class="form-control class-required" id="'+answer.title+'" name="'+answer.title+'" value="'+answer.title+'"/>';
        }
        else {
            answerhtml += '<input type="radio" class="form-control" id="'+answer.title+'" name="'+answer.title+'" value="'+answer.title+'"/>';
        }

        answerhtml += ' <span class="option-label-fsr">'+answer.title+'</span>'
                      + '</label>';

        return ($('<div/>', {
            'class':'answers',
            'html':answerhtml
        }));   
    },
    answerMarkupChkBox : function (answer,req) {
        var answers2 = [];
        var answerhtml2 = '';
        answerhtml2 += '<label for='+answer.title+' class="btn btn-default btn-lg" data-element="btnchkbox">';
        if( req == true){
            answerhtml2 += '<input type="checkbox" class="options form-control class-required" name="options" value="'+answer.title+'" id="'+answer.title+'"/>';
        } 
        else {
            answerhtml2 += '<input type="checkbox" class="options form-control" name="options" value="'+answer.title+'" id="'+answer.title+'"/>';
        }

        answerhtml2 += ' <span class="option-label-fsr">'+answer.title+'</span>'
                    + '</label>';
        
        return ($('<div/>', {
            'class':'answers',
            'html':answerhtml2
        }))
    },
    answerMarkupSelect : function (questionId,answer,req){
       var sidebarhtml = '';
       var dropdownhtml = '';

        if (req == true) {
            sidebarhtml += '<input class="list-value form-control class-required" type="hidden" value="'+answer.title+'">'
        }
        else {
            sidebarhtml += '<input class="list-value form-control" type="hidden" value="'+answer.title+'">'
        }
        
        sidebarhtml += '<h5 class="sidebar-list-heading">'
                    + '<span class="list-span" value="'+answer.title+'">'+answer.title+'</span></h5>';
                  
        $('<li/>',{
            'class':'listItem',
            'data-target': 'list-'+questionId,
            'html' : sidebarhtml
        }).appendTo('#sidebarSelect'); //Appends answers to sidebar overlay for select elements

        // Replicates sidebar answers for desktop
        if (isMobile.phone === false) {
            $('<li/>',{
                'class':'listItem',
                'data-target': 'list-'+questionId,
                'html' : sidebarhtml
            }).appendTo('.dropdown-menu .list-select'); //Appends answers to sidebar overlay for select elements
        }
    },
    appendPageName: function () {
        var pages = PAGE_ELEMENTS.fsr.pages;
        pages.forEach(function (page, page_index) {
            $('#fp-nav ul li').eq(page_index).attr('data-after', page.unique_name);
        });
    },
    appendButton : function (pageId, buttontext) {
        var buttonhtml = '';

        buttontext === undefined ? buttontext = 'Next' : buttontext = buttontext;

        buttonhtml += '<button type="button" class="btn btn-primary btn-lg btnevent" data-target="btnevent_new">'+ buttontext +'</button>';
        var numberOfRadioSingle = $('#sec-' + pageId + ' .fp-custom-scroll').find('.radio-group-fsr').length;
        var numberOfElements = $('#sec-' + pageId + ' .fp-custom-scroll').find('.elements').length;
        $('<div/>', {
            'class': 'btns',
            'html': buttonhtml
        }).appendTo($('#sec-' + pageId + ' .fp-custom-scroll'));
        if (numberOfRadioSingle === 1 && numberOfElements === 1) {
            $('#sec-' + pageId + ' .fp-custom-scroll').find('.btnevent').hide();
        }
    },
    submitButton : function (pageId) {
        
        var buttonhtml = '';
       
        buttonhtml += '<button type="button" class="btn btn-primary btn-lg btnevent" data-target="btnevent_submit">Submit</button>';
        $('<div/>', {
            'class':'btns',
            'html': buttonhtml
             }).appendTo($('#sec-'+pageId+' .fp-custom-scroll'));
    }
}

