/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var validateInput = {
    doc: $(document),
    lastPage:false,
    active_btn: {},
    var_strings: [],
    bind : function(){
        this.doc.on('click', '[data-target="btnevent_new"]', function () {
            validateInput.validate2($(this));
        });
        this.doc.on('click', '[data-target="btnevent_submit"]', function () {
            validateInput.lastPage = true;
            validateInput.validate2($(this));
        });
        this.doc.on('keydown', '[data-element= "textType"]', function (event) {
            validateInput.check(event, $(this));
        });
        this.doc.on('click', '.listItem', function () {
            validateInput.select($(this));
        });
        this.doc.on('click', '[data-element="chkbox"]', function (event) {
            event.preventDefault();
            validateInput.validatechkbox($(this));
        });
        this.doc.on('change', '[data-element="uploadType"]', function (event) {
            validateInput.upload($(this));
        });
        this.doc.on('click', '[data-target="btnupload"]', function (event) {
            validateInput.validateupload($(this));
        });
        this.doc.on('click', '[data-target="btnnothanks"]', function (event) {
            var parent = $(this).closest('.fp-tableCell');

            validateInput.removeUploadValidate($(this));
            parent.find('.btnevent').click();
        });
        this.doc.on('click', '[data-toggle="sidebar"]', function (event) {
            validateInput.validateSelectList($(this),'mobile');
        });

        this.doc.on('click', '[data-toggle="dropdown"]', function (event) {
            validateInput.validateSelectList($(this),'non-mobile');
        });

        // If tile select single, automatically go to next screen
        this.doc.on('click', '[data-element="btnradio"]', function (event) {
            var parent = $(this).closest('.fp-tableCell');
            if (parent.find('.elements').length === 1 || parent.find('.text-content').length === 1) {
                var t;
                timeout = setTimeout(function(){ clearTimeout(t); parent.find('.btnevent').click(); },100);
            }
            console.log(parent.find('.elements').length);
        });
        this.doc.on('keydown', '.btnevent', function (e) {
            var code = e.keyCode || e.which;
            if (code == '9') {
                if(!e.shiftKey)
                    e.preventDefault();
            }
            if (code == '32' || code == '13') {
                $(this).trigger('click');
            }
        });
        this.doc.on('keydown', 'body', function (e) {
            var code = e.keyCode || e.which;
            if (code == '9') {
                var cnt = 0;
                
                $('.btnevent:in-viewport').each(function(){
                    if($(this).offset().top !== 0 && $(this).offset().left !== 0)
                        cnt++;
                });
                if(e.shiftKey && $(document.activeElement).is('div')){
                    e.preventDefault();
                }
                if (cnt ===0) {
                    e.preventDefault();
                }
                    
            }
        });

        // capture first name and use it to replace instances of %%first_name%% variables
        var fname;
        this.doc.on('blur', '#FirstName', function(event) {
            function capitalizeFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            fname = capitalizeFirstLetter($(this).val());
            $(this).val(fname);

            validateInput.check_strings(fname);
        });

        // handle Tab keydown
        var i = 0;
//        this.doc.on('keydown', 'input[type="text"]', function(event) {
//            if (event.keyCode === 9) {  //tab pressed
//               console.log('sam',$(':focus'))
////                event.preventDefault(); // stops its action
////
//                if (event.shiftKey) { 
//                    i = i - 1;
//                    if (i < 0) { i = FULL_PAGE.active_inputs.length - 1; }
//                }
//                else {
//                    i = i + 1;
//                    if (i === FULL_PAGE.active_inputs.length) { i = 0; }
//                }
//
//                FULL_PAGE.active_inputs[i].focus();
//            }
//        });
    },

    isValidate: function(question_element,value,validation){
        var err = [];
        var field_name = question_element.children('h1').text();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var contactReg = /^(((\+614){1}[0-9]{8})|([0]{1}[0-9]{9}))$/g;
        if(typeof value == 'undefined') return false;

        $.each( validation, function( key, valid ){
            key = $.trim(key).toLowerCase();
            switch(key) {
                case "required":
                    valid && (value == '' || value.length == 0) && err.push("Please provide this information");
                    break;
                case "email":
                    valid && (!emailReg.test(value)) && err.push("Please provide a valid email address: e.g. myEmail@email.com");
                    break;
                case "contact":
                    valid && (!contactReg.test(value)) && err.push("Please provide a valid ten-digit contact number");
                    break;
            }

            if(err.length == 1) return false;
        });

        var label = question_element.find('.validation_label');

        if(err.length > 0 ){       
            var validation_text = err.join('<br />');
            label.html(validation_text);
            label.addClass('errormsg');
            validateInput.shake_btn();

            return false;
        }
        else {
            label.removeClass('errormsg');
            label.text('');
            return true;
        }   
    },
    validate2 :function(element){
        var elem =[];
        var vals =[];
        var page_element = element.parents('section.section');
        var page_id = (page_element.attr('id') || 'sec-0').split("-")[1];
        if(PAGE_ELEMENTS.elementValidation[page_id]){
            var question_elements = PAGE_ELEMENTS.elementValidation[page_id];
            $.each( question_elements, function( key, question ){
                var question_element = page_element.find('#element-'+key);
                var question_type = question.type;
                var validation = question.validation;
                var form_element = question_element.find('.form-control');
                var value = '';
                switch(question_type){
                    case "text":
                    case "email":
                    case "contact":
                        value = form_element.val();
                        validateInput.isValidate(question_element,value.trim(),validation) && elem.push(question_element) && vals.push(value);
                    break;
                    case "select":
                        value = question_element.find('.form-control[type="hidden"]').val() || '';
                        validateInput.isValidate(question_element,value.trim(),validation) && elem.push(question_element) && vals.push(value);
                    break;
                    case "upload":
                        value = form_element.attr('data-value').trim()
                        validateInput.isValidate(question_element,value.trim(),validation) && elem.push(question_element) && vals.push(value);
                    break;
                    case "radio":
                        value = question_element.find('.form-control[type="radio"]:checked').val() || '';
                        validateInput.isValidate(question_element,value.trim(),validation) && elem.push(question_element) && vals.push(value);

                    break;
                    case "checkbox":
                        var chkboxvalues = question_element.find('.form-control[type="checkbox"]:checked').map(function(){
                            return this.value;
                        }).toArray();
                        value = chkboxvalues || '';
                        validateInput.isValidate(question_element,value,validation) && elem.push(question_element) && vals.push(value);
                    break;
                    case "opt-in":
                        value = question_element.find('.optbox[type="checkbox"]:checked').val() || 'No';
                        validateInput.isValidate(question_element,value.trim(),validation) && elem.push(question_element) && vals.push(value);
                    break;
                    case "content":
                        elem.push(question_element) && vals.push('')
                    break;
                    default:
                        value = form_element.val();
                        validateInput.isValidate(question_element,value,validation) && elem.push(question_element) && vals.push(value);
                    break;
                }
                
              });
              (element.parent('.btns').siblings('.elements').length == elem.length) && validateInput.move_nextlevel(elem,vals);
        }else
            validateInput.move_nextlevel(elem,vals);
              
    },
    check :function(event,obj) {     
       var keycode = event.keyCode || event.which;
        if(keycode == '13') {
              var value = obj.val();
           
            if((value.trim() == '') && (input.hasClass('class-required'))) {
                var label = obj.next('label');
                
                if(label.hasClass('errormsg')){
                    label.removeClass('errormsg');
                } else {
                label.addClass('errormsg');
                validateInput.shake_btn();
                $('.errormsg').text("Field cannot be empty!");
                }
            input.focus();
            } 
        } else {
            var label = obj.next('label');
            if(label.hasClass('errormsg')){
                    label.removeClass('errormsg');
                    label.text('');
                    
            }
        }
    },
    check_if_multiple_questions: function() {
        var active_screen_elems = $('.fp-section.active .elements').length,
            has_text_content = $('.fp-section.active .text-content').length;

        if (active_screen_elems > 1 || has_text_content >= 1) { return false;  }
        else if (active_screen_elems === 1 && has_text_content >= 1 ) { return false; }
        else { return true }
    },
    select: function(element){
        var obj= element.find('.list-value');
        var target =(element.attr('data-target') || 'list-0').split("-")[1];
        var value = obj.val();
        var btn =  $('#select-'+target).closest('.form-group').find('.selectDisplayText');
        $('#select-'+target).val(value);
        $(btn).html(value);

        if (isMobile.phone === true) {
            TOGGLE_MENU.hide();
        }

        if (validateInput.check_if_multiple_questions() === false) {
            $('.fp-section.active').find('.btnevent').click();
        }

        console.log(validateInput.check_if_multiple_questions());
    },
    
    validateradio : function(element,event) {
       var type = 'radio';
       var value = element.children('input').val();
       event.preventDefault();
       this.move_next(element,value,type);
        
    },
    validatechkbox : function(element){
       
        var type = "checkbox";
        var chkboxvalues = $('input:checkbox:checked').map(function(){
            return this.value;
        }).toArray();
        var label = element.next('label');
        if(chkboxvalues.length == 0){
            if(label.hasClass('errormsg')){
                label.removeClass('errormsg');
                label.txt('');    
            }
            else {
                label.addClass('errormsg');
                validateInput.shake_btn();
                $('.errormsg').text("Please make atleast one selection to proceed");
            }
        }
        else {
            if(label.hasClass('errormsg')){
                label.removeClass('errormsg');
            }
            this.move_next(element,chkboxvalues,type);
        }
    },
    upload :function(element){
          if (!(window.File && window.FileReader && window.FileList && window.Blob && window.Blob.prototype.slice)) {
            alert("Sorry! You are using an older or unsupported browser. Please update your browser");
            return;
        }
        var file = element[0].files[0];
        if (!this.validateFileType(file)) {
            return;
        }
        element.after( '<p class="progress">0%</p>' );
        var s3uploads = new S3MultiUpload(file, {user: 'user', pass: 'pass',fileNum: 1,userDirectory:'fsrV3'});

            s3uploads.onServerError = function(command, jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 403) {
                    alert("Sorry you are not allowed to upload");
                } else {
                    console.log("Our server is not responding correctly");
                }
            };

            s3uploads.onS3UploadError = function(xhr) {
                s3uploads.waitRetry();
                alert("Retrying in " + s3uploads.RETRY_WAIT_SEC + " seconds");
            };

            s3uploads.onProgressChanged = function(uploadingSize, uploadedSize, totalSize, fileNum) {
                var percentage = Math.round(uploadingSize + uploadedSize);
                element.next('.progress').html(Math.round(uploadedSize) + '%');
            };

            s3uploads.onUploadCompleted = function(data,obj) {
                element.next('.progress').remove();
                element.attr('data-value',data.fileName);
                //validateInput.move_next(element,element.data('value'),'upload');
            };

            s3uploads.start();   
    },
    validateFileType : function (fileObj) {
        var _validFileExtensions = ['.jpg', '.jpeg', '.pdf', '.doc', '.docx', '.bmp', '.png'];
        var sFileName = fileObj.name;        
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return (false);
                }
            }
            return true;
    },
    validateupload: function (button) {
        var element = button.siblings('.form-group').children('input');
        if ((element.attr('data-value').trim() == '') && (element.hasClass('class-required'))) {
            if (element.next('.progress').length == 1) {
                alert("Uploading is still in progress");
            } else {
                var label = element.siblings('label');
                label.text("Field cannot be empty!");
                if (label.hasClass('errormsg')) {
                    label.removeClass('errormsg');
                } else {
                    label.addClass('errormsg');
                    validateInput.shake_btn();
                }
            }

        } else {
                element.siblings('label').removeClass('errormsg');
                this.move_next(element, element.data('value'), 'upload');
        }


    },
    removeUploadValidate: function(button) {
        var question_element = button.parents('.elements');
        var page_element = button.parents('section.section');
        var q_id = (question_element.attr('id') || 'element-0').split('-')[1];
        var page_id = (page_element.attr('id') || 'element-0').split('-')[1];
        var validation = PAGE_ELEMENTS.elementValidation[page_id][q_id]['validation'];
        PAGE_ELEMENTS.elementValidation[page_id][q_id]['validation']['required'] = false;
        button.addClass('nothanks-activate').siblings('.upload-control').addClass('upload-control-inactive');
        
    },
    validateSelectList: function(button,device){
        var question_id = (button.parents('.elements').attr('id') || 'element-0').split('-')[1];
        var select = device === 'mobile' ? '#sidebarSelect' : '.list-select';
        var options = $(select+' li');
        var numoptions= options.length;
        var i = 0;
        options.hide(0,function(){
            i++;
            (i == numoptions) && $(select+' li[data-target="list-'+question_id+'"]').show();
        });
    },
    move_next: function(element,value,type) {
        validateInput.busy_button('active');

        if(type == 'select'){
            var question_id = (element.attr('data-target') || 'list-0').split("-")[1];
            TOGGLE_MENU.hide();
        } 
        else {
            var question_id = (element.parents('.elements').attr('id') || 'element-0').split("-")[1];
        }

        var answer = value;

        if(FULL_PAGE.preview == true) {
            validateInput.busy_button('inactive');
            $.fn.fullpage.moveSectionDown();
            validateInput.trigger_thankyoupage();
        }
        else {
            FULL_PAGE.save_answers(question_id,answer).then(function(res){
                if(res !== false && res.response == 200){
                    validateInput.busy_button('inactive');
                    $.fn.fullpage.moveSectionDown();
                    validateInput.trigger_thankyoupage();
                }
                else{
                    validateInput.busy_button('inactive');
                    validateInput.process_notification('error');
                    console.log("Data not saved");
                }
            });
        }
    },
    move_nextlevel : function(element,value) {
        validateInput.busy_button('active');

        if(FULL_PAGE.preview == true) {
            validateInput.busy_button('inactive');
            $.fn.fullpage.moveSectionDown();
            setWhatsDone();
        }
        else {
            $.each( element, function( key, val ){
                var question_id = (element[key].attr('id') || 'element-0').split("-")[1];
                var answer = value[key];
                FULL_PAGE.save_answers(question_id,answer);
            });

            FULL_PAGE.save_fsr().then(function(res){
                validateInput.busy_button('inactive');

                if(res !=false && res.response == 200) {
                    $.fn.fullpage.moveSectionDown();
                    validateInput.trigger_thankyoupage();
                    setWhatsDone();
                } 
                else {
                    validateInput.process_notification('error');
                    console.log("Data not saved");
                }
            }); 
        }

        function setWhatsDone() {
            var nav = $('#fp-nav ul li');
            nav.eq(parseInt(FULL_PAGE.val_screen_index)).find('a').addClass('done');
        }
    },
    trigger_thankyoupage: function() {
        if (validateInput.lastPage) {
            validateInput.process_notification('success');
            validateInput.process_thankyou();
        }
    },
    process_notification: function(status) {
        var $notif = $('#notificationMessage'), message = '#notificationSuccessMessage',
            v_show_options = { display: 'block', easing: [0, 0.96, 0.86, 0.99], duration: 600 },
            v_hide_options = { display: 'none', easing: [0, 0.96, 0.86, 0.99], duration: 2000, delay: 3000 } ;

        $notif.removeClass('success error');

        if (isMobile.phone === true) { 
            $notif.velocity({ opacity: 1, translateY: ['0','30px'] }, v_show_options )
                  .velocity({ opacity: 0, translateY: '30px' }, v_hide_options );
        }
        else { 
            $notif.velocity({ opacity: 1, translateX: ['0','30px'] }, v_show_options )
                  .velocity({ opacity: 0, translateX: '15px' }, v_hide_options );
        }

        if (status === 'success') { $notif.addClass('success'); $notif.find(message).text('Submitted successfully'); }
        else if (status === 'error') { $notif.addClass('error'); $notif.find(message).text('Something went wrong while submitting'); }
    },
    process_thankyou: function() {
        var duration = 10;
        validateInput.doc.find('#counterText').html(duration);

        $('#fp-nav').velocity({opacity: 0},{ duration: 300, delay: 1000 }).css({ 'z-index': '-1', 'pointer-events': 'none' });

        function countDown(i, callback) {
            callback = callback || function(){};
            var int = setInterval(function() {
                validateInput.doc.find('#counterText').html(i);
                i-- || (clearInterval(int), callback());
                console.log(i);
            }, 1000);
        }

        countDown(duration,function(i) {
            if (FULL_PAGE.preview === false) {
                window.location.href = "https://www.gqaustralia.edu.au/";
            }
        });
    },
    store_strings: function() {
        $('.dyn-text:visible').each(function(i, obj) {
            var elem = $(this);
            // Push original string
            // if (elem.prop('tagName') === 'H1' || elem.prop('tagName') === 'H2' || elem.prop('tagName') === 'H3') {
                validateInput.var_strings.push({ display: elem.text(),edited: '',prevname: undefined});
            // }
            // else if (elem.prop('tagName') === 'INPUT') {
                // validateInput.var_strings.push({display: elem.attr('placeholder'),edited: ''});
            // }
        });
    },
    check_strings: function(first_name) {
        $('.dyn-text:visible').each(function(i, obj) {
            var elem = $(this);
            var display = validateInput.var_strings[i].display;
            var edited = validateInput.var_strings[i].edited;

            str = display.replace('%%first_name%%','%%'+first_name+'%%');

            validateInput.var_strings[i].prevname = first_name;

            if (validateInput.var_strings[i].prevname === undefined) {
                edited = str.replace('%%'+first_name+'%%', first_name);
            }
            else {
                edited = str.replace('%%'+validateInput.var_strings[i].prevname+'%%', first_name);
            }
            
            // Push original string
            elem.text(edited);
            display = str;
        });
    },
    busy_button: function(status) {
        var txt, working = '<i class="zmdi zmdi-settings zmdi-hc-spin"></i> working...',
            $active_btn = $('.fp-section.active .btnevent');

        if (status === 'active') {
            validateInput.active_btn['text'] = $active_btn.text();
            validateInput.active_btn['button'] = $active_btn;
            $active_btn.html(working);
            $active_btn.prop('disabled',true);

        }
        else if (status === 'inactive') {
            validateInput.active_btn.button.prop('disabled',false);
            validateInput.active_btn.button.html(validateInput.active_btn.text);
        }
    },
    shake_btn: function() {
        $('.fp-section.active .btnevent').velocity('callout.shake');
    }
}
$(document).ready(function(){
    validateInput.bind();
});
