var FSR_GTM = {
    gtmId: 'GTM-TGSC38',
    init: function (gex_id) {
        // id&&(this.gtmId=id);
        // if (this.gtmId.trim() == '') {
        this.gtmId = this.gtmId || 'GTM-TGSC38';
        if (gex_id === undefined) {
            console.log('Google Experiment Id not Available');
            //return false;
            gex_id = '';
        }

        $('head').append('<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\': new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],' +
                'j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);' +
                '})(window,document,\'script\',\'dataLayer\',\'' + this.gtmId + '\');<\/script>');
        $('body').prepend('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=' + this.gtmId + '" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>');
               
    },
    sendPage: function (page) {
        var pageName = page;
        if (typeof page === 'number') {
            pageName = this.stringifyNumber(page);
        }
        pageName = pageName + 'page';
        var customEvent = document.createEvent('Event');
        customEvent.initEvent(pageName, true, true);
        console.log(customEvent);
        document.dispatchEvent(customEvent);
        dataLayer && dataLayer.push({
            'pageTitle': pageName,
            'event': pageName
        });
    },
    special: ['zeroth', 'first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelvth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth'],
    deca: ['twent', 'thirt', 'fourt', 'fift', 'sixt', 'sevent', 'eight', 'ninet'],
    stringifyNumber: function (n) {
        if (n < 20)
            return this.special[n];
        if (n % 10 === 0)
            return this.deca[Math.floor(n / 10) - 2] + 'ieth';
        return this.deca[Math.floor(n / 10) - 2] + 'y-' + this.special[n % 10];
    }
}