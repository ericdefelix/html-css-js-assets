// Fullpage .JS
var FULL_PAGE = {
    bind: function() {
    },
    fsr : {},
    GET: {},
    answers: [],
    preview:false,
    active_inputs: [],
    val_screen_index: 0,

    init: function(response) {
        // this.get_data();
        this.fsr = response.data;        
        this.fsr.sessionToken = response.token;
        PAGE_ELEMENTS.init(FULL_PAGE.fsr);

        FSR_GTM.init(FULL_PAGE.fsr.gtm_tag);
        
        $('#fsrFullPage').fullpage({
            lockAnchors :true,
            navigation: true,
            // css3: true,
            // easingcss3: 'cubic-bezier(.57,.77,.5,.94)',
            scrollingSpeed: 1000,
            fixedElements: '.header',
            afterLoad: function(anchorLink, index){
                FSR_GTM.sendPage(index);
                FULL_PAGE.val_screen_index = index - 1;
                FULL_PAGE.handle_tab();

                FULL_PAGE.gex = response.data.gtm_tag;
            },
            afterRender: function() {
                var body = $('body');
                if (isMobile.phone === true) {
                    body.addClass('is-mobile');
                }
                else {
                    body.addClass('is-desktop');
                }

                FULL_PAGE.declare_custom_scrollbar();
                FULL_PAGE.make_options_scroll();
                validateInput.busy_button();
                PAGE_ELEMENTS.appendPageName();
            }
        });
        $.fn.fullpage.setMouseWheelScrolling(false);
        $.fn.fullpage.setAllowScrolling(false);
        $.fn.fullpage.setKeyboardScrolling(false);
    },
    handle_tab: function() {
        FULL_PAGE.active_inputs = [];

        $('.fp-section.active input[type="text"]').each(function(index, val) {
            FULL_PAGE.active_inputs.push($(this));
        });
    },
    is_mobile: function() {
        var top;
        var currentTop = 0;
        var doc = $(document);
        var is_still_focused = false;

        if (isMobile.android.device === true) {
            doc.on('blur', '.fp-section.active [data-element="textType"]', function(event) {

                // if (is_still_focused === true) {
                    $('.fp-section.active .fp-custom-scroll').velocity({translateY: '0' },{
                        duration: 500, 
                        easing: [0, 0.96, 0.86, 0.99], 
                        complete: function() {
                            $('#fp-nav, .header').velocity({'opacity': 1},{duration: 100});
                        } 
                    });
                // }
                
                // FULL_PAGE.make_options_scroll();
            });

            doc.on('focus', '.fp-section.active [data-element="textType"]', function(event) {            
                $('#fp-nav, .header').velocity({'opacity': 0},{duration: 100});

                top = $(this).offset().top - 120;

                // is_still_focused = true;
                // is_still_focused = true;

                // if (is_still_focused === false) {
                    $('.fp-section.active .fp-custom-scroll').velocity({translateY: -top},{ 
                        duration: 500, 
                        easing: [0, 0.96, 0.86, 0.99],
                        complete: function() {
                            // is_still_focused = false;
                        }
                    });
                // }
            });
        }
    },
    declare_custom_scrollbar: function() {
        if (isMobile.phone === true || isMobile.tablet === true ) {
            return false;
        }
        else {
            
            // style the scrollbar
            $('.fp-custom-scroll').mCustomScrollbar({
                // advanced:{ updateOnContentResize: true },
                callbacks: {
                    scrollbarPosition: 'inside',
                    onUpdate: function() {
                        FULL_PAGE.make_options_scroll();
                    },
                    onScrollStart : function() {
                        $('.fp-section.active .fp-tableCell').removeClass('overflow-gradient');
                    }                 
                }
            });
        }
    },
    make_options_scroll: function() {

        var obj = $(document).find('.fp-section .fp-custom-scroll');

        obj.each(function(index, obj) {
            var elements = $(this),
                elements_height = $(this).prop('scrollHeight'),
                doc_height = window.innerHeight,
                fixed_offset = $('#fp-nav').outerHeight() + $('.header').outerHeight() + 15,
                total_height = elements_height + fixed_offset,
                allowed_height = doc_height - fixed_offset;

            function optionTooMany() {
                if (total_height >= allowed_height) { return true; }
                else { return false; }
            }

            function get_height() {
                var h;
                h = doc_height - fixed_offset;
                return parseInt(h);
            }

            if (optionTooMany() === true) {
                elements.css('height',get_height());
                elements.parent().addClass('fp-overflowing overflow-gradient');
            }

            else {
                elements.removeAttr('style');
                elements.parent().removeClass('fp-overflowing overflow-gradient');
            }

            $('.fp-custom-scroll').on('touchmove scroll click', function() {
                $(this).parent('.fp-tableCell').removeClass('overflow-gradient');
            });
            
            // console.log('doc height is ' +doc_height+'. '+ allowed_height + ' is allowed_height. ' + total_height + ' is total_height');
        });
    },
    // adjust_mobile_view: function() {
    //     var rebuild_delay,rebuild_scroll_delay;

    //     // FULL_PAGE.make_options_scroll();

    //     rebuild_delay = setTimeout(function() {
    //         FULL_PAGE.rebuild();
    //         clearTimeout(rebuild_delay);
    //     },300)
    // },
    detect_mobile_addressbar: function() {
        if (("standalone" in window.navigator) && !window.navigator.standalone ){
            console.log('test');
        }
    },
    // detect_orientation: function() {
    //     window.addEventListener('orientationchange', function() {
    //         FULL_PAGE.adjust_mobile_view();
    //         console.log('change orientation');
    //     });
    // },
    on_resize: function() {
        var resizeTimer;

        $(window).on('resize', function(event) {
            // $('.fp-section.active').velocity({translateY: '0' },{
            //     duration: 500, 
            //     easing: [0, 0.96, 0.86, 0.99], 
            //     complete: function() {
            //         // $('#fp-nav, .header').show();
            //     } 
            // });

            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                FULL_PAGE.make_options_scroll();
            }, 100);
        });
    },
    get_data: function(url) {
        $.ajax({
            type:'GET',
            url: UTILS.url('front/fsr?url='+url),
            dataType: 'json',
            cache: true
        })
        .success(function(response) {
            //console.log(response);
            FULL_PAGE.init(response);
            if (response.data.hasOwnProperty('bg_image')){
                $('body').css('background-image', 'url('+response.data.bg_image+')');
            }
        })
        .error(function() {
            console.log("error");
        });
    },
    save_answers: function(question_id,answer) {
        var answers_length = this.answers.length;
        var flag_available = false;
        for(i=0;i<answers_length;i++) {
            if(this.answers[i].question_id == question_id) {
                this.answers[i].answer = answer;
                flag_available = true;
            }
        }
        if(!flag_available){
            this.answers[answers_length] = {question_id:question_id,answer:answer};
        }
        //return this.save_fsr();
    },
    get_referrer: function(name, url) {
        if (!url) {
          url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    save_fsr: function() {
        
        var deferred = $.Deferred();
        var referrer = FULL_PAGE.get_referrer('s');
        var utm_source = FULL_PAGE.GET['utm_source'] || '';
        var utm_medium = FULL_PAGE.GET['utm_medium'] || '';
        var utm_campaign = FULL_PAGE.GET['utm_campaign'] || '';
        var utm_term = FULL_PAGE.GET['utm_term'] || '';
        var utm_content = FULL_PAGE.GET['utm_content'] || '';
        var data = {
            id:FULL_PAGE.fsr.fsr_id,
            token:FULL_PAGE.fsr.sessionToken,
            data:FULL_PAGE.answers, 
            utm_source: utm_source,
            utm_campaign: utm_campaign,
            utm_medium: utm_medium,
            utm_term: utm_term,
            utm_content: utm_content
        };
        $.ajax({
            type:'POST',
            url: UTILS.url('front/response'),
            data:data,
            dataType: 'json',
            cache: true
        })
        .success(function(response) {
            deferred.resolve(response);
        })
        .error(function() {
            deferred.reject( false );
        });

        return deferred.promise();
    },
    build: function(url,preview,get) {
        this.GET = get;
        this.preview = (preview && true) || false;
        this.get_data(url);
        this.is_mobile();
        this.on_resize();
        
    }
}

$(document).ready(function() {
    var GET = {};
    var query = window.location.search.substring(1).split("&");
    
    for (var i = 0, max = query.length; i < max; i++) {
        if (query[i] === "") // check for trailing & with no param
        continue;

        var param = query[i].split("=");
        GET[decodeURIComponent(param[0])] = decodeURIComponent(param[1] || "");
    }

    if (window.location.search.indexOf('url=') > -1) {
        var url = GET['url']; // use this once functionality is ready in function get_data
        console.log(url + ' has url param');
    }
    else {
        var string = document.location.pathname,
            url_string = string.split('/'),
            url;

        var pathname = url_string[url_string.length-1],
            mod_pathname;

        // if (pathname.indexOf('?') > -1) { mod_pathname = pathname.split('?'); }
        if (pathname.indexOf('&') > -1) { mod_pathname = pathname.split('&'); url = mod_pathname[0]; }
        else { url = pathname; }

        console.log(url + ' no url param');
    }

    var preview = GET['preview'];
    FULL_PAGE.build(url,preview,GET);
});