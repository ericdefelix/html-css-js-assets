<!DOCTYPE html>
<html>

<head>
    <?php include 'GoogleExperiment.php';?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Get Qualified Australia: Free Skills Review</title>
    <meta name="description" content="Free Skills Review">
    <meta name="author" content="GQA App Dev">
    <meta name="copyright" content="Get Qualified Australia">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <script>
        var host = document.location.host;

        if (host === 'localhost') {                    
            document.write("<base href='" + document.location.origin + "/fsrv3.0-frontend/' />");
        }
        else {
            document.write("<base href='" + document.location.origin +"' />");
        }
    </script>


    <!-- Core CSS -->
    <link href='gqa-ui-core/img/favicon.png' rel='shortcut icon' type='image/x-icon'/ >
    <link href="gqa-ui-core/fonts/open-sans/open-sans.css" rel="stylesheet" />
    <link href="gqa-ui-core/fonts/material-design-iconic/css/material-design-iconic.min.css" rel="stylesheet" />
    <link href="gqa-ui-core/css/bootstrap.min.css" rel="stylesheet" />
    <link href="gqa-ui-core/css/custom-scrollbar.min.css" rel="stylesheet" />

    <!-- Sniff if browser is loaded on mobile -->
    <script src="gqa-ui-core/js/plugins/unsafe-plugis/isMobile/isMobile.min.js"></script> 

    <!-- GQA -->
    <link href="gqa-ui/css/ui.min.css" rel="stylesheet" />
    <link href="gqa-ui/css/extra.css" rel="stylesheet" />
<img src="//bat.bing.com/action/0?ti=4046501&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" />

</head>

<body class="has-bg">


    <header class="header">
        <img class="logo" src="gqa-ui-core/img/gqa-logo-inverse.svg" alt="Get Qualified Australia">
        <a class="phone-number" href="tel:133-775">133 775</a>
    </header>

    <main class="list-screen" id="fsrFullPage"></main>

    <!-- Sidebar Right Static -->
    <section class="sidebar-right-static sidebar-panel has-overlay" id="sidebarParent">
        <div class="sidebar-header">
            <button type="button" class="btn btn-transparent btn-sidebar-close" data-toggle="sidebar">
                <i class="zmdi zmdi-chevron-left zmdi-hc-2x"></i>
            </button>
            <span class="sidebar-header-caption">Select One</span>
        </div>
        <div class="sidebar-scroll" data-sidebar-scroll >
            <input type="text" class="form-control">
            <ul class='sidebar-list js_sidebarList' id='sidebarSelect'></ul>
        </div>
    </section>
    <!-- Sidebar Right Static -->

    <!-- If submitted sucessfully -->
    <div class="notification" id="notificationMessage" style="display: none;">
        <i class="svg-container"></i>
        <span id="notificationSuccessMessage"></span>
    </div>

    <!-- Core Scripts -->
    <script src="gqa-ui-core/js/jquery-1.11.2.min.js"></script>
    <script src="gqa-ui-core/js/jquery.viewport.min.js"></script>
    
    <script src="gqa-ui-core/js/bootstrap.min.js"></script>
    <script src="gqa-ui-core/js/velocity.min.js"></script>
    <script src="gqa-ui-core/js/velocity.ui.min.js"></script>
    <script src="gqa-ui-core/js/gqa-utilities.min.js"></script>

    <!-- SVG -->
    <!-- <script src="gqa-ui/js/svg-injector.min.js"></script> -->

    <!-- polyfill for handling VM and VH units -->
    <script src="gqa-ui-core/js/plugins/unsafe-plugis/vminpoly/tokenizer.min.js"></script>
    <script src="gqa-ui-core/js/plugins/unsafe-plugis/vminpoly/parser.min.js"></script>
    <script src="gqa-ui-core/js/plugins/unsafe-plugis/vminpoly/vminpoly.min.js"></script>

    <!-- Full Page container -->
    <script src="gqa-ui-core/js/plugins/FullPage/scrolloverflow.min.js"></script>
    <script src="gqa-ui-core/js/plugins/FullPage/jquery.fullPage.js"></script>

    <!-- Custom Scrollbar -->
    <script src="gqa-ui-core/js/plugins/custom-scrollbar.min.js"></script>
    <script src="gqa-ui-core/js/plugins/bootstrap3-typeahead.min.js"></script>

    <!-- GQA -->
    <script type="text/javascript"> var dataLayer = [];</script>

    <script src="gqa-ui/js/ui.js"></script>
    <script src="gqa-ui/js/fsr-gtm.js"></script>
    <script src="gqa-ui/js/upload.js"></script>
    <script src="gqa-ui/js/elements-templates.js"></script>
    <script src="gqa-ui/js/populate-fsr-pages.js"></script>
    <script src="gqa-ui/js/validation.js"></script>
</body>

</html>