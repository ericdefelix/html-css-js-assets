$(function () {

	// Initialize Tooltips
	$('[data-toggle="tooltip"]').tooltip();

    // Toggle Years
    $('body').on('click', '.js_current-year-btn', function() {
    	$('.js_current-year').removeClass('hidden');
    	$('.js_previous-year').addClass('hidden');
    });

    $('body').on('click', '.js_previous-year-btn', function() {
    	$('.js_current-year').addClass('hidden');
    	$('.js_previous-year').removeClass('hidden');
    });

})
