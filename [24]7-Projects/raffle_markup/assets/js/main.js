var RANDOMIZER = {
    name: $('.js_empName'),
    number: $('.js_empNumber'),
    name_display: $('#selectedNameDisplay'),
    dept_display: $('#selectedDeptDisplay'),
    prize_display: $('#selectedPrize'),
    emp_number: $('#selectedEmpNumber').text(),
    emp_num_chars: null,
    li_height: $('.js_randomizeChars .list span').outerHeight(),
    btn_draw: $('#drawEmp'),
    icon: $('#displayIcon'),
    group: $('#displayGroup'),
    tmr: null,
    looper: null,
    tmr_ids: [],
    list: [],
    config: { easing: 'spring', duration: 900 },
    bind: function() {
        $('#drawEmp').on('click', RANDOMIZER.draw);
    },
    gen_li: function() {
        // Create the DOM
        var numbers = '';
        for (i = 1; i <= 5; i++) {
            numbers = numbers + "<li class='list'>";
            for (j = 0; j <= 9; j++) {
                numbers = numbers + "<span>" + j + "</span>";
            }
            numbers = numbers + "</li>";
        }
        $('#randomizeNumbers').append(numbers);

        // Store the elements so we can control it later
        $('.js_randomizeChars .list').each(function() {
            RANDOMIZER.list.push($(this));
        });
    },
    randomize: function() { 
        var selected_employee = Math.floor((Math.random() * RANDOMIZER.list.children.length)); 
        return selected_employee; 
    },
    reset_ui: function() {
        RANDOMIZER.name_display.hide();
        RANDOMIZER.dept_display.hide();
        RANDOMIZER.prize_display.hide();
        RANDOMIZER.icon.velocity({opacity: 1},{display: 'block', duration: 300});
        RANDOMIZER.emp_num_chars = null;

        for (var x = 1; x < RANDOMIZER.list.length; x++) {
            RANDOMIZER.list[x].removeAttr('style');
        };
    },
    draw: function() {
        // var selItem = RANDOMIZER.randomize(),
        // RANDOMIZER.name_display.text($('#activeEmployees tr').eq(selItem).find('td.js_empName').text());
        // RANDOMIZER.dept_display.text($('#activeEmployees tr').eq(selItem).find('td.js_empDepartment').text());
        RANDOMIZER.reset_ui();
        RANDOMIZER.btn_draw.prop('disabled',true).html('Drawing...');

        RANDOMIZER.icon.velocity({rotateY: '360deg'},{ loop: true, easing: [.99,.01,.33,1], duration: 2000 });
        RANDOMIZER.tmr = setTimeout(RANDOMIZER.stop, 5000);

        RANDOMIZER.animate_numbers();
        RANDOMIZER.emp_num_chars = RANDOMIZER.emp_number.split(''); // store characters
        RANDOMIZER.emp_num_chars = RANDOMIZER.emp_num_chars.splice(4,RANDOMIZER.emp_num_chars.length); // Exclude 247 characters
    },
    animate_numbers: function() {
        for (var x = 0; x < RANDOMIZER.list.length; x++) {
            RANDOMIZER.list[x].velocity(
                {
                    translateY: -(RANDOMIZER.list[x].innerHeight() - 139)
                }, 
                { 
                    duration: 500, 
                    easing: 'linear', 
                    delay: (x*100) + 200, 
                    complete: function() {
                        RANDOMIZER.loop($(this), $(this).innerHeight()); // Pass the objects and the height for animation
                        RANDOMIZER.tmr_ids.push(RANDOMIZER.looper); // Store the timers for each animating element so we can clear them later
                    }
                }
            );
        };

    },
    loop: function(obj,height) {
        RANDOMIZER.looper = setInterval(function() {
            obj.velocity({translateY: [-(height - RANDOMIZER.li_height),'0px'] });
        }, 500);
    },
    stop: function() {
        RANDOMIZER.tmr = clearTimeout(RANDOMIZER.tmr);

        // Stops previous activity
        RANDOMIZER.icon.velocity('stop');
        RANDOMIZER.icon.velocity({ opacity: 0},{ display: 'none', duration: 300 });
        RANDOMIZER.btn_draw.removeAttr('disabled').html('Draw');

        // Display the winner
        RANDOMIZER.display_winner();
    },
    display_winner: function() {        
        RANDOMIZER.group.velocity({ opacity: 1 },{ display: 'block', duration: 500,

            // Select which numbers are to be displayed
            begin: function() {
                // Clear all timers for the animating numbers
                for (var i = 0; i < RANDOMIZER.tmr_ids.length ; i++) {
                    window.clearInterval(RANDOMIZER.tmr_ids[i]);
                };

                for (var x = 1; x < RANDOMIZER.list.length; x++) {
                    RANDOMIZER.list[x].velocity(
                    { 
                        translateY: ["-"+RANDOMIZER.emp_num_chars[x - 1]+"0%", "0%"]
                    }, RANDOMIZER.config);
                };

                switch(RANDOMIZER.emp_number.charAt(0)){
                    case 'P':
                        RANDOMIZER.list[0].velocity({ translateY: '-0%' }, RANDOMIZER.config );
                        break;
                    case 'R':
                        RANDOMIZER.list[0].velocity({ translateY: '-33.3333%' }, RANDOMIZER.config );
                        break;
                    case 'T':
                        RANDOMIZER.list[0].velocity({ translateY: '-66.6666%' }, RANDOMIZER.config );
                        break;
                    default:
                        RANDOMIZER.list[0].velocity({ translateY: '-0%' }, RANDOMIZER.config );
                        break;
                }

            },

            // Animate the displaying of the winner
            complete: function(){
                RANDOMIZER.name_display.velocity('transition.flipBounceYIn', { display: 'block', duration: 500 });
                RANDOMIZER.dept_display.velocity('transition.flipBounceYIn',{ delay: 300, display: 'block', duration: 500 });
                RANDOMIZER.prize_display.velocity('transition.flipBounceYIn',{ delay: 600, display: 'block', duration: 500 });
            }
        });
    },
    build: function() {
        this.bind();
        this.gen_li();
    }
}

$(document).ready(function() {
    RANDOMIZER.build();
});