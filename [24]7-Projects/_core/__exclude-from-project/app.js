// Initialize UI Components
$(function () {


    // BS3 Components
 	$('[data-toggle="tooltip"]').tooltip({
        animation: false
    });
    
  
    // Filter list
    $(".js_dropdown-select li a").click(function(event) {
        event.preventDefault();
        var item = $(this).closest('.js_dropdown-select').prev('button').find('.js_dropdown-select-value');
        $(item).text($(this).text());
        $(item).val($(this).text());
    });

    var cur_class = 'sub-navbar-turquoise';
    $('#subnavbarClass').html(cur_class);
    $('#selectNavbarColor input[name="subnavbar"]').on('change', function() {

        $('#subnavbar').removeClass(cur_class);
        $('#subnavbar').addClass($(this).val());
        $('#subnavbarClass').html($(this).val());
        prev_class = $(this).val();

        setTimeout(
            function(){
                cur_class = prev_class;
        }, 200);
    });
})


$(".snippet-value").each(function(index) {
	$(this).next().find('textarea').val(this.innerHTML);
});

function simulatePace() {
    location.reload();
}

function simulateMessage() {
    var newline = ' <li class="comment-not-you animated bounce"> <div class="chat-card-avatar"> <div class="avatar"> <img src="assets-common/img/avatar-default.svg" alt="User"> </div> </div> <div class="chat-card-data"> <span class="text-info">John Doe</span> <small class="chat-timestamp">06/26/15 2:13PM</small> <p>This is a sample inserted message.</p> </div> </li>';
    var message_list = $('.chat-card-comments ul');
    var offset = message_list.height(); 

    $('.writing-indicator').removeClass('hidden').show()
    setTimeout(
        function(){ 
        $('.writing-indicator').hide();
        message_list.append(newline);
        message_list.velocity( "scroll",
        {
            translateZ: 0, // Force Hardware Acceleration
            duration: 450,
            easing: [.74,.01,.46,.99],
            offset: offset,
            container: $('.chat-card-comments')
        });
    }, 2500);

}

$('#simTopRightAlert').click(function() {
    $('.top-right').show().addClass('animated fadeInRight');
});

$('#simBottomRightAlert').click(function() {
    $('.bottom-right').show().addClass('animated fadeInRight');
});

$('#closeTopRightAlert').click(function() {
    $('.top-right').addClass('fadeOutRight').removeClass('fadeInRight');
});

$('#closeBottomRightAlert').click(function() {
    $('.bottom-right').addClass('fadeOutRight').removeClass('fadeInRight');
});

$('#simNotifications').click(function() {
    $('.alert-notification').addClass('animated fadeIn').show();

    setTimeout(function(){
        $('.alert-notification').addClass('fadeOut').removeClass('fadeIn');        
    }, 2000)
});


var current_class = '';
$('#btnModalSM').click(function() {
    $(this).closest('.modal-dialog').addClass('modal-sm');
    $(this).closest('.modal-dialog').removeClass(current_class);
    current_class = 'modal-sm';
});
$('#btnModalLG').click(function() {
    $(this).closest('.modal-dialog').addClass('modal-lg');
    $(this).closest('.modal-dialog').removeClass(current_class);
    current_class = 'modal-lg';
});
$('#btnModalXLG').click(function() {
    $(this).closest('.modal-dialog').addClass('modal-xlg');
    $(this).closest('.modal-dialog').removeClass(current_class);
    current_class = 'modal-xlg';
});
$('#btnModalFULL').click(function() {
    $(this).closest('.modal-dialog').addClass('modal-full');
    $(this).closest('.modal-dialog').removeClass(current_class);
    current_class = 'modal-full';
});
$('#removeClass').click(function() {
    $(this).closest('.modal-dialog').removeClass(current_class);
    current_class = '';
});
