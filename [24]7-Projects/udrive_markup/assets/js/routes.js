/*
 * Pointer Events Polyfill: Adds support for the style attribute "pointer-events: none" to browsers without this feature (namely, IE).
 * (c) 2013, Kent Mewhort, licensed under BSD. See LICENSE.txt for details.
 */

// constructor
function PointerEventsPolyfill(options){
    // set defaults
    this.options = {
        selector: '*',
        mouseEvents: ['click','dblclick','mousedown','mouseup'],
        usePolyfillIf: function(){
            if(navigator.appName == 'Microsoft Internet Explorer')
            {
                var agent = navigator.userAgent;
                if (agent.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/) != null){
                    var version = parseFloat( RegExp.$1 );
                    if(version < 11)
                      return true;
                }
            }
            return false;
        }
    };
    if(options){
        var obj = this;
        $.each(options, function(k,v){
          obj.options[k] = v;
        });
    }

    if(this.options.usePolyfillIf())
      this.register_mouse_events();
}

// singleton initializer
PointerEventsPolyfill.initialize = function(options){
    if(PointerEventsPolyfill.singleton == null)
      PointerEventsPolyfill.singleton = new PointerEventsPolyfill(options);
    return PointerEventsPolyfill.singleton;
};

// handle mouse events w/ support for pointer-events: none
PointerEventsPolyfill.prototype.register_mouse_events = function(){
    // register on all elements (and all future elements) matching the selector
    $(document).on(this.options.mouseEvents.join(" "), this.options.selector, function(e){
       if($(this).css('pointer-events') == 'none'){
             // peak at the element below
             var origDisplayAttribute = $(this).css('display');
             $(this).css('display','none');

             var underneathElem = document.elementFromPoint(e.clientX, e.clientY);

            if(origDisplayAttribute)
                $(this)
                    .css('display', origDisplayAttribute);
            else
                $(this).css('display','');

             // fire the mouse event on the element below
            e.target = underneathElem;
            $(underneathElem).trigger(e);

            return false;
        }
        return true;
    });
};


//--------------------------------------------------------------------------------
// Determine Day
//--------------------------------------------------------------------------------
var MERIDIEM = {
	meridiem: $('*[data-meridiem]'),
	tmr: undefined,
	part: '',
	icon: $('#environmentIndicator > .icon'),

	build: function() {
	    var today = new Date(),
	        curHr = today.getHours();

	    if(curHr >= 8 && curHr <= 16) { this.part = 'day'; }
	    else if (curHr >= 6 && curHr <= 7) { this.part = 'morning'; }
	    else if (curHr > 16 && curHr <= 18) { this.part = 'afternoon'; }
	    else { this.part = 'evening'; }

	    MERIDIEM.meridiem.attr('data-meridiem', MERIDIEM.part);
	    MERIDIEM.icon.attr('class', '').addClass('icon fi-' + this.part);
	},

	update: function() {
		this.tmr = setInterval(function(){
			MERIDIEM.meridiem.setAttribute('data-meridiem', MERIDIEM.part);
			clearInterval(MERIDIEM.tmr);
		}, (60*60*1000))
	}
}


//--------------------------------------------------------------------------------
// Build Structures
//--------------------------------------------------------------------------------
var BUILDINGS = {
	bldg: $('.building'),
	bldg_set: $('.building-set'),
	bldg_gap: $('.building-surroundings'),
	map_layers: $('.js_dynamicWidth'),
	map_width: null,
	menu: [],
	scroll: $('.js_customScroll'),
	course_desc: '.course-description',
	doc: $(document),
	tmr_bldg_menu: [],

	init: function() {
		BUILDINGS.bldg_gap = $('.building-surroundings');
	    BUILDINGS.bldg = $('.building');
	    BUILDINGS.bldg_set = $('.building-set');
	    BUILDINGS.map_layers = $('.js_dynamicWidth');
		BUILDINGS.compute_width();
	},

	bind: function() {
		this.doc.on('click', '.building', function() {
			BUILDINGS.show_menu($(this));
			// $('<div class="building-menu-overlay"></div>').insertBefore($(this));
		});

        this.scroll.mCustomScrollbar({
            autoHideScrollbar:  false,
            axis:               'y',
            contentTouchScroll: true,
            scrollbarPosition:  'inside',
            scrollInertia:      200,
            scrollButtons:      {enable: false},
            theme:              'dark',
            advanced:           { updateOnContentResize: true }
        });

        this.doc.on('click', this.course_desc, function() { $(this).toggleClass('truncate'); });
        this.doc.on('click', '.btn-enroll', function(event) { $(event.target).siblings('.course-description').toggleClass('truncate'); });
        // this.doc.on('click', '.btn-close-menu-self', function(){ BUILDINGS.close_menu_self($(this)); } );

        this.doc.on('click', '.btn-skip', function(){ BUILDINGS.hide_intro_message(); } );

        this.doc.on('click,hover', '.not-applicable-sign', function(event) {
	        if (!event.preventDefault)
	        {
	            event.returnValue = false;
	            event.cancelBubble = true;
	        }
        	event.preventDefault();
        	event.stopPropagation();
        	event.stopImmediatePropagation();
        	return false;
        });
	},

	close_menu: function(source) {
		$(this.menu[returnIndex()]).removeClass('open').removeAttr('style').hide(0,function(){
			BUILDINGS.menu.shift();
			// console.log(BUILDINGS.menu);
		}); 
		$(this.menu[0]).siblings('.building-menu-overlay').remove();

		function returnIndex() {
			var x;
			if (source === 'manual') { x = BUILDINGS.menu.length-1; return x; }
			if (source === 'auto') { x = 0; return x; }
		}
		// clearTimeout(this.tmr_bldg_menu[0]);
	},

	show_menu: function(that) {
		var targetBldg = that.next();

		targetBldg.show(0, function(){
			if (!targetBldg.hasClass('open')) {
				targetBldg.addClass('open');
				BUILDINGS.menu.push(targetBldg);
			}
		});

		// Close the previous building
		if (BUILDINGS.menu.length == 2) { this.close_menu('auto'); }

		// Display Professor
		$(this.menu[this.menu.length-1]).find('.professor-message').show();

		// Auto display of Professor/building introductory message
		BUILDINGS.reset_menu();

		// var tmr;
		// tmr = setTimeout( BUILDINGS.hide_intro_message, BUILDINGS.how_long() );
		// BUILDINGS.tmr_bldg_menu.push(tmr);
	},

	how_long: function() {
		var len = ($('.building-menu.open .professor-message').text().length);

		if (len > 350) { return 18000; }
		else if(len <= 350 && len > 150) { return 13000; }
		else { return 7000; }
	},

	hide_intro_message: function(that_tmr) {
		$('.building-menu.open .building-menu-tabs').show();
		$('.building-menu.open .professor-message').hide();
		$('.building-menu.open .btn-skip').hide();

		clearTimeout(BUILDINGS.tmr_bldg_menu[0]);
		BUILDINGS.tmr_bldg_menu.shift();	
	},

	reset_menu: function() {
		// clearTimeout(BUILDINGS.tmr_bldg_menu[0]);
		$(this.menu[0]).find('.building-menu-tabs').hide();
		$(this.menu[0]).find('.professor-message').show();
		$(this.menu[0]).find('.btn-skip').show();
		BUILDINGS.tmr_bldg_menu.shift();		
	},

	compute_width: function() {
		// Set the map width for navigation calculation
		var bldg = 0, gap = 0;
		this.bldg_set.each(function() { bldg += parseInt($(this).width(), 10); });
		this.bldg_gap.each(function() { gap += parseInt($(this).width(), 10); });
		CAR.map_width = bldg + gap + $(document).outerWidth()/2;

		// Set the width of the map for laying out the building blocks
		this.map_width = CAR.map_width;

		// Set a default width if no buildings are present
		this.bldg.length ? this.map_layers.css('width',this.map_width*2) : this.map_layers.css('width', $(document).innerWidth());
		this.bldg.length ? $('#foreground').css('width',this.map_width*2) : $('#foreground').css('width',$(document).innerWidth());

		return this.map_width;
	},
	generate: function() {
		this.bind();
		this.compute_width();
	}
}

//--------------------------------------------------------------------------------
// Build  HUD
//--------------------------------------------------------------------------------
var HUD = {
	menuBtn: $('#btnHUDMenu'),
	menuSubBtns: $('.btn-hud-submenu'),
	is_open: false,
	doc: $(document),
	bd: '<div class="menu-backdrop"></div>',
	confirmJob: $('#confirmJobFunction'),
	confirmRole: $('#confirmRole'),

	bind: function() {
		this.doc.on('click','#btnHUDMenu', function(){ HUD.is_open == false ? HUD.view_menu() : HUD.hide_menu(); });

		this.doc.on('click', '#jobFunctions label', function() {
		    if (!$("input[name='jobSelection']:checked").val()) {
				HUD.confirmJob.attr('disabled', false);
				HUD.confirmJob.find('span:first').hide();
				HUD.confirmJob.find('span:last').show();
		    }
		});

		this.doc.on('click', '#confirmJobFunction', function() { 
			$('#jobFunctions').hide();
			$('#jobRoles').show();

			$('#selectedJobFunction').html($("input[name='jobSelection']:checked").next('label').text());
		});

		this.doc.on('click', '#jobRoles label', function() {
		    if (!$("input[name='roleSelection']:checked").val()) {
				HUD.confirmRole.attr('disabled', false);
				HUD.confirmRole.find('span:first').hide();
				HUD.confirmRole.find('span:last').show();
		    }
		});

		$('#modalRoutes').on('hidden.bs.modal', HUD.reset_switch_route_view);

	},
	reset_switch_route_view: function() {
		HUD.confirmJob.attr('disabled', true);
		HUD.confirmJob.find('span:first').show();
		HUD.confirmJob.find('span:last').hide();
		$('#jobFunctions').show();
		$('#jobRoles').hide();
		HUD.confirmRole.attr('disabled', true);
		HUD.confirmRole.find('span:first').show();
		HUD.confirmRole.find('span:last').hide();

		$("input[name='roleSelection']").prop('checked', false);
	},
	view_menu: function() {
		HUD.is_open = true;
		$(this.bd).insertBefore($('#mapMenu'));
		HUD.menuBtn.find('.md').velocity({ rotateZ: '45deg' } , { easing: 'spring' });
		$('#btnAvatar').velocity({ translateX: [0 , '50px'], opacity: 1, },{ display: 'block', easing: [.96,.17,.19,.78], duration: 120 });
		$('#btnRoutes').velocity({ translateX: [0 , '50px'], opacity: 1, },{ display: 'block', easing: [.96,.17,.19,.78], duration: 120,delay: 70 });
		$('#btnFastTravel').velocity({ translateX: [0 , '50px'], opacity: 1, },{ display: 'block', easing: [.96,.17,.19,.78], duration: 120, delay: 170 });
		$('#btnAbout').velocity({ translateX: [0 , '50px'], opacity: 1, },{ display: 'block', easing: [.96,.17,.19,.78], duration: 120, delay: 270 });
		$('.menu-backdrop').on('click', this.hide_menu);
	},
	hide_menu: function() {
		HUD.menuBtn.find('.md').velocity({ rotateZ: '0deg' } , { easing: 'spring' });
		$('#btnAvatar, #btnRoutes, #btnAbout, #btnFastTravel').velocity('reverse',{ complete: function(){ HUD.is_open = false; $('.menu-backdrop').remove(); } });
	},
	build: function() {
		this.bind();
	}
}

//--------------------------------------------------------------------------------
// Build Car Navigation
//--------------------------------------------------------------------------------
var CAR = {
    divs: $('.layer'),
    ease: 0.1,
    targetX: 0,
    currentX: 0,

	wheelspin: null,
	wheel: $('.car-wheel'),
	degrees: 120,
	_x: null,
	body: $('#carBody'),
	car: $('#car'),

    bind: function() {
        CAR.divs = Array.prototype.slice.call(CAR.divs, 0);
        CAR.divs.forEach(function(d) { 
        	d.parallaxSpeed = d.getAttribute("data-parallax-speed");
        });

        // document.addEventListener('touchmove', function(e) { 
        // 	e.preventDefault();
        // });

        // Add virtual scroll listener
        VirtualScroll.on(function(e) {

            // Accumulate delta value on each scroll event
            CAR.targetX -= e.deltaX;

            // Clamp the value so it doesn't go too far left
            CAR.targetX = Math.max(-(BUILDINGS.map_width - $(document).innerWidth()), CAR.targetX);
            CAR.targetX = Math.min(0, CAR.targetX);

            CAR.move();
        });
    },

    move: function() {
        // Make it appear as if it's moving forward
        if (CAR.targetX < CAR.currentX && CAR._x < (BUILDINGS.map_width - $(document).innerWidth())) {
			CAR.forward_car();
        }

        // Make it appear as if it's moving in reverse
        if (CAR.targetX > CAR.currentX && CAR._x > 0) {
        	CAR.reverse_car();
        }
    },

    map_scroll: function(x_pos) {
        requestAnimationFrame(CAR.map_scroll);

        CAR.currentX += Math.round((CAR.targetX - CAR.currentX) * CAR.ease);

        // Run on every layer, multiplying it by the parallax speed
        CAR.divs.forEach(function(d) {
            var p = d.parallaxSpeed;

            // Create the CSS transform string
            var v1 = "translateX(" + (CAR.currentX*p) + "px) translateZ(0)";// translateZ(0)";

            // Apply CSS style
            d.style['webkitTransform'] = v1;
            d.style['msTransform'] = v1;
            d.style['webkitTransform'] = v1;
            d.style['mozTransform'] = v1;
            d.style.transform = v1;

        });
    },

	forward_car: function() {
		this._x += this.degrees;

		this.wheel.css({
			transform: 'rotateZ('+ this._x +'deg)',
			MozTransform: 'rotateZ('+ this._x +'deg)',
			WebkitTransform: 'rotateZ('+ this._x +'deg)',
			msTransform: 'rotateZ('+ this._x +'deg)'
		})
		this.body.removeClass('car-reverse');
	},

	reverse_car: function() {
		// console.log(CAR.targetX);
		this._x += -this.degrees;

		this.wheel.css({
			transform: 'rotateZ('+ this._x +'deg)',
			MozTransform: 'rotateZ('+ this._x +'deg)',
			WebkitTransform: 'rotateZ('+ this._x +'deg)',
			msTransform: 'rotateZ('+ this._x +'deg)'
		})
		this.body.addClass('car-reverse');
	},

    build: function(){
        this.bind();
        this.map_scroll();
    }
}

//--------------------------------------------------------------------------------
// Build Building Selection
//--------------------------------------------------------------------------------
var FAST_TRAVEL = {
	doc: $(document),
	car_pos: 0,
	list_pos: 0,
	bldg_list: '#travelBldgList',
	bldg_scroll: '#travelBldgScroll',
    bind: function() {
    	this.doc.on('click', '.travel-bldg-list li[data-goto]', function() {
    		FAST_TRAVEL.go_to($(this).data('goto'));
    	});
    	this.doc.on('click', '#travelRight', function() {
    		FAST_TRAVEL.move('right');
    	});
    	this.doc.on('click', '#travelLeft', function() {
    		FAST_TRAVEL.move('left');
    	});
    },
    go_to: function(building_name) {
    	var car_pos = $('[data-bldg-name="'+ building_name +'"]').position();
    	FAST_TRAVEL.car_pos = Math.round(car_pos.left);

    	// Override the current target of the car
    	try {
    		CAR.targetX = -FAST_TRAVEL.car_pos;
    		CAR.move();

    	} catch(e) {
    		// console.log('building name not synced');
    	}
    },
    move: function(direction) {
    	w = FAST_TRAVEL.doc.find('#travelBldgScroll').innerWidth(),
    	offset = 150;
    	a = w - 86;

    	FAST_TRAVEL.doc.find('#travelBldgList').velocity({ 
    		translateX: function() {
    			if (direction == 'right') {
	    			FAST_TRAVEL.list_pos = FAST_TRAVEL.list_pos + offset;
	    			// console.log(FAST_TRAVEL.list_pos + ' right ');
	    			return -FAST_TRAVEL.list_pos;
    			}
    			if (direction == 'left') {
    				if (FAST_TRAVEL.list_pos >= offset) {
		    			FAST_TRAVEL.list_pos = FAST_TRAVEL.list_pos - offset;
		    			// console.log(FAST_TRAVEL.list_pos + ' left ');
		    			return -FAST_TRAVEL.list_pos;
    				}
    			}
    		}
    	},{ 
    		easing:[0, 0.96, 0.86, 0.99],
    		duration: 500
    	});
    },
    build: function() {
    	this.bind();
    }
}

// Generate clouds
var CLOUDS = {
	cloud: '<span class="clouds"></span>',
	group: [],
	animated: [],
	layer: $('.cloud-set'),
	seed: 15,
	top_range: $('.cloud-set').innerHeight() - 40,

	random_offset: function() {
		var o = Math.floor(Math.random() * 5 + 1),
			s = null;
		
		if (o == 3) { return 50; }
		else { return 0; }
	},
	random_top: function() {  return Math.floor(Math.random() * this.top_range ) + 70; },
	random_left: function() {  return Math.floor(Math.random() * 14400/5 ); },
	random_width: function() {  return Math.floor(Math.random() * 190 ) + 80; },
	random_height: function() {  return Math.floor(Math.random() * 50 ) + 5; },
	random_opacity: function() {  return (Math.floor(Math.random() * 80 ) + 5) / 100; },
	random_duration: function() { return (Math.floor(Math.random() * 100000 ) + 50000); },

	emit: function() {
		for (var c = 0 ; c < this.seed; c++) {
			this.group.push(
				$(this.cloud).css({ 
					top: this.random_top() + this.random_offset() + 'px' , 
					left: this.random_left() + this.random_offset() + 'px',
					width: this.random_width() + 40 + 'px',
					height: this.random_height() + 'px',
					opacity: this.random_opacity(),
					mozOpacity: this.random_opacity(),
					webkitOpacity: this.random_opacity(),
					filter: 'alpha(opacity='+ this.random_opacity()*100 +')'
				})
			);

			if (Math.floor(Math.random() * this.seed)%2 == 0) {
				this.animated.push(c);
			}
			
		};

		for (var a = 0; a < CLOUDS.animated.length; a++) {
			$(this.group[a]).velocity( { translateX: '-100%' }, { easing: 'linear', duration: CLOUDS.random_duration() })
		};
	},

	place: function() {
		this.layer.append(this.group);
	},

	generate: function() {
		this.emit();
		this.place();
	}
}

// LMVP PLane, etc.
var FLYING_OBJECTS = {
	doc: $(document),
	plane: $('.banner-lmvp'),
	banner: $('.lmvp-message'),
	airspace: $('.cloud-set').innerHeight() - 40,

	random_interval: function() { return (Math.floor(Math.random() * 1000 ) + 2000); },
	random_elevation: function() {  return Math.floor(Math.random() * FLYING_OBJECTS.airspace); },

	launch: function() {
		FLYING_OBJECTS.plane.css({
			"top": FLYING_OBJECTS.random_elevation() + "px", 
			"left": FLYING_OBJECTS.doc.width() + "px",
		});
	},	

	fly: function() {
		var travelLength = FLYING_OBJECTS.doc.width() + FLYING_OBJECTS.plane.width() + 500;
		FLYING_OBJECTS.plane.velocity({ translateX: [ - travelLength, FLYING_OBJECTS.doc.width() ] }, { 
			easing: 'linear', 
			duration: 60000,
			complete: function() {
				FLYING_OBJECTS.launch();
			}
		});
	},

	get_frequency: function() {
		var p;
		clearInterval(p);
		p = setInterval(function(){
			FLYING_OBJECTS.fly();
		}, FLYING_OBJECTS.random_interval);		
	},

	build: function(){
		FLYING_OBJECTS.get_frequency();
	}
}

//--------------------------------------------------------------------------------
// Build Environment
//--------------------------------------------------------------------------------

// Generate trees, benches, etcetera

// sprite map
var sp = {
	stall: ['stall-one','stall-two','stall-three','stall-four'],
	bush: ['bush-one','bush-two','bush-three','bush-four','bush-five'],
	flowerpot: ['flowerpot-one','flowerpot-two','flowerpot-three','flowerpot-four','flowerpot-five'],
	flower: ['flower-one','flower-two','flower-three','flower-four'],
	tree: ['tree-one','tree-two','tree-three','tree-four','tree-five','tree-six','tree-seven','tree-eight','tree-nine','tree-ten','tree-eleven'],
	bench: ['bench-one','bench-two','bench-three','bench-four'],
	bin: ['bin-one','bin-two','bin-three'],
	flag: ['flag-one','flag-two','flag-three','flag-four'],
}

// build the sprites
var ENVIRONMENT = {
    space: $('.building-surroundings'), // 18 gaps
	get_elem: function(className,left,top,zindex) { elem = '<i class="'+ className +'" style="left:'+ left +'px; top: '+ top +'px; z-index:' + zindex +';"></i>'; return elem; },
	generate: function () {
	    this.space = $('.building-surroundings');

		this.space.eq(1).append(
			this.get_elem(sp.tree		[0],100,62,1) + 
			this.get_elem(sp.bush		[4],-21,202,3) +
			this.get_elem(sp.bush		[0],408,186.5,1) +
			this.get_elem(sp.tree		[10],443,126,1) +
			this.get_elem(sp.tree		[0],225,62,1) +
			this.get_elem(sp.bench		[2],146,184.5,1) +
			this.get_elem(sp.tree		[3],329,95.5,1) +
			this.get_elem(sp.flowerpot	[3],434,183,1) +
			this.get_elem(sp.bush		[3],283,197.5,1) +
			this.get_elem(sp.bush		[3],31,191,1) +
			this.get_elem(sp.flower		[3],-19,192.5,3)
		);
		this.space.eq(2).append(
			this.get_elem(sp.tree		[1],5,90.5,1) + 
			this.get_elem(sp.tree		[5],54,155,1) + 
			this.get_elem(sp.tree		[3],132,96.5,1) + 
			this.get_elem(sp.tree		[1],340,83.5,1) + 
			this.get_elem(sp.tree		[10],417,128,1) + 
			this.get_elem(sp.bush		[4],169,201,1) + 
			this.get_elem(sp.bush		[2],-15,205,1) + 
			this.get_elem(sp.bush		[3],275,200.5,1) + 
			this.get_elem(sp.flowerpot	[2],404,184,1) + 
			this.get_elem(sp.flowerpot	[1],379,186.5,1) + 
			this.get_elem(sp.flower		[5],49,188,1) + 
			this.get_elem(sp.flower		[2],64,190,1) + 
			this.get_elem(sp.flower		[3],75,189.5,1) + 
			this.get_elem(sp.bin		[2],106,184,1)
		);
		this.space.eq(3).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(4).append(
			this.get_elem(sp.flag		[3],256,45,1) + 
			this.get_elem(sp.bush		[4],155,205,2) + 
			this.get_elem(sp.bush		[2],399,189.5,1) + 
			this.get_elem(sp.bush		[3],369,198.5,1) + 
			this.get_elem(sp.tree		[9],114,113.5,1) + 
			this.get_elem(sp.tree		[2],185,106,1) + 
			this.get_elem(sp.tree		[0],70,70,1) + 
			this.get_elem(sp.bush		[2],11,199.5,1) + 
			this.get_elem(sp.flower		[2],430,190,1) + 
			this.get_elem(sp.flower		[0],225,194,1) + 
			this.get_elem(sp.flower		[3],461,191.5,1) + 
			this.get_elem(sp.tree		[9],455,113.5,1) + 
			this.get_elem(sp.tree		[5],-2,160.5,1)
		);
		this.space.eq(5).append(
			this.get_elem(sp.tree		[3],-5,98.5,1) + 
			this.get_elem(sp.tree		[1],118,85.5,1) + 
			this.get_elem(sp.tree		[3],184,96.5,1) + 
			this.get_elem(sp.tree		[9],58,125.5,1) + 
			this.get_elem(sp.tree		[9],311,104.5,1) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.tree		[7],236,107,1)
		);
		this.space.eq(6).append(
			this.get_elem(sp.stall		[1],31,125.5,1) + 
			this.get_elem(sp.bush		[4],-6,210,1) + 
			this.get_elem(sp.stall		[2],222,128.5,1) + 
			this.get_elem(sp.tree		[10],428,122,1) + 
			this.get_elem(sp.bin		[1],411,167,1) + 
			this.get_elem(sp.bush		[2],339,200.5,1) + 
			this.get_elem(sp.flower		[2],371,190,1) + 
			this.get_elem(sp.flower		[2],359,193,1) + 
			this.get_elem(sp.flower		[1],388,194,1) + 
			this.get_elem(sp.bush		[3],467,196.5,1)
		);
		this.space.eq(7).append(
			this.get_elem(sp.flag		[1],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[10],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[1],430,90,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(8).append(
			this.get_elem(sp.bin		[1],4,166,1) +
			this.get_elem(sp.tree		[2],416,114.5,1) +
			this.get_elem(sp.tree		[3],29,104.5,1) +
			this.get_elem(sp.tree		[2],121,115.5,1) +
			this.get_elem(sp.tree		[3],262,96.5,1) +
			this.get_elem(sp.tree		[9],333,121.5,1) +
			this.get_elem(sp.tree		[9],197,120.5,1) +
			this.get_elem(sp.flowerpot	[4],460,195.5,1) +
			this.get_elem(sp.flowerpot	[2],-24,185.5,1) +
			this.get_elem(sp.flowerpot	[2],45,190.5,1) +
			this.get_elem(sp.flowerpot	[0],-39,183.5,1) +
			this.get_elem(sp.bush		[3],84,202.5,1) +
			this.get_elem(sp.stall		[3],300,177,1)
		);
		this.space.eq(9).append(
			this.get_elem(sp.flag		[2],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[10],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[8],430,130,1) +
			this.get_elem(sp.tree		[5],-32,159,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(10).append(
			this.get_elem(sp.stall		[2],374,134.5,1) +
			this.get_elem(sp.tree		[2],0,106.5,1) +
			this.get_elem(sp.tree		[2],471,109.5,1) +
			this.get_elem(sp.tree		[2],311,108.5,1) +
			this.get_elem(sp.tree		[2],62,108.5,1) +
			this.get_elem(sp.tree		[5],-2,160.5,1) +
			this.get_elem(sp.tree		[9],265,113.5,1) +
			this.get_elem(sp.tree		[7],97,114,1) +
			this.get_elem(sp.bush		[2],136,190.5,1) +
			this.get_elem(sp.bush		[4],228,195,1) +
			this.get_elem(sp.bench		[3],122,183.5,1) +
			this.get_elem(sp.bench		[3],241,184.5,1) +
			this.get_elem(sp.flower		[0],81,192,1) +
			this.get_elem(sp.flower		[2],60,189,1)
		);
		this.space.eq(11).append(
			this.get_elem(sp.tree		[2],14,110.5,1) +
			this.get_elem(sp.tree		[2],464,107.5,1) +
			this.get_elem(sp.tree		[3],79,97.5,1) +
			this.get_elem(sp.tree		[3],395,96.5,1) +
			this.get_elem(sp.tree		[2],270,108.5,1) +
			this.get_elem(sp.tree		[9],343,113.5,1) +
			this.get_elem(sp.tree		[7],51,119,1) +
			this.get_elem(sp.tree		[8],230,126.5,1) +
			this.get_elem(sp.bush		[3],146,189.5,1) +
			this.get_elem(sp.bush		[2],-8,194.5,1) +
			this.get_elem(sp.bush		[3],410,202.5,1) +
			this.get_elem(sp.bin		[2],19,183,1) +
			this.get_elem(sp.stall		[1],71,126.5,1) +
			this.get_elem(sp.stall		[0],253,125.5,1)
		);
		this.space.eq(12).append(
			this.get_elem(sp.tree		[9],-5,128.5,1) + 
			this.get_elem(sp.tree		[9],118,128.5,1) + 
			this.get_elem(sp.tree		[3],184,96.5,1) + 
			this.get_elem(sp.tree		[1],58,84,1) + 
			this.get_elem(sp.tree		[7],381,124,2) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.bench		[2],276,187,2)
		);
		this.space.eq(13).append(
			this.get_elem(sp.bin		[1],0,168,2) +
			this.get_elem(sp.tree		[9],437,114.5,2) +
			this.get_elem(sp.tree		[3],332,95.5,2) +
			this.get_elem(sp.tree		[9],204,111.5,2) +
			this.get_elem(sp.tree		[3],75,104.5,2) +
			this.get_elem(sp.bush		[4],139,192,2) +
			this.get_elem(sp.bush		[2],42,202.5,2) +
			this.get_elem(sp.bush		[3],404,198.5,2) +
			this.get_elem(sp.bush		[1],244,197,2) +
			this.get_elem(sp.flower		[2],206,191,2) +
			this.get_elem(sp.flower		[0],172,192,2) +
			this.get_elem(sp.flower		[0],391,194,2) +
			this.get_elem(sp.flower		[2],381,193,2) +
			this.get_elem(sp.flower		[3],155,192.5,2) +
			this.get_elem(sp.flowerpot	[4],498,193.5,2) +
			this.get_elem(sp.bush		[1],310,203.5,2)
		);
		this.space.eq(14).append(
			this.get_elem(sp.tree		[9],-5,113.5,1) + 
			this.get_elem(sp.tree		[9],118,112.5,1) + 
			this.get_elem(sp.tree		[3],184,108.5,1) + 
			this.get_elem(sp.tree		[1],58,92.5,1) + 
			this.get_elem(sp.tree		[7],381,124,2) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.bench		[1],284,187,2)
		);
		this.space.eq(15).append(
			this.get_elem(sp.flag		[0],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[9],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[1],430,90,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(16).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(17).append(
			this.get_elem(sp.tree 		[2],5,106.5,1) +
			this.get_elem(sp.tree 		[2],310,106.5,1) +
			this.get_elem(sp.tree 		[3],97,95.5,1) +
			this.get_elem(sp.tree 		[3],418,96.5,1) +
			this.get_elem(sp.tree 		[9],38,122.5,1) +
			this.get_elem(sp.tree 		[9],353,123.5,1) +
			this.get_elem(sp.tree 		[7],163,123,1) +
			this.get_elem(sp.tree 		[1],240,79.5,1) +
			this.get_elem(sp.tree 		[8],291,133.5,1) +
			this.get_elem(sp.flowerpot 	[3],496,183,1) +
			this.get_elem(sp.flowerpot 	[2],427,185.5,1) +
			this.get_elem(sp.flowerpot 	[2],99,187.5,1) +
			this.get_elem(sp.flowerpot 	[4],339,191.5,1) +
			this.get_elem(sp.flowerpot 	[3],128,184,1) +
			this.get_elem(sp.bush 		[3],214,196.5,1) +
			this.get_elem(sp.bush 		[1],-13,200,1)
		);
		this.space.eq(18).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(19).append(
			this.get_elem(sp.stall		[1],31,125.5,1) + 
			this.get_elem(sp.bush		[4],-6,210,1) + 
			this.get_elem(sp.stall		[2],222,128.5,1) + 
			this.get_elem(sp.tree		[10],428,122,1) + 
			this.get_elem(sp.bin		[1],411,167,1) + 
			this.get_elem(sp.bush		[2],339,200.5,1) + 
			this.get_elem(sp.flower		[2],371,190,1) + 
			this.get_elem(sp.flower		[2],359,193,1) + 
			this.get_elem(sp.flower		[1],388,194,1) + 
			this.get_elem(sp.bush		[3],467,196.5,1)
		);
		this.space.eq(20).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(21).append(
			this.get_elem(sp.flag		[1],253,43,1) +
			this.get_elem(sp.bush		[2],159,201,1) +
			this.get_elem(sp.bush		[4],282,192,1) +
			this.get_elem(sp.bush		[3],335,201,1) +
			this.get_elem(sp.bush		[5],262,160,1) +
			this.get_elem(sp.tree		[10],0,125,1) +
			this.get_elem(sp.tree		[1],56,90,1) +
			this.get_elem(sp.tree		[10],474,123,1) +
			this.get_elem(sp.tree		[4],386,150,1) +
			this.get_elem(sp.tree		[1],430,90,1) +
			this.get_elem(sp.bush		[5],335,201,1) + 
			this.get_elem(sp.bush		[1],105,204,1)
		);
		this.space.eq(22).append(
			this.get_elem(sp.bench		[0],76,189,1) + 
			this.get_elem(sp.bench		[1],325,192.5,1) + 
			this.get_elem(sp.bush		[1],-18,199,1) + 
			this.get_elem(sp.bush		[4],433,200,1) + 
			this.get_elem(sp.tree		[3],391,104.5,1) + 
			this.get_elem(sp.tree		[9],15,119.5,1) + 
			this.get_elem(sp.tree		[7],147,120,1) + 
			this.get_elem(sp.tree		[2],195,108.5,1) + 
			this.get_elem(sp.tree		[2],249,118.5,1)
		);
		this.space.eq(23).append(
			this.get_elem(sp.tree		[9],-5,113.5,1) + 
			this.get_elem(sp.tree		[9],118,112.5,1) + 
			this.get_elem(sp.tree		[3],184,108.5,1) + 
			this.get_elem(sp.tree		[1],58,92.5,1) + 
			this.get_elem(sp.tree		[7],381,124,2) + 
			this.get_elem(sp.tree		[3],386,98.5,1) + 
			this.get_elem(sp.bench		[1],284,187,2)
		);
		this.space.eq(24).append(
			this.get_elem(sp.tree		[9],-5,113.5,1) + 
			this.get_elem(sp.tree		[9],118,112.5,1) + 
			this.get_elem(sp.tree		[1],58,92.5,1) + 
			this.get_elem(sp.tree		[7],381,124,2) + 
			this.get_elem(sp.tree		[3],386,98.5,1) 
		);
	}
}

//--------------------------------------------------------------------------------
// Build World
//--------------------------------------------------------------------------------

function build_world() {
	BUILDINGS.generate();
	CLOUDS.generate();
	ENVIRONMENT.generate();
	HUD.build();
	FAST_TRAVEL.build();
	MERIDIEM.build();
	CAR.build();
}

$(document).ready(function() {
	// Shim for IE9 , demmet IE
	PointerEventsPolyfill.initialize({});
	build_world();
	$('*[data-toggle="tooltip"]').tooltip({ animation: false });
});